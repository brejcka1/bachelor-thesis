\section{Background} \label{sec:background}

In this section, we describe the concept of Profinet IO IRT \cite{profinet} standard that is widely used in industrial communication network and represents one of~the possible applications of~the algorithms described further in this thesis. We also describe the concepts of heuristics and exact methods. In the end, we review heuristic methods published in related literature. 

\subsection{Ethernet Scheduling}

As described in \cite{hanzalek_profinet} and \cite{galloway_net}, original Ethernet communication was intended to be event triggered. Event triggered communication tries to pass streams in the system immediately as created. However, since there is no time slot prebooked for the transmission, it can easily happen that the link is already occupied and the task must wait until it can be transmitted.

On the contrary, the usage in hard real-time systems requires deterministic and predictable behavior. Ethernet was not designed to satisfy these requirements. The ability to guarantee the event order is not typically implemented in networking protocols such as TCP/IP. Therefore different standards have been introduced to overcome the hardware shortcomings -- here we briefly describe the Profinet IO IRT standard.

Profinet IO IRT is a hard real-time communication protocol using individual static schedules that are distributed among the network nodes. Special hardware (switch) capable of~processing these schedules is required. Four different communication classes are defined to serve different requirements: RT Class UDP, RT Class 1, RT Class 2, and RT Class 3. They differ in~clock synchronization and real-time capability. The communication cycle is divided between the classes and creates individual communication intervals. Class UDP and Class 1 serve for event-triggered communication and are not suitable for critical traffic. Class 2 is deprecated and not currently used. Class 3 has the highest priority and allows to implement the time-triggered concept which is necessary to satisfy the real-time requirements. With properly synchronized clocks the jitter of the communication cycle is as low as possible.

The algorithms presented in this thesis are implemented for general communication scheduling problem, if applied on Profinet IO IRT standard, it would be necessary to introduce additional "safety margin" parameter separating each transmission frame.  \cite{hanzalek_profinet}

\subsection{Heuristic Algorithms}

Heuristic algorithms are especially useful for problems with a large solution space. In such problems, searching the whole solution space becomes impossible due to time requirements. Based on \cite{ko}, the permutation Flow-shop scheduling problem has $n!$ possible flow order setups where $n$~is the number of flows (tasks). Considering it takes $\approx 1$ ns to schedule each task order setup, for only 20 tasks it would take around 77 years to compute the solution if exploring every option. Even though efficient space searching can significantly reduce the number of~explored task setups, the exact algorithms still spend a lot of time searching for the optimal solution. Therefore, it is often necessary to compromise on optimality to obtain a fast solution.

Based on \cite{pearl}, there are three main performance measures for an algorithm:
\begin{itemize}
	\item Completeness -- whenever a solution exists, the algorithm finds it
	\item Optimality -- the algorithm always returns solution of the best objective value
	\item Complexity -- measures the time and memory requirements of the algorithm
\end{itemize}

In this thesis, we will focus on the time complexity while keeping the optimality and completeness as close as possible to the solution of exact methods.

According to \cite{modra}, scheduling heuristic algorithms can be divided into several categories. They can also use different approaches for creating the schedule and different priority rules. The priority rules are used for creating the order in which the tasks are added to the schedule. In this thesis, we follow these paradigms to formally describe the implemented algorithms. 

\subsubsection{Constructive Heuristics}

Constructive heuristics create a schedule from scratch by sequentially adding tasks. Since tasks have different scheduling difficulty (number of repetitions in the hyper period, duration, etc.), priority rules are created to determine the order in which the tasks are added to the schedule. A useful priority rule must maintain precedences -- if task $a$ depends on the execution of task $b$, task $b$ should be placed before the task $a$ in the priority list.

Many different \textbf{priority rules} are described in \cite{modra}. Here we will present a selection of~these rules with corresponding criteria suitable for the scope of this thesis:
\begin{itemize}
	\item \textit{MTS}: Most total successors

	The number of successors of the given task.

	\item \textit{EST}: Earliest start time

	The earliest possible start time of the task based on its predecessors.

	\item \textit{LST}: Latest start time

	The latest possible start time of the task based on its successors.

	\item \textit{MSLK}: Minimum slack 

	The slack in which the task can start = \textit{LST} -- \textit{EST}.

	\item \textit{RED}: Resource equivalent duration

	The product of duration and weighted resource requirements.

\end{itemize}






\newpage
Two main \textbf{schemes} for constructing the schedule are:
\begin{itemize}
	\item Serial scheduling scheme \label{ff}

	The serial scheduling scheme sequentially adds tasks from the sorted priority lists while keeping the constraints satisfied. It strictly follows the order given by the priority list and assigns each selected task the lowest possible start time. Simply said -- it assigns start times to tasks. Further, in the text, we refer to this approach as First Fit.

	\item Parallel scheduling scheme

	The parallel scheduling scheme works with the sorted priority list as well. In contradistinction to the serial scheme, it iterates over free start times. It selects the lowest possible start time and then searches through the priority list to find the first possible task to be assigned to this start time while keeping the constraints satisfied. Simply said~--~it~assigns tasks to start times.
\end{itemize}

\subsubsection{Improvement Heuristics} 

Improvement heuristics enhance already created schedule obtained by a constructive heuristics. Different operations depending on the problem specification and optimization criteria are performed to~find a local optimum. As in any optimization problem, it is necessary to~prevent getting stuck in a loop. Some of the used techniques are genetic algorithms, simulated annealing, and tabu search.











\subsection{Exact Algorithms}

Exact algorithms are complete and optimal. We will introduce two approaches suitable for solving the scheduling problem -- Constraint Programming (CP) and Integer Linear Programming (ILP). Since we use the CP mainly for enhancing our heuristics, we will use its version that is faster but does not guarantee optimality. On the other hand, the introduced ILP model guarantees optimality if provided sufficient time. Hence, we will further use the ILP method to compare the objective value of our other algorithms.

\subsubsection{Constraint Programming} \label{sec:csp}

Based on \cite{zelena}, scheduling is a Constraint Satisfaction Problem (CSP) which is a problem requiring a search for a feasible solution that satisfies all the predefined constraints. It is defined as a triple of:
\begin{itemize}
	\item $X = \{x_1, ..., x_n\}$ -- a set of decision variables
	\item $D = \{D_1, ..., D_n\}$ -- a set of allowable values for each variable
	\item $C = \{C_1, ..., C_m\}$ -- a set of constraints
\end{itemize}

Each variable from $x_i \in X$ can be assigned only a value $a_i$ belonging to its domain $D_i \in D$. The value assignment ($x_i = a_i, x_j = a_j, ...$) is subject to constraints defined in $C$. For each constraint, we can define a consistency checking function $f$ such that $f_i(x_1, ..., x_n) = 1$ if and only if the constraint $C_i$ is satisfied. An assignment that satisfies all $C_i \in C$ is called \textit{feasible}. An assignment where all variables are instantiated is called \textit{complete}. Otherwise, we call the assignment \textit{partial}. A \textit{solution} of CSP is a feasible and complete assignment.

In the scheduling problem, the variables represent the tasks that are to be scheduled, and the domains represent their possible start times. Hence, the domains are finite sets of discrete values. The constraints are binary and work over each pair of tasks.

The CSP is typically solved via a tree search algorithm where each node represents a~partial assignment of variables and reduced domains $D' = \{D_1', ..., D_n'\}$ where $D_i' \subseteq D_i$. Each time a~variable is assigned, the node creates a~new branch and performs a~consistency check. In~case the consistency check fails, the node is not further explored. The algorithm stops when it finds the first complete and feasible assignment or if all nodes were explored without finding such an assignment.

It is important to note that the na\"ive algorithm has a big branching factor -- if all domains $D_i$ had the same size then the number of all different complete assignments would be $|D_i|^n$ where $n$ is the number of variables. 

The CSP tree search algorithm is complete because it finds the solution each time it~exists (assuming we have enough time and resources for the computations). However, it is not optimal since it returns the first found solution. If needed, the algorithm can be improved in~a~way that allows adding an objective function \cite{zelena}, but we do not use this technique and rather use the techniques allowing us to find the first complete solution as quickly as possible.

There are many different CSP techniques helping to speed up the na\"ive tree search. Further, we will describe -- Backtracking, Backjumping, and Backmarking.


\textit{Backtracking} is used for a depth-first search that assigns values to variables one by one and when a conflict occurs (the domain of some unassigned variable is empty), the algorithm backtracks to the most recently assigned variable. The pseudocode is shown in Algorithm~\ref{alg:backtrack}. Line~\ref{alg:inference} of the algorithm performs a forward checking. Forward checking is a procedure in~which we reduce domains of variables that have not yet been assigned based on the currently assigned variable's value. In~case this step results in some of the variables having an empty domain, a feasible solution does not exist for the current partial assignment, and the algorithm needs to backtrack.

\begin{algorithm}[th]
\caption{Backtracking algorithm for CSP by Russell and Norvig \cite{russell_AI}}\label{alg:backtrack}
\begin{algorithmic}[1]
\Procedure{backtracking-search}{$csp$}
    \State \Return \Call{backtrack}{$\{\}$, $csp$}
\EndProcedure

\Procedure{backtrack}{$assignment$, $csp$}
	\If {$assignment$ is complete} \Return $assignment$ \EndIf
	\State $var \gets$ select-unassigned-variable(csp)
	\For{$value \in$ order-domain-values($var$, $assignment$, $csp$)}
		\If {$value$ is consistent with $assignment$}
			\State add$\{var = value\}$ to $assignment$
			\State $inferences \gets$ inference($csp$, $var$, $value$) \label{alg:inference}
			\If {$inferences \neq failure$}
				\State add $inferences$ to $assignment$
    			\State $result \gets$ \Call{backtrack}{$assignment$, $csp$}
				\If {$result \neq failure$} \Return $result$	\EndIf
			\EndIf
		\EndIf
		\State remove $\{var = value\}$ and $inferences$ from $assignment$
	\EndFor
    \State \Return failure
\EndProcedure
\end{algorithmic}
\end{algorithm}

\textit{Conflict-Directed Backjumping} (CBJ) is a more efficient version of backtracking. While in~backtracking, the algorithm returns to the previous level of the tree, the backjumping method can jump right to the assignment that caused the current failure. More specifically, we create a conflict set $conf(x_i)$ for each variable $x_i \in X$. Each time we try to assign $x_i$ some value from its domain and the assignment fails due to consistency checks, we add the variable $x_j$ that caused conflict to the conflict set of $x_i$. After we unsuccessfully try to assign $x_i$ every value from its domain, we backjump to the most recently assigned variable $x_h$ from the conflict set of $x_i$ and update the conflict set of $x_h$:
$$conf(x_h) \gets conf(x_h) \cup conf(x_i) \setminus \{x_h\}$$
This update helps us to keep information about the conflicting variables. \cite{russell_AI}

\textit{Backmarking} is a method introduced by Gaschnig \cite{backmarking}. In this method the results of~consistency checks are saved and reused after the algorithm backtracks; this helps to avoid unnecessary constraints rechecking.

As introduced in Kondrak and van Beek \cite{kondrak}, both of the methods can be combined. Moreover, in \cite{vlk_dp}, Vlk shows an iterative version of the combined \textit{Conflict-Directed Backjumping with Backmarking} (\textit{CBJ\_BM}). Later on, in Section \ref{sec:multiplepass}, we will elaborate on this exact algorithm while creating a more sophisticated heuristics. In the mentioned chapter, we~also describe the \textit{CBJ\_BM} more thoroughly and provide pseudocode.

\subsubsection{Integer Linear Programming} \label{ILP}

Integer Linear Programming (ILP) is a special case of Constraint Satisfaction Problem where all variables are discrete and constraints linear. The ILP is given by matrix $\mathbf{A} \in \mathbb{R}^{m \times n}$ and vectors $\mathbf{b} \in \mathbb{R}^m$ and $\mathbf{c} \in \mathbb{R}^n$. The goal is to find a vector $\mathbf{x} \in \mathbb{Z}^n$ such that $\mathbf{A} \mathbf{x} \leq \mathbf{b}$ and $\mathbf{c}^T \mathbf{x}$ is maximized. \cite{modra}

Even though the exact methods are not the core of this thesis, an ILP model was implemented as a reference solution for the heuristics and is further described in Section \ref{solILP}.
















\subsection{Related Work}

In \cite{pira_line}, Pira proposes an improvement heuristic following the game theory paradigm. As~in game theory problem where two players take turns, changing their strategies based on the current knowledge, the algorithm updates schedule partitions to optimize their quality.

Minaeva et al. \cite{anna_heur} address the problem by creating a constructive heuristic that sequentially adds tasks to the schedule based on priority order. In the case of the infeasibility of the given task, a reason graph is used to help with the backtracking. After this step, the schedule is optimized by local neighborhood search using ILP.

Syed and Fohler \cite{syed} present a search-tree pruning heuristic based on job response-time. The heuristic searches for groups of symmetrical sub-schedules and uses only one sub-schedule from the group in the searching process (tree pruning technique). They emphasize the fact that pruning of infeasible search-tree paths is as important as looking for new feasible ones and use Parallel Iterative Deepening A* to create the schedule.

Finally, Bansal creates a divide-and-conquer heuristic in his master thesis \cite{bansal_dp}. Contrary to~most of the algorithms in the literature, he proposes a decentralized approach aiming mostly at large-scale networks. Following the divide-and-conquer paradigm, the schedule for each link is created separately, and then the schedules are completed together. However, from the text, it is unclear how the algorithm treats conflicts among the pre-created schedules.

There are several different approaches to consider when designing a heuristic algorithm. From the literature described above, it is clear that any introduction of backtracking significantly increases the time for which the solver runs. Therefore, we decided to focus on one pass heuristics that don't use any backtracking and their power lies in creating the order in which the streams are added to the schedule. This concept was partially introduced in \cite{anna_heur} but was not deeply explored. Further, we apply the knowledge gained from the one pass heuristics and design Conflict-Directed Backjumping and Backmarking search method proposed in \cite{vlk_dp} which is using dynamic granularity to speed up the searching.