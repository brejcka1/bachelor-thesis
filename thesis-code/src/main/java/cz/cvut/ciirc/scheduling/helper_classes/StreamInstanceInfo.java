package cz.cvut.ciirc.scheduling.helper_classes;

import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import lombok.Getter;
import lombok.Setter;

/**
 * Holds additional information for specified stream instance.
 * Useful for priority rules and solverMethods.
 */

@Getter
public class StreamInstanceInfo {
    private int earliestStart = 0;
    @Setter
    private int latestStart;
    private int IDinStreamList;
    @Getter
    private StreamInstance curSI;
    @Setter
    private StreamInstanceInfo prevSII;
    @Setter
    private StreamInstanceInfo nextSII;

    public StreamInstanceInfo(StreamInstance fli, int latestStart, StreamInstanceInfo efli, StreamInstanceInfo next, int idInList, int earlStart) {
        IDinStreamList = idInList;
        curSI = fli;
        earliestStart = earlStart;
        this.latestStart = latestStart;
        prevSII = efli;
        nextSII = next;
    }

    @Override
    public String toString() {
        return "StreamInstanceInfo{" +
                "earliestStart=" + earliestStart +
                ", latestStart=" + latestStart +
                ", IDinStreamList=" + IDinStreamList +
                ", stream=" + curSI.getStream() +
                ", link=" + curSI.getLink() +
                '}';
    }

    /**
     * Sets new earliest start time in case it is more restrictive.
     */
    public void updateEarliestStart(int newValue) {
        earliestStart = Math.max(newValue, earliestStart);
    }

    /**
     * Sets new latest start time in case it is more restrictive.
     */
    public void updateLatestStart(int newValue) {
        latestStart = Math.min(newValue, latestStart);
    }
}
