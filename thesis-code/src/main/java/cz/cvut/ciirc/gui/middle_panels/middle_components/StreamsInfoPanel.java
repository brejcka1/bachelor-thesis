package cz.cvut.ciirc.gui.middle_panels.middle_components;

import cz.cvut.ciirc.gui.GUIConstants;
import cz.cvut.ciirc.network_and_traffic_model.Stream;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

import static cz.cvut.ciirc.gui.GUIConstants.smallBold;

public class StreamsInfoPanel extends JScrollPane {

    public StreamsInfoPanel(List<Stream> streams) {
        JTable jTable;

        DefaultTableModel model = new DefaultTableModel();
        String[] columnNames = {"id", "name", "duration", "period", "release", "deadline", "sender", "receiver"};
        model.setColumnIdentifiers(columnNames);


        for (Stream stream : streams) {
            List<String> list = new ArrayList<>();
            list.add(stream.getId() + "");
            list.add(stream.getName() + "");
            list.add(stream.getDuration() + "");
            list.add(stream.getPeriod() + "");
            list.add(stream.getRelease() + "");
            list.add(stream.getDeadline() + "");
            list.add(stream.getOrigin() + "");
            list.add(stream.getTarget() + "");
            model.addRow(list.toArray());
        }

        jTable = new JTable(model);

        jTable.getTableHeader().setFont(smallBold);
        jTable.setFont(GUIConstants.small);
        jTable.setRowHeight(GUIConstants.small.getSize() + 5);

        jTable.setFillsViewportHeight(true);
        getViewport().add(jTable);
    }


}
