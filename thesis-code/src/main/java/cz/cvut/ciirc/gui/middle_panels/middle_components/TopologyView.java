package cz.cvut.ciirc.gui.middle_panels.middle_components;

import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.gui.GUIConstants;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;

import javax.swing.*;
import java.awt.*;

/**
 * Main class to visually represent the graph topology.
 */
@SuppressWarnings("SpellCheckingInspection")
public class TopologyView extends JPanel {

    public TopologyView(ProblemInstance problemInstance) {
        super();
        init(problemInstance);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1000, 800);
    }

    private void init(ProblemInstance problemInstance) {
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        setLayout(new GridLayout());

        Graph graph = new SingleGraph("Tutorial", false, true);
        graphInit(graph, problemInstance);

        Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
        ViewPanel viewPanel = viewer.addDefaultView(false);
        viewer.enableAutoLayout();
        add(viewPanel);

        setGraphVisual(graph);
    }

    private void setGraphVisual(Graph graph) {
        graph.setAttribute("ui.quality");
        graph.setAttribute("ui.antialias");

        graph.setAttribute("ui.stylesheet", "" +
                "edge {" +
                "   text-background-mode: rounded-box;" +
                "   text-background-color: white;" +
                "   text-padding: " + (GUIConstants.small.getSize() / 5) + "px; " +
                "   text-alignment: along;" +
                "   text-size: " + GUIConstants.small.getSize() + "px;" +
                "   size: 3px;" +
                "   fill-color: " + GUIConstants.darkGreen + ";" +
                "}");

        graph.setAttribute("ui.stylesheet", "" +
                "node {" +
                "   shape: rounded-box;" +
                "   text-background-mode: rounded-box;" +
                "   size-mode: fit; " +
                "   text-padding: " + (GUIConstants.mediumBold.getSize() / 5) + "px; " +
                "   text-size: " + GUIConstants.mediumBold.getSize() + "px;" +
                "   text-alignment: center;" +
                "}");

        for (Node node : graph) {
            if (node.getDegree() > 1) {
                node.addAttribute("ui.style", "text-background-color: " + GUIConstants.lightGreen + ";");
            } else {
                node.addAttribute("ui.style", "text-background-color: " + GUIConstants.midGreen + ";");
            }
        }
    }

    private void graphInit(Graph graph, ProblemInstance problemInstance) {
        for (cz.cvut.ciirc.network_and_traffic_model.Node curNode : problemInstance.getNodes()) {
            Node node = graph.addNode(curNode.getName());
            node.addAttribute("ui.label", node.getId());
        }
        for (Link link : problemInstance.getLinks()) {
            graph.addEdge(link.getId() + "",
                    problemInstance.getNodes().get(link.getFrom()).getName(),
                    problemInstance.getNodes().get(link.getTo()).getName());
        }
    }
}