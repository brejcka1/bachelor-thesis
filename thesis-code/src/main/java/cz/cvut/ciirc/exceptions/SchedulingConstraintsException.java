package cz.cvut.ciirc.exceptions;

public class SchedulingConstraintsException extends CustomException {

    public SchedulingConstraintsException(String s) {
        super(s);
    }

    public SchedulingConstraintsException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
