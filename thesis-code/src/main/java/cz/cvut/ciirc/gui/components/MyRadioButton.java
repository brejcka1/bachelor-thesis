package cz.cvut.ciirc.gui.components;

import cz.cvut.ciirc.AlgorithmType;

import javax.swing.*;

public class MyRadioButton extends JRadioButton {
    private final AlgorithmType algorithmType;

    public MyRadioButton(AlgorithmType algorithmType) {
        super(algorithmType.name());
        this.algorithmType = algorithmType;
    }

    public AlgorithmType getAlgorithmType() {
        return algorithmType;
    }
}
