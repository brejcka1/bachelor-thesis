package cz.cvut.ciirc.scheduling.baseline_methods;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;
import cz.cvut.ciirc.scheduling.helper_classes.StreamInstanceInfo;

import java.util.*;
import java.util.concurrent.TimeoutException;

/**
 * Conflicted backjumping and backmarking as originally described in the diploma thesis pseudocode:
 * Marek Vlk: Dynamic Scheduling
 */

@SuppressWarnings("Duplicates")
public class CBJ_BM_NOH extends Heuristics {
    public CBJ_BM_NOH(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.CBJ_BM_NOH);
    }


    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        List<StreamInstanceInfo> siiList = initializeStreamInfoList();
        Stream stream;
        Stream prev = null;
        int value = -1;

        for (StreamInstanceInfo curSii : siiList) {
            if (prev == null || prev.getId() != curSii.getCurSI().getStream().getId()) {
                stream = curSii.getCurSI().getStream();
                prev = stream;
                Random random = new Random();
                value = random.nextInt(getProblemInstance().getStreams().size() * 5);
            }

            pQ.add(new ObjectPriorities<>(curSii, value, curSii.getIDinStreamList()));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        int numSII = pQ.size();
        StreamInstanceInfo[] variables = new StreamInstanceInfo[numSII];
        for (int i = 0; i < numSII; i++) {
            variables[i] = (StreamInstanceInfo) pQ.poll().getObject();
        }
        int siiID = 0;
        int curStart;
        boolean success = false;
        boolean fail;

        // create arrays
        int[] newVals = new int[numSII];
        int[] startTimes = new int[numSII];
        int[][] mark = new int[numSII][getProblemInstance().getHP()]; // A default value of 0 for arrays of integral types is guaranteed by the language spec
        int[][] backTo = new int[numSII][getProblemInstance().getHP()];
        ArrayList<HashSet<Integer>> conflictSet = new ArrayList<>(numSII);
        for (int id = 0; id < numSII; id++) {
            conflictSet.add(new HashSet<>());
        }
        // initialize start times to earliest values
        for (int id = 0; id < startTimes.length; id++) {
            startTimes[id] = variables[id].getCurSI().getStream().getRelease();
            newVals[id] = variables[id].getCurSI().getStream().getRelease();
        }

        while (siiID < numSII) {
            curStart = newVals[siiID];
            success = false;
            // try to assign current variable of index siiID
            while (!success && curStart <= (variables[siiID].getCurSI().getStream().getDeadline() - variables[siiID].getCurSI().getReSIInHP(0).getDurationWithTimeLag())) {
                // break in case the time is out
                if (Thread.currentThread().isInterrupted()) {
                    return false;
                }
                // check if assignment was prohibited in the future (type A saving)
                if (mark[siiID][curStart] < backTo[siiID][curStart]) {
                    conflictSet.get(siiID).add(mark[siiID][curStart]);
                    curStart++;
                    continue;
                }
                startTimes[siiID] = curStart;
                fail = false;

                // run consistency checks and skip the ones that were already done (type B saving)
                for (int j = backTo[siiID][curStart]; j < siiID; j++) {
                    if (!isConsistent(variables[siiID], startTimes[siiID], variables[j], startTimes[j])) {
                        conflictSet.get(siiID).add(j);
                        mark[siiID][curStart] = j;
                        fail = true;
                        break;
                    }
                }

                // update mark and back to based on successful or failed assignment
                if (!fail) {
                    mark[siiID][curStart] = siiID - 1;
                    success = true;
                }
                backTo[siiID][curStart] = siiID;
                curStart++;
            }
            // if assignment of current variable not successful -> backtrack
            if (!success) {
                siiID = getNewSiiID(variables, siiID, newVals, backTo, conflictSet);
                if (siiID == -1) return false;
            } else {
                newVals[siiID] = curStart;
                siiID++;
            }
        }

        // save calculated offsets
        if (success) {
            for (int i = 0; i < numSII; i++) {
                StreamInstance curSI = variables[i].getCurSI();
                for (int hpID = 0; hpID < curSI.getNumInHP(); hpID++) {
                    curSI.getReSIInHP(hpID).setStart(startTimes[i] + curSI.getStream().getPeriod() * hpID);
                }
            }
        }
        return success;
    }


    /**
     * Returns an index to which the method should jump.
     *
     * @param variables   List of variables
     * @param siiID       ID of the current variable
     * @param newVals     Domains of the variables
     * @param backTo      Clever field for consistency checks reduction
     * @param conflictSet Conflict sets for all variables
     * @return Index to which the method backjumps
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    @Override
    protected int getNewSiiID(StreamInstanceInfo[] variables, int siiID, int[] newVals, int[][] backTo, ArrayList<HashSet<Integer>> conflictSet) throws ArrayNotInitializedException {
        Optional<Integer> maxCS;
        int jumpID;// select index to jump to, join conflict sets
        maxCS = conflictSet.get(siiID).stream().max(Integer::compareTo);
        if (maxCS.isPresent()) jumpID = maxCS.get();
        else
            return -1;
        conflictSet.get(jumpID).addAll(conflictSet.get(siiID));
        conflictSet.get(jumpID).remove(jumpID);
        // update backTo
        for (int k = jumpID + 1; k < variables.length; k++) {
            for (int value = variables[k].getCurSI().getStream().getRelease(); value <= variables[k].getCurSI().getStream().getDeadline() - variables[k].getCurSI().getReSIInHP(0).getDurationWithTimeLag(); value++) {
                int original = backTo[k][value];
                backTo[k][value] = Math.min(original, jumpID);
            }
        }
        // update variables and conflict sets
        while (siiID > jumpID) {
            newVals[siiID] = variables[siiID].getCurSI().getStream().getRelease();
            conflictSet.get(siiID).clear();
            siiID--;
        }
        return siiID;
    }

    /**
     * Check if there are any conflicts between currently and previously scheduled instances.
     *
     * @param curSII   currently scheduled stream instance
     * @param curStart current start time
     * @param oldSII   previously scheduled stream instance
     * @param oldStart previous start time
     * @return true in case no constraint violation was found
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    @Override
    protected boolean isConsistent(StreamInstanceInfo curSII, int curStart, StreamInstanceInfo oldSII, int oldStart) throws ArrayNotInitializedException {
        boolean isSatisfied;
        // Link constraint - not overlapping for any hpID if on the same link
        if (curSII.getCurSI().getLink().getId() == oldSII.getCurSI().getLink().getId()) {
            isSatisfied = checkLinkConstraint(curSII, curStart, oldSII, oldStart);
            if (!isSatisfied) return false;
        }

        // Precedence constraint - if both belong to the same stream, precedences are kept
        if (curSII.getCurSI().getStream().getId() == oldSII.getCurSI().getStream().getId()) {
            return checkPrecedenceConstraint(curSII, curStart, oldSII, oldStart);
        }

        return true;
    }

    /**
     * Check if there are any precedence conflicts between currently and previously scheduled instances.
     *
     * @param curSII   currently scheduled stream instance
     * @param curStart current start time
     * @param oldSII   previously scheduled stream instance
     * @param oldStart previous start time
     * @return true in case no constraint violation was found
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    @Override
    protected boolean checkPrecedenceConstraint(StreamInstanceInfo curSII, int curStart, StreamInstanceInfo oldSII, int oldStart) throws ArrayNotInitializedException {
        // if true cur must finish with all delays before old starts else otherwise
        int nodeLag, duration;
        if (curSII.getIDinStreamList() < oldSII.getIDinStreamList()) {
            nodeLag = getProblemInstance().getNodes().get(curSII.getCurSI().getLink().getTo()).getTimeLag();
            duration = curSII.getCurSI().getReSIInHP(0).getDurationWithTimeLag();
            return curStart + duration + nodeLag <= oldStart;
        } else {
            nodeLag = getProblemInstance().getNodes().get(oldSII.getCurSI().getLink().getTo()).getTimeLag();
            duration = oldSII.getCurSI().getReSIInHP(0).getDurationWithTimeLag();
            return oldStart + duration + nodeLag <= curStart;
        }
    }

    /**
     * Check if there are any link conflicts between currently and previously scheduled instances.
     *
     * @param curSII   currently scheduled stream instance
     * @param curStart current start time
     * @param oldSII   previously scheduled stream instance
     * @param oldStart previous start time
     * @return true in case no constraint violation was found
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    @Override
    protected boolean checkLinkConstraint(StreamInstanceInfo curSII, int curStart, StreamInstanceInfo oldSII, int oldStart) throws ArrayNotInitializedException {
        int curHPStart, curHPend, oldHPStart, oldHPend;
        int curPeriod = curSII.getCurSI().getStream().getPeriod();
        int oldPeriod = oldSII.getCurSI().getStream().getPeriod();
        int curDuration = curSII.getCurSI().getReSIInHP(0).getDuration();
        int oldDuration = oldSII.getCurSI().getReSIInHP(0).getDuration();

        curHPStart = curStart;
        curHPend = curStart + curDuration;
        oldHPStart = oldStart;
        oldHPend = oldStart + oldDuration;
        // if periods are same - check only in hpID = 0 otherwise check all combinations
        if (curSII.getCurSI().getStream().getPeriod() == oldSII.getCurSI().getStream().getPeriod()) {
            return (curHPend <= oldHPStart || oldHPend <= curHPStart);
        } else {
            for (int curHPid = 0; curHPid < curSII.getCurSI().getNumInHP(); curHPid++) {
                oldHPStart = oldStart;
                oldHPend = oldStart + oldDuration;
                for (int oldHPid = 0; oldHPid < oldSII.getCurSI().getNumInHP(); oldHPid++) {
                    if (!(curHPend <= oldHPStart || oldHPend <= curHPStart)) return false;
                    oldHPStart += oldPeriod;
                    oldHPend += oldPeriod;
                }
                curHPStart += curPeriod;
                curHPend += curPeriod;
            }
        }
        return true;
    }
}