package cz.cvut.ciirc.exceptions;

public class TopologyInitializationException extends CustomException {

    public TopologyInitializationException(String s) {
        super(s);
    }

    public TopologyInitializationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
