package cz.cvut.ciirc.gui;


import cz.cvut.ciirc.gui.middle_panels.*;
import cz.cvut.ciirc.scheduling.Solution;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;

/**
 * Represents the middle panel containing the core of the GUI (several different screens).
 */
public class MiddlePanel extends JPanel {
    private final CardLayout cardLayout;

    private FilePanel filePanel;
    private SolverPanel solverPanel;
    private TopologyPanel topologyPanel;
    private SchedulePanel schedulePanel;
    private InfoPanel infoPanel;

    private Solution solution;


    MiddlePanel(Path instancePath) {
        super(new CardLayout());
        cardLayout = (CardLayout) getLayout();
        init(instancePath);
    }

    private void init(Path instancesPath) {
        // initialize all panels
        filePanel = new FilePanel(instancesPath);
        solverPanel = new SolverPanel();
        topologyPanel = new TopologyPanel(this);
        schedulePanel = new SchedulePanel(this);
        infoPanel = new InfoPanel(this);
        // add all panels
        add(filePanel, "file");
        add(solverPanel, "solver");
        add(topologyPanel, "instance");
        add(schedulePanel, "run");
        add(infoPanel, "stats");
    }

    void switchToFilePanel() {
        cardLayout.show(this, "file");
    }

    void switchToInstancePanel() {
        cardLayout.show(this, "instance");
    }

    void switchToSchedulePanel() {
        cardLayout.show(this, "run");
    }

    void switchToStatisticsPanel() {
        cardLayout.show(this, "stats");
    }

    void switchToSolverPanel() {
        cardLayout.show(this, "solver");
    }


    FilePanel getFilePanel() {
        return filePanel;
    }

    SolverPanel getSolverPanel() {
        return solverPanel;
    }

    public Solution getSolution() {
        return solution;
    }

    public void setSolution(Solution solution) {
        this.solution = solution;
        topologyPanel.updatePanel();
        schedulePanel.updatePanel();
        infoPanel.updatePanel();
    }
}
