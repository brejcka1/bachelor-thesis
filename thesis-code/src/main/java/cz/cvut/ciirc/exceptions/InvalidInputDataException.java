package cz.cvut.ciirc.exceptions;

public class InvalidInputDataException extends CustomException {

    public InvalidInputDataException(String s) {
        super(s);
    }

    public InvalidInputDataException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
