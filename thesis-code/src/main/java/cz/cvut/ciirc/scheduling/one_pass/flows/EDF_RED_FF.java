package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.exceptions.UtilizationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;

import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Earliest Deadline Resource Equivalent Duration First Fit Heuristic</h3>
 * <p>
 * One pass heuristic method. Stream instances are sorted by earliest deadline and then decreasing product of duration and weighted resource requirements.
 * <p>
 * Each stream instance is placed to first available slot.
 */
public class EDF_RED_FF extends Heuristics {
    public EDF_RED_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.EDF_RED_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) {
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws UtilizationException, ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        double[] utilization = getProblemInstance().getUtilization();
        int value;
        int utilCoef;
        for (Stream stream : getProblemInstance().getStreams()) {
            value = 0;
            for (StreamInstance si : stream.getStreamInstanceList()) {
                utilCoef = (int) Math.ceil(utilization[si.getLink().getId()] * 10);
                value += (si.getReSIInHP(0).getDurationWithTimeLag() * utilCoef);
            }
            value = 10 * stream.getProblemInstance().getHP() - value;
            pQ.add(new ObjectPriorities<>(stream, stream.getDeadline(), value));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamsFirstFit(pQ);
    }
}