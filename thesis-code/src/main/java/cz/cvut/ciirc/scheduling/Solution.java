package cz.cvut.ciirc.scheduling;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.data_format.OutputFile;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.SchedulingConstraintsException;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import cz.cvut.ciirc.network_and_traffic_model.ReoccurredStreamInstance;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import lombok.Getter;

import java.util.Comparator;
import java.util.List;

/**
 * Class that holds information about the instance and its solution.
 * Checks the correctness of the solution, counts objective and prepares the output proto file.
 */

@Getter
public class Solution {
    /**
     * Solved problem instance
     */
    private ProblemInstance problemInstance;
    /**
     * Solver that solved the instance
     */
    private AlgorithmType solverType;
    /**
     * True in case the solution was found, false otherwise
     */
    private boolean scheduleFound;
    /**
     * Objective value of the solution
     */
    private int objValue = -1;
    /**
     * Time it took the solver to solve the instance (or default time limit)
     */
    private double elapsedTime = -1;
    /**
     * Output proto file
     */
    private OutputFile.OutputFileProto outputProto = null;

    /**
     * Default constructor
     *
     * @param problemInstance Solved problem instance
     * @param solverType      Type of algorithm that solved the instance
     * @param scheduleFound   True in case the solution was found, false otherwise
     * @param elapsedTime     Time it took the solver to solve the instance
     */
    public Solution(ProblemInstance problemInstance, AlgorithmType solverType, boolean scheduleFound, double elapsedTime) {
        this.problemInstance = problemInstance;
        this.solverType = solverType;
        this.scheduleFound = scheduleFound;
        this.elapsedTime = elapsedTime;
    }

    /**
     * Processes the solution - checks it, initializes output proto and counts objective.
     *
     * @throws SchedulingConstraintsException In case some constraints were violated
     * @throws ArrayNotInitializedException   In case some constraints were violated
     */
    public void process() throws SchedulingConstraintsException, ArrayNotInitializedException {
        if (scheduleFound) {
            try {
                check();
            } catch (SchedulingConstraintsException e) {
                throw new SchedulingConstraintsException("Solution found by " + solverType.getMethodClass().getName() + " is not correct! - Solution didn't pass the check", e);
            }
        }
        initializeOutProto();
        countObjective();
    }


    /**
     * Checks the correctness of the solution.
     *
     * @return True in case the solution is correct, otherwise throws exception
     * @throws ArrayNotInitializedException   In case of faulty problem instance initialization
     * @throws SchedulingConstraintsException In case some constraint was violated
     */
    private boolean check() throws ArrayNotInitializedException, SchedulingConstraintsException {
        boolean control;

        control = checkIfEverythingScheduled();
        control = control && checkOverlappingConflicts(); // constraint (1)
        control = control && checkPrecedenceConstraints(); // constraint (2)
        control = control && checkZeroJitter(); // constraint(3)
        control = control && checkDeadlineAndRelease(); // constraint (4)

        return control;
    }

    /**
     * Initializes the output proto.
     */
    private void initializeOutProto() {
        List<ReoccurredStreamInstance> reSIList;
        OutputFile.OutputFileProto.Builder outBuilder = OutputFile.OutputFileProto.newBuilder();
        outBuilder.setScheduleFound(isScheduleFound());
        outBuilder.setObjective(getObjValue());
        outBuilder.setSchedulingTime(getElapsedTime());
        if (isScheduleFound()) {
            for (Link link : problemInstance.getLinks()) {
                OutputFile.LinkScheduleProto.Builder schBuilder = OutputFile.LinkScheduleProto.newBuilder();
                schBuilder.setLinkName(link.getName());
                reSIList = problemInstance.getAllReoccurredStreamInstancesForLink(link.getId());
                for (ReoccurredStreamInstance reSI : reSIList) {
                    OutputFile.TimeIntervalProto.Builder timeIntervalBuilder = OutputFile.TimeIntervalProto.newBuilder();
                    timeIntervalBuilder.setStart(reSI.getStart()).setEnd(reSI.getEnd())
                            .setStreamName(reSI.getParent().getStream().getName());
                    schBuilder.addTimeInterval(timeIntervalBuilder.build());
                }
                outBuilder.addSchedule(schBuilder.build());
            }
        }
        outputProto = outBuilder.build();
    }

    /**
     * Counts objective value of the found solution.
     *
     * @throws ArrayNotInitializedException in case of faulty problem instance initialization
     */
    private void countObjective() throws ArrayNotInitializedException {
        if (scheduleFound) {
            objValue = 0;
            for (Stream stream : problemInstance.getStreams()) {
                objValue += stream.getTotalRequiredTime();
            }
        }
    }

    /**
     * All predecessors of the reoccurred stream instance must finish before this instance starts.
     *
     * @return True in case the constraint is satisfied
     * @throws ArrayNotInitializedException   In case of faulty problem initialization
     * @throws SchedulingConstraintsException In case of constraint violation
     */
    private boolean checkPrecedenceConstraints() throws ArrayNotInitializedException, SchedulingConstraintsException {
        int minStartTime;
        StreamInstance prev;
        for (Stream stream : problemInstance.getStreams()) {
            for (int hpIndex = 0; hpIndex < stream.getNumHPInstances(); hpIndex++) {
                prev = null;
                for (StreamInstance fli : stream.getStreamInstanceList()) {
                    minStartTime = 0;
                    if (prev != null) {
                        minStartTime = prev.getReSIInHP(hpIndex).getEnd()
                                + problemInstance.getNodes().get(prev.getLink().getTo()).getTimeLag();
                    }

                    if (fli.getReSIInHP(hpIndex).getStart() < minStartTime) {
                        throw new SchedulingConstraintsException("Precedence constraints violated for stream: " + stream
                                + "allowed link start time: " + minStartTime + " real start time: "
                                + fli.getReSIInHP(hpIndex).getStart());
                    }
                    prev = fli;
                }
            }
        }
        return true;
    }

    /**
     * Nothing can overlap on the links.
     *
     * @return True in case the constraint is satisfied
     * @throws SchedulingConstraintsException In case of constraint violation
     */
    private boolean checkOverlappingConflicts() throws SchedulingConstraintsException {
        int end;
        List<ReoccurredStreamInstance> linkFI;
        for (Link link : problemInstance.getLinks()) {
            linkFI = problemInstance.getAllReoccurredStreamInstancesForLink(link.getId());
            linkFI.sort(Comparator.comparing(ReoccurredStreamInstance::getStart, Comparator.nullsLast(Comparator.naturalOrder())));
            for (int reSiID = 0; reSiID < linkFI.size() - 1; reSiID++) {
                end = linkFI.get(reSiID).getStart() + linkFI.get(reSiID).getDuration();
                if (end > linkFI.get(reSiID + 1).getStart()) {
                    throw new SchedulingConstraintsException("ReoccurredStreamInstances: "
                            + "\n" + linkFI.get(reSiID)
                            + "\n" + linkFI.get(reSiID + 1)
                            + "\nare overlapping on:\n" + link
                            + "\nstart time: " + linkFI.get(reSiID).getStart()
                            + "\nend time: " + end + "\nnext start time: " + linkFI.get(reSiID + 1).getStart());
                }
            }
        }
        return true;

    }


    /**
     * All reoccurred stream instances must have their start times assigned.
     *
     * @return True in case the constraint is satisfied
     * @throws SchedulingConstraintsException In case of constraint violation
     */
    private boolean checkIfEverythingScheduled() throws SchedulingConstraintsException {
        List<ReoccurredStreamInstance> unscheduled = problemInstance.getAllUnscheduledReoccurredStreamInstances();
        if (!unscheduled.isEmpty()) {
            throw new SchedulingConstraintsException("There are some unscheduled Reoccurred Stream Instances");
        }
        return true;
    }

    /**
     * All reoccurred stream instances on the same stream instance must be strictly periodic.
     *
     * @return True in case the constraint is satisfied
     * @throws ArrayNotInitializedException   In case of faulty problem initialization
     * @throws SchedulingConstraintsException In case of constraint violation
     */
    private boolean checkZeroJitter() throws ArrayNotInitializedException, SchedulingConstraintsException {
        List<Stream> streams = this.problemInstance.getStreams();
        int expectedStart, realStart, period;
        ReoccurredStreamInstance fri;
        for (Stream stream : streams) {
            for (StreamInstance fli : stream.getStreamInstanceList()) {
                fri = fli.getReSIInHP(0);
                period = fli.getStream().getPeriod();
                expectedStart = fri.getStart() + period;
                for (int hpIndex = 1; hpIndex < fli.getNumInHP(); hpIndex++) {
                    realStart = fli.getReSIInHP(hpIndex).getStart();
                    if (expectedStart != realStart) {
                        throw new SchedulingConstraintsException("ZJ constraints violated for stream: " + fli +
                                "\nStart time is: " + realStart + " but should be " + expectedStart);
                    }
                    expectedStart += period;
                }
            }
        }
        return true;
    }

    /**
     * All streams must begin after release time and end before deadline.
     *
     * @return True in case the constraint is satisfied
     * @throws ArrayNotInitializedException   In case of faulty problem initialization
     * @throws SchedulingConstraintsException In case the constraint is not satisfied
     */
    private boolean checkDeadlineAndRelease() throws ArrayNotInitializedException, SchedulingConstraintsException {
        int release, deadline;
        for (Stream stream : problemInstance.getStreams()) {
            release = stream.getRelease();
            deadline = stream.getDeadline();
            for (int hpID = 0; hpID < stream.getNumHPInstances(); hpID++) {
                if (stream.getFirstReSIInPeriod(0).getStart() < release) {
                    throw new SchedulingConstraintsException("Earliest release time: " + release + " but scheduled at: "
                            + stream.getFirstReSIInPeriod(0).getStart());
                }
                if (stream.getLastReSIInPeriod(0).getEnd() > deadline) {
                    throw new SchedulingConstraintsException("Deadline: " + deadline + " but ends at: "
                            + stream.getLastReSIInPeriod(0).getEnd());
                }

            }
        }
        return true;

    }
}
























