package cz.cvut.ciirc.helper;

import com.google.protobuf.Message;
import cz.cvut.ciirc.SchedulerConstants;
import cz.cvut.ciirc.data_format.InputFile;
import cz.cvut.ciirc.data_format.LinkList;
import cz.cvut.ciirc.data_format.NodeList;
import cz.cvut.ciirc.data_format.StreamList;
import cz.cvut.ciirc.exceptions.GeneratorException;
import cz.cvut.ciirc.exceptions.InputFileNotFoundException;
import cz.cvut.ciirc.exceptions.InvalidInputDataException;
import cz.cvut.ciirc.exceptions.UnspecifiedException;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class handling most of the IO and proto files related stuff, mostly static methods.
 */
public class ProtoHandler {
    /**
     * Default prefix for nodes defined only by number.
     */
    public static String nodePrefix = "node_";
    /**
     * Default prefix for links defined only by number.
     */
    private static String linkPrefix = "link_";
    /**
     * Default prefix for streams defined only by number.
     */
    private static String streamPrefix = "stream_";

    /**
     * Reads input proto from specified path.
     *
     * @param inputPath Path from which the proto is read
     * @return Java proto representation
     * @throws InputFileNotFoundException In case the file was not found
     * @throws InvalidInputDataException  In case the file could not be read
     */
    public static InputFile.InputFileProto readInput(Path inputPath) throws InputFileNotFoundException, InvalidInputDataException {
        InputFile.InputFileProto inputFile;
        try {
            FileInputStream inStream = new FileInputStream(inputPath.toString());
            inputFile = InputFile.InputFileProto.parseFrom(inStream);
        } catch (FileNotFoundException e) {
            throw new InputFileNotFoundException("Could not find proto input file on address: " + inputPath, e);
        } catch (IOException e) {
            throw new InvalidInputDataException("Could not read input file", e);
        }
        return inputFile;
    }

    /**
     * Saves specified proto input to specified path.
     *
     * @param outputPath Path to which the proto is saved
     * @param content    Proto buffer in java class format to be saved
     * @throws UnspecifiedException In case the file could not be created
     */
    public static void writeProto(Path outputPath, Message content) throws UnspecifiedException {
        FileOutputStream writer;
        try {
            writer = new FileOutputStream(outputPath.toString());
            content.writeTo(writer);
        } catch (IOException e) {
            throw new UnspecifiedException("Could not create output file on address: " + outputPath, e);
        }
    }

    /**
     * Creates directory on specified path in case it does not exist.
     *
     * @param dirPath Path of the directory
     */
    private static void createDir(Path dirPath) {
        File f = dirPath.toFile();
        if (!f.exists()) {
            if (!f.mkdir()) System.err.println("Failed to create folder " + dirPath);
        }
    }

    /**
     * Goes through .pb files in the given directory and searched for the first free file ID.
     *
     * @param dirID ID of the directory
     * @return ID of the first not occupied file name
     * @throws GeneratorException In case maximal number of files was exceeded for the specified directory
     */
    public static int findFirstFreeFileID(int dirID) throws GeneratorException {
        int fileID = 0;
        Path dirPath = getInstanceDirPath(dirID);
        while (fileID < SchedulerConstants.MAX_NUM_INSTANCES_IN_FOLDER) {
            File f = dirPath.resolve(getFilePathEnding(fileID)).toFile();
            if (!f.exists()) {
                break;
            }
            fileID++;
        }
        if (fileID == SchedulerConstants.MAX_NUM_INSTANCES_IN_FOLDER) {
            throw new GeneratorException("Maximal number of instances reached in folder: " + dirPath);
        }
        return fileID;
    }

    /**
     * Adds one entry to link list proto.
     *
     * @param listBuilder Link list proto builder
     * @param id          ID of the link
     * @param origin      Origin of the link
     * @param target      Destination of the link
     * @param weight      Weight of the link
     * @param timeLag     Time lag on the link
     */
    public static void addLinkToProto(LinkList.LinkListProto.Builder listBuilder, int id, int origin, int target, int weight, int timeLag) {
        LinkList.LinkProto.Builder builder = LinkList.LinkProto.newBuilder();
        builder.setName(linkPrefix + id).setOrigin(nodePrefix + origin).setTarget(nodePrefix + target).setTimeLag(timeLag).setWeight(weight);
        listBuilder.addLink(builder.build());
    }

    /**
     * Adds one entry to node list proto.
     *
     * @param listBuilder Link list proto builder
     * @param id          ID of the node
     * @param timeLag     Time lag on the node
     */
    public static void addNodeToProto(NodeList.NodeListProto.Builder listBuilder, int id, int timeLag) {
        NodeList.NodeProto.Builder builder = NodeList.NodeProto.newBuilder();
        builder.setName(nodePrefix + id).setTimeLag(timeLag);
        listBuilder.addNode(builder.build());
    }

    /**
     * Adds one entry to stream list proto.
     *
     * @param listBuilder Stream list proto builder
     * @param id          ID of the stream
     * @param origin      Origin of the stream
     * @param target      Target of the stream
     * @param duration    Duration of the stream
     * @param period      Period of the stream
     * @param release     Release time of the stream
     * @param deadline    Deadline of the stream
     */
    public static void addStreamToProto(StreamList.StreamListProto.Builder listBuilder, int id, int origin, int target, int duration, int period, int release, int deadline) {
        StreamList.StreamProto.Builder builder = StreamList.StreamProto.newBuilder();
        builder.setName(streamPrefix + id).setOrigin(nodePrefix + origin).setTarget(nodePrefix + target).setDuration(duration)
                .setPeriod(period).setDeadline(deadline).setRelease(release);
        listBuilder.addStream(builder.build());
    }

    /**
     * Adds one entry to stream list proto.
     *
     * @param listBuilder Stream list proto builder
     * @param id          ID of the stream
     * @param origin      Origin of the stream
     * @param target      Target of the stream
     * @param duration    Duration of the stream
     * @param period      Period of the stream
     * @param release     Release time of the stream
     * @param deadline    Deadline of the stream
     */
    public static void addStreamToProto(StreamList.StreamListProto.Builder listBuilder, int id, String origin, String target, int duration, int period, int release, int deadline) {
        StreamList.StreamProto.Builder builder = StreamList.StreamProto.newBuilder();
        builder.setName(streamPrefix + id).setOrigin(origin).setTarget(target).setDuration(duration)
                .setPeriod(period).setDeadline(deadline).setRelease(release);
        listBuilder.addStream(builder.build());
    }

    /**
     * Helper method that returns file name of instance based on its ID.
     *
     * @param instanceID ID of the instance
     * @return Instance file name
     */
    private static String getFilePathEnding(int instanceID) {
        return "instance_" + instanceID + ".pb";
    }

    /**
     * Returns path to main instances directory
     *
     * @return Path to main instances directory
     */
    private static Path getRootInstancesDirPath() {
        Path path = Paths.get(System.getProperty("user.dir")).resolveSibling("instances");
        createDir(path);
        return path;
    }

    /**
     * Returns path to instances directory of specified ID
     *
     * @param dirID ID of the directory
     * @return Path to the instances directory of specified ID
     */
    private static Path getInstanceDirPath(int dirID) {
        Path path = getRootInstancesDirPath().resolve("instance_" + dirID);
        createDir(path);
        return path;
    }

    /**
     * Returns path to instance of specified ID in specified directory.
     *
     * @param dirID  ID of the directory
     * @param fileID ID of the file
     * @return Path to the instance
     */
    public static Path getInstanceFilePath(int dirID, int fileID) {
        return getInstanceDirPath(dirID).resolve("instance_" + fileID + ".pb");
    }

    /**
     * Returns path to main results directory
     *
     * @return Path to main results directory
     */
    private static Path getRootResultsDirPath() {
        Path path = Paths.get(System.getProperty("user.dir")).resolveSibling("results");
        createDir(path);
        return path;
    }

    /**
     * Returns path to results directory of specified ID
     *
     * @param dirID ID of the directory
     * @return Path to the results directory of specified ID
     */
    private static Path getResultDirPath(int dirID) {
        Path path = getRootResultsDirPath().resolve("result_" + dirID);
        createDir(path);
        return path;
    }

    /**
     * Returns path to results of instance of specified ID in specified directory.
     *
     * @param dirID  ID of the directory
     * @param fileID ID of the instance
     * @return Path to the results
     */
    public static Path getResultFilePath(int dirID, int fileID) {
        return getResultDirPath(dirID).resolve("result_" + fileID + ".pb");
    }

    /**
     * Returns path to statistics of instances in specified directory.
     *
     * @param dirID ID of the directory
     * @return Path to the statistics file
     */
    public static Path getStatsPath(int dirID) {
        return getRootResultsDirPath().resolve("result_" + dirID + ".txt");
    }
}
