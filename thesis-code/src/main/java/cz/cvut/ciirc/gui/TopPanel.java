package cz.cvut.ciirc.gui;

import javax.swing.*;
import java.awt.*;

import static cz.cvut.ciirc.gui.GUIConstants.largeBold;
import static cz.cvut.ciirc.gui.GUIConstants.mediumBold;

/**
 * Represents top GUI panel showing GUI workflow.
 */
class TopPanel extends JPanel {
    private final Dimension space = new Dimension(0, 15);
    private JLabel topLabel;
    private JLabel workFlowLabel;


    TopPanel() {
        init();
    }

    private void init() {
        setBackground(Color.gray);
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        topLabel = new JLabel("Scheduler");
        workFlowLabel = new JLabel("Choose Instance + Solver -> View Schedule & Topology & Instance info");
        addLabels();
        improveLabels();
    }

    private void addLabels() {
        add(Box.createRigidArea(space));
        add(topLabel, CENTER_ALIGNMENT);
        add(workFlowLabel, CENTER_ALIGNMENT);
        add(Box.createRigidArea(space));
    }

    private void improveLabels() {
        topLabel.setAlignmentX(CENTER_ALIGNMENT);
        workFlowLabel.setAlignmentX(CENTER_ALIGNMENT);
        topLabel.setFont(largeBold);
        workFlowLabel.setFont(mediumBold);
    }
}
