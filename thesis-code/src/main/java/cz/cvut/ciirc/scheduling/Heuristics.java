package cz.cvut.ciirc.scheduling;

import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.SchedulerConstants;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.exceptions.UtilizationException;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import cz.cvut.ciirc.scheduling.helper_classes.LinkTimeSlots;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;
import cz.cvut.ciirc.scheduling.helper_classes.StreamInstanceInfo;

import java.util.*;
import java.util.concurrent.*;

/**
 * Parent class for all solverMethods containing common methods.
 * <p>
 * Heuristics can schedule on two basis - stream or stream instance.
 */
public class Heuristics extends Solver {
    /**
     * Containing sequentially updated schedule
     */
    private ArrayList<LinkTimeSlots> linkSchedule;


    public Heuristics(ProblemInstance problemInstance) {
        super(problemInstance);
        linkScheduleInit();
    }

    /**
     * Calculates start time of the currently scheduled stream instance based on previous knowledge and est.
     *
     * @param originalVal previously assigned start time
     * @param sii         the currently scheduled stream instance
     * @param variables   a list of all stream instances
     * @param curSiiID    scheduling progress id of the current sii
     * @param conflictSet the conflict set of currently scheduled sii
     * @return calculated start time
     * @throws ArrayNotInitializedException In case the framework is faulty
     */
    protected int calculateOffset(int originalVal, StreamInstanceInfo sii, StreamInstanceInfo[] variables, int curSiiID, ArrayList<HashSet<Integer>> conflictSet) throws ArrayNotInitializedException {
        int est = sii.getCurSI().getStream().getRelease();
        List<StreamInstance> neighbors = sii.getCurSI().getStream().getStreamInstanceList();
        int conflictSIIid = 0;
        boolean found = false;

        for (int siiID = 0; siiID < sii.getIDinStreamList(); siiID++) {
            if (neighbors.get(siiID).getReSIInHP(0).getStart() != SchedulerConstants.DEFAULT_START_TIME) {
                found = false;
                for (int checkID = 0; checkID < curSiiID; checkID++) {
                    if (variables[checkID].getCurSI() == neighbors.get(siiID)) {
                        conflictSIIid = checkID;
                        found = true;
                        break;
                    }
                }
                if (est < neighbors.get(siiID).getReSIInHP(0).getStart()) {
                    if (!found) System.err.println("Adding incorrent checkID in CBJ_BM");
                    conflictSet.get(curSiiID).add(conflictSIIid);
                }
                est = neighbors.get(siiID).getReSIInHP(0).getStart();
            }
            est += (neighbors.get(siiID).getReSIInHP(0).getDurationWithTimeLag()
                    + getProblemInstance().getNodes().get(neighbors.get(siiID).getLink().getTo()).getTimeLag());
        }
        return Math.max(est, originalVal);
    }


    /**
     * Returns an index to which the method should jump.
     *
     * @param variables   List of variables
     * @param siiID       ID of the current variable
     * @param newVals     Domains of the variables
     * @param backTo      Clever field for consistency checks reduction
     * @param conflictSet Conflict sets for all variables
     * @return Index to which the method backjumps
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    protected int getNewSiiID(StreamInstanceInfo[] variables, int siiID, int[] newVals, int[][] backTo, ArrayList<HashSet<Integer>> conflictSet) throws ArrayNotInitializedException {
        Optional<Integer> maxCS;
        int jumpID;// select index to jump to, join conflict sets
        maxCS = conflictSet.get(siiID).stream().max(Integer::compareTo);
        if (maxCS.isPresent()) jumpID = maxCS.get();
        else
            return -1;
        conflictSet.get(jumpID).addAll(conflictSet.get(siiID));
        conflictSet.get(jumpID).remove(jumpID);
        // update backTo
        for (int k = jumpID + 1; k < variables.length; k++) {
            for (int value = variables[k].getEarliestStart(); value <= variables[k].getLatestStart(); value++) {
                backTo[k][value] = Math.min(backTo[k][value], jumpID);
            }
        }
        // update variables and conflict sets
        while (siiID > jumpID) {
            getProblemInstance().getStreamInstances().get(variables[siiID].getCurSI().getId()).getReSIInHP(0).setStart(SchedulerConstants.DEFAULT_START_TIME);
            variables[siiID].getCurSI().getReSIInHP(0).setStart(SchedulerConstants.DEFAULT_START_TIME);
            newVals[siiID] = variables[siiID].getEarliestStart();
            conflictSet.get(siiID).clear();
            siiID--;
        }
        variables[jumpID].getCurSI().getReSIInHP(0).setStart(SchedulerConstants.DEFAULT_START_TIME);
        return siiID;
    }

    /**
     * Check if there are any conflicts between currently and previously scheduled instances.
     *
     * @param curSII   currently scheduled stream instance
     * @param curStart current start time
     * @param oldSII   previously scheduled stream instance
     * @param oldStart previous start time
     * @return true in case no constraint violation was found
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    protected boolean isConsistent(StreamInstanceInfo curSII, int curStart, StreamInstanceInfo oldSII, int oldStart) throws ArrayNotInitializedException {
        boolean isSatisfied;
        // Link constraint - not overlapping for any hpID if on the same link
        if (curSII.getCurSI().getLink().getId() == oldSII.getCurSI().getLink().getId()) {
            isSatisfied = checkLinkConstraint(curSII, curStart, oldSII, oldStart);
            if (!isSatisfied) return false;
        }

        // Precedence constraint - if both belong to the same stream, precedences are kept
        if (curSII.getCurSI().getStream().getId() == oldSII.getCurSI().getStream().getId()) {
            return checkPrecedenceConstraint(curSII, curStart, oldSII, oldStart);
        }

        return true;
    }

    /**
     * Check if there are any precedence conflicts between currently and previously scheduled instances.
     *
     * @param curSII   currently scheduled stream instance
     * @param curStart current start time
     * @param oldSII   previously scheduled stream instance
     * @param oldStart previous start time
     * @return true in case no constraint violation was found
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    protected boolean checkPrecedenceConstraint(StreamInstanceInfo curSII, int curStart, StreamInstanceInfo oldSII, int oldStart) throws ArrayNotInitializedException {
        // if true cur must finish with all delays before old starts else otherwise
        int nodeLag, duration;
        if (curSII.getIDinStreamList() < oldSII.getIDinStreamList()) {
            nodeLag = getProblemInstance().getNodes().get(curSII.getCurSI().getLink().getTo()).getTimeLag();
            duration = curSII.getCurSI().getReSIInHP(0).getDurationWithTimeLag();
            return curStart + duration + nodeLag <= oldStart;
        } else {
            nodeLag = getProblemInstance().getNodes().get(oldSII.getCurSI().getLink().getTo()).getTimeLag();
            duration = oldSII.getCurSI().getReSIInHP(0).getDurationWithTimeLag();
            return oldStart + duration + nodeLag <= curStart;
        }
    }

    /**
     * Check if there are any precedence conflicts between currently and previously scheduled instances.
     *
     * @param curSII   currently scheduled stream instance
     * @param curStart current start time
     * @param oldSII   previously scheduled stream instance
     * @param oldStart previous start time
     * @return true in case no constraint violation was found
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    protected boolean checkLinkConstraint(StreamInstanceInfo curSII, int curStart, StreamInstanceInfo oldSII, int oldStart) throws ArrayNotInitializedException {
        int curHPStart, curHPend, oldHPStart, oldHPend;
        int curPeriod = curSII.getCurSI().getStream().getPeriod();
        int oldPeriod = oldSII.getCurSI().getStream().getPeriod();
        int curDuration = curSII.getCurSI().getReSIInHP(0).getDuration();
        int oldDuration = oldSII.getCurSI().getReSIInHP(0).getDuration();

        curHPStart = curStart;
        curHPend = curStart + curDuration;
        oldHPStart = oldStart;
        oldHPend = oldStart + oldDuration;
        // if periods are same - check only in hpID = 0 otherwise check all combinations
        if (curSII.getCurSI().getStream().getPeriod() == oldSII.getCurSI().getStream().getPeriod()) {
            return (curHPend <= oldHPStart || oldHPend <= curHPStart);
        } else {
            for (int curHPid = 0; curHPid < curSII.getCurSI().getNumInHP(); curHPid++) {
                oldHPStart = oldStart;
                oldHPend = oldStart + oldDuration;
                for (int oldHPid = 0; oldHPid < oldSII.getCurSI().getNumInHP(); oldHPid++) {
                    if (!(curHPend <= oldHPStart || oldHPend <= curHPStart)) return false;
                    oldHPStart += oldPeriod;
                    oldHPend += oldPeriod;
                }
                curHPStart += curPeriod;
                curHPend += curPeriod;
            }
        }
        return true;
    }

    /**
     * Returns the priority queue based on which the streams or stream instances are scheduled.
     *
     * @return The priority queue of objects
     * @throws RuntimeException             In case this method was not overridden by child class
     * @throws ArrayNotInitializedException In case of faulty problem instance initialization
     * @throws UtilizationException         in case of resource utilization was exceeded
     */
    protected PriorityQueue<ObjectPriorities> getPQ() throws RuntimeException, ArrayNotInitializedException, UtilizationException {
        throw new RuntimeException("getPQ() must be overridden");
    }

    /**
     * Method that schedules the provided problem instance.
     *
     * @param pQ Priority queue of objects based on which the schedule is made
     * @return True if the schedule was found
     * @throws RuntimeException             In case this method was not overridden by child class
     * @throws ArrayNotInitializedException In case of faulty problem instance initialization
     * @throws ImplementationException      In case of faulty implementation
     * @throws TimeoutException             In case time limit was exceeded
     */
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws RuntimeException, ArrayNotInitializedException, ImplementationException, TimeoutException {
        throw new RuntimeException("run() must be overridden");
    }

    /**
     * Updates priority queue base on next object.
     *
     * @param pQ   The priority queue
     * @param next The object
     * @throws RuntimeException             In case this method was not overridden by child class
     * @throws ArrayNotInitializedException In case of faulty problem instance initialization
     * @throws ImplementationException      In case of faulty implementation
     */
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) throws RuntimeException, ArrayNotInitializedException, ImplementationException {
        throw new RuntimeException("updatePQ() must be overridden if being used");
    }


    /**
     * Main method that runs all of the heuristics - creates priority queue and then creates a schedule - all
     * heuristics must override these two methods. Each method is run in its own thread and stopped after
     * the time limit is reached.
     *
     * @param timeLimit The time for which the heuristics should run.
     */
    @Override
    public void solve(int timeLimit) throws ArrayNotInitializedException, ImplementationException, UtilizationException {
        long start = System.nanoTime();

        ExecutorService executor = Executors.newCachedThreadPool();
        Callable<Object> task = () -> {
            PriorityQueue<ObjectPriorities> pQ = getPQ();
            return schedule(pQ);
        };
        Object result;
        Future<Object> future = executor.submit(task);

        try {
            result = future.get(timeLimit, TimeUnit.SECONDS);
        } catch (TimeoutException ex) {
            result = false;
        } catch (InterruptedException | ExecutionException e) {
            if (e.getCause() instanceof ArrayNotInitializedException) {
                throw (ArrayNotInitializedException) e.getCause();
            } else if (e.getCause() instanceof UtilizationException) {
                throw (UtilizationException) e.getCause();
            }
            throw new ImplementationException("Problems in solverMethods solve method - see cause", e);
        } finally {
            future.cancel(true);
        }

        try {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new ImplementationException("Could not stop the thread pool in heuristic method");
        }

        setElapsedTime((System.nanoTime() - start) / (1.0 * (Math.pow(10, 9))));
        setSolution(new Solution(problemInstance, getAlgorithmType(), (boolean) result, getElapsedTime()));
    }

    /**
     * Creates empty schedule for the links.
     */
    private void linkScheduleInit() {
        linkSchedule = new ArrayList<>();
        for (Link link : problemInstance.getLinks()) {
            LinkedList<Integer> freeSlotStarts = new LinkedList<>();
            LinkedList<Integer> freeSlotEnds = new LinkedList<>();
            freeSlotStarts.add(0);
            freeSlotEnds.add(problemInstance.getHP());
            linkSchedule.add(new LinkTimeSlots(freeSlotStarts, freeSlotEnds, link));
        }
    }

    /**
     * Removes free time slots that have just been scheduled.
     *
     * @param startInFirstPeriod Start time of the stream instance
     * @param fli                The stream instance
     * @param duration           Duration of the stream instance on the given link
     * @throws ArrayNotInitializedException In case of framework error
     * @throws ImplementationException      In case the time slot does not exist
     */
    private void markStreamInstanceScheduled(int startInFirstPeriod, StreamInstance fli, int duration) throws ArrayNotInitializedException, ImplementationException {
        LinkTimeSlots lts = linkSchedule.get(fli.getLink().getId());

        for (int i = 0; i < fli.getNumInHP(); i++) {
            lts.removeFreeTimeSlot(startInFirstPeriod + i * fli.getStream().getPeriod(), duration);
            fli.getReSIInHP(i).setStart(startInFirstPeriod + i * fli.getStream().getPeriod());
        }
    }

    // General methods

    /**
     * Schedules priority queue of streams.
     *
     * @param pQ The priority queue of streams
     * @return true in case the schedule was found
     * @throws ArrayNotInitializedException In case of framework error
     * @throws ImplementationException      In case of child method error
     * @throws TimeoutException             In case the time limit was exceeded
     */
    protected boolean scheduleStreamsFirstFit(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        int startTime = -1;
        int total = pQ.size();
        int progress = 0;
        int earlStart, duration;
        Link link;

        while (!pQ.isEmpty()) {
            Stream stream = (Stream) pQ.poll().getObject();
            earlStart = 0;

            for (StreamInstance fli : stream.getStreamInstanceList()) {
                if (Thread.currentThread().isInterrupted()) {
                    throw new TimeoutException("Time limit exceeded!");
                }
                link = fli.getLink();
                duration = fli.getReSIInHP(0).getDurationWithTimeLag();
                startTime = linkSchedule.get(link.getId()).getFirstAllFitSlotID(duration, fli.getStream().getPeriod(),
                        fli.getNumInHP(), earlStart, stream.getDeadline(), stream.getRelease());

                if (startTime == -1) {
                    System.out.println("Scheduling progress: " + progress + "/" + total + " streams");
                    System.out.println("----------> Heuristics " + this.getAlgorithmType().name() + " didn't find solution for stream and stream instance: " + stream.getName() + "/" + fli.getLink());
                    break;
                } else {
                    markStreamInstanceScheduled(startTime, fli, duration);
                }

                earlStart = fli.getReSIInHP(0).getEnd() + problemInstance.getNodes().get(link.getTo()).getTimeLag();
            }

            if (startTime == -1) break;
            progress++;
        }
        return startTime != -1;
    }

    /**
     * Schedules priority queue of streams.
     *
     * @param pQ The priority queue of streams
     * @return true in case the schedule was found
     * @throws ArrayNotInitializedException In case of framework error
     * @throws ImplementationException      In case of child method error
     * @throws TimeoutException             In case the time limit was exceeded
     */
    protected boolean scheduleStreamInstancesFirstFit(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        int startTime = -1;
        int total = pQ.size();
        int progress = 0;
        int earliestUSStartTime, duration;
        StreamInstanceInfo sii;

        while (!pQ.isEmpty()) {
            if (Thread.currentThread().isInterrupted()) {
                throw new TimeoutException("Time limit exceeded!");
            }
            sii = (StreamInstanceInfo) pQ.poll().getObject();
            earliestUSStartTime = Math.max(sii.getEarliestStart(), sii.getCurSI().getStream().getRelease());
            Link link = sii.getCurSI().getLink();

            duration = sii.getCurSI().getReSIInHP(0).getDurationWithTimeLag();
            startTime = linkSchedule.get(link.getId()).
                    getFirstAllFitSlotID(duration, sii.getCurSI().getStream().getPeriod(),
                            sii.getCurSI().getNumInHP(), earliestUSStartTime,
                            sii.getCurSI().getStream().getDeadline(), sii.getCurSI().getStream().getRelease());

            if (startTime == -1 || startTime > sii.getLatestStart()) {
                startTime = -1;
                System.out.println("Scheduling progress: " + progress + "/" + total + " streams");
                System.out.println("------------> Heuristics " + this.getAlgorithmType().name() + " didn't find solution for stream and stream instance: " + sii.getCurSI().getStream().getName() + "/" + sii.getCurSI().getLink());
                break;
            } else {
                markStreamInstanceScheduled(startTime, sii.getCurSI(), duration);
                updateTimeIntervalsForOtherStreamInstances(pQ, sii);
            }
            progress++;
        }
        return startTime != -1;
    }

    /**
     * Updates earliest and latest start time of the dependent stream instances
     *
     * @param pQ  The priority queue containing the stream instances
     * @param sii The currently scheduled stream instance
     * @throws ArrayNotInitializedException In case of child method error
     * @throws ImplementationException      In case of child method error
     */
    private void updateTimeIntervalsForOtherStreamInstances(PriorityQueue<ObjectPriorities> pQ, StreamInstanceInfo sii) throws ArrayNotInitializedException, ImplementationException {
        int earlStart, nodeTimeLag;
        int prevLatestStart;
        int fliIndex = sii.getIDinStreamList();
        StreamInstanceInfo cur, prev;
        List<StreamInstance> streamInstanceList = sii.getCurSI().getStream().getStreamInstanceList();

        nodeTimeLag = problemInstance.getNodes().get(sii.getCurSI().getLink().getTo()).getTimeLag();
        earlStart = sii.getCurSI().getReSIInHP(0).getEnd() + nodeTimeLag;

        // update earliest start times of followers
        cur = sii.getNextSII();
        for (int i = fliIndex + 1; i < streamInstanceList.size(); i++) {
            if (cur.getCurSI().getReSIInHP(0).getStart() == -1) {
                cur.updateEarliestStart(earlStart);
                updatePQ(pQ, cur);
            }
            nodeTimeLag = problemInstance.getNodes().get(sii.getCurSI().getLink().getTo()).getTimeLag();
            earlStart += (sii.getCurSI().getReSIInHP(0).getDurationWithTimeLag() + nodeTimeLag);
            cur = cur.getNextSII();
        }

        // update latest times of predecessors
        if (fliIndex != 0) {
            cur = sii;
            prev = cur.getPrevSII();

            for (int i = fliIndex; i >= 1; i--) {
                nodeTimeLag = problemInstance.getNodes().get(sii.getCurSI().getLink().getTo()).getTimeLag();
                prevLatestStart = cur.getCurSI().getReSIInHP(0).getStart()
                        - (nodeTimeLag + prev.getCurSI().getReSIInHP(0).getDurationWithTimeLag());
                prev.updateLatestStart(prevLatestStart);
                prev = cur.getPrevSII();
                cur = prev;
            }
        }
    }

    /**
     * Removes stream instance from the priority queue and returns its index in the stream list.
     *
     * @param pQ      Priority queue
     * @param sii     The stream instance to be removed
     * @param current The object to be removed
     * @return The stream instance index
     * @throws ImplementationException In case the object was not found
     */
    protected int getFliIndexAndRemoveFromPQ(PriorityQueue<ObjectPriorities> pQ, StreamInstanceInfo sii, ObjectPriorities current) throws ImplementationException {
        int fliIndex;
        pQ.remove(current);

        fliIndex = -1;
        for (int i = 0; i < sii.getCurSI().getStream().getStreamInstanceList().size(); i++) {
            if (sii.getCurSI().getStream().getStreamInstanceList().get(i).equals(sii.getCurSI())) {
                fliIndex = i;
            }
        }

        if (fliIndex == -1) {
            throw new ImplementationException("ERROR in " + getClass().getName() + " solverMethods updatePQ - cannot find item in flowList");
        }
        return fliIndex;
    }

    /**
     * Initializes list of all stream instances in the network and calculates earliest and latest start times
     *
     * @return list of all stream instances with additional information
     * @throws ArrayNotInitializedException In case of faulty framework initialization
     */
    public List<StreamInstanceInfo> initializeStreamInfoList() throws ArrayNotInitializedException {
        int earliestStart;
        StreamInstanceInfo prev;
        List<StreamInstanceInfo> siiList = new ArrayList<>();
        int numSI;
        int latestStart;
        int firstCurSII;
        for (Stream stream : getProblemInstance().getStreams()) {
            earliestStart = stream.getRelease();
            prev = null;
            numSI = stream.getStreamInstanceList().size();

            for (int i = 0; i < numSI; i++) {
                StreamInstanceInfo sii = getNewStreamInstanceInfo(prev, stream, i, earliestStart);
                prev = sii;
                siiList.add(sii);
                earliestStart += sii.getCurSI().getReSIInHP(0).getDurationWithTimeLag() +
                        getProblemInstance().getNodes().get(sii.getCurSI().getLink().getTo()).getTimeLag();
            }

            latestStart = stream.getDeadline() - stream.getStreamInstanceList().get(numSI - 1).getReSIInHP(0).getDurationWithTimeLag();
            firstCurSII = siiList.size() - numSI;
            for (int i = numSI - 1; i >= 0; i--) {
                StreamInstanceInfo curSII = siiList.get(firstCurSII + i);
                curSII.setLatestStart(latestStart);
                if (i != 0) {
                    latestStart -= (getProblemInstance().getNodes().get(curSII.getCurSI().getLink().getFrom()).getTimeLag()
                            + curSII.getPrevSII().getCurSI().getReSIInHP(0).getDurationWithTimeLag());
                }
            }
        }
        return siiList;
    }

    /**
     * Creates new stream instance info object containing the stream instance and additional information like earliest and latest start time.
     *
     * @param prev          The predecessor stream instance
     * @param stream        The stream
     * @param i             Id of the stream instance in the stream list
     * @param earliestStart Earliest start of the stream instance
     * @return the newly created stream instance info
     */
    private StreamInstanceInfo getNewStreamInstanceInfo(StreamInstanceInfo prev, Stream stream, int i, int earliestStart) {
        StreamInstance fli = stream.getStreamInstanceList().get(i);
        StreamInstanceInfo sii = new StreamInstanceInfo(fli, getProblemInstance().getHP(), prev, null, i, earliestStart);
        if (prev != null) {
            prev.setNextSII(sii);
        }
        return sii;
    }
}
