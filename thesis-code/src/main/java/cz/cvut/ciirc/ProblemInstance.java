package cz.cvut.ciirc;

import cz.cvut.ciirc.data_format.InputFile;
import cz.cvut.ciirc.data_format.LinkList;
import cz.cvut.ciirc.data_format.NodeList;
import cz.cvut.ciirc.data_format.StreamList;
import cz.cvut.ciirc.exceptions.*;
import cz.cvut.ciirc.helper.StaticMath;
import cz.cvut.ciirc.network_and_traffic_model.*;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class that represents the solved instance. Contains all classes described in network_and_traffic_model package and
 * methods manipulating with them. Also contains several convenient getter methods.
 */

@Getter
public class ProblemInstance {
    /**
     * Hyper period of the streams
     */
    private int HP;
    /**
     * ID of the instance
     */
    private int instanceID;
    /**
     * Links in the network
     */
    private List<Link> links;
    /**
     * NodesInfoPanel in the network
     */
    private List<Node> nodes;
    /**
     * Streams send in the network
     */
    private List<Stream> streams;
    /**
     * List of stream instances
     */
    private List<StreamInstance> streamInstances;

    /**
     * Average utilization over all links in the network
     */
    private double avgUtilization = -1;
    /**
     * Maximal utilization over all links in the network
     */
    private double maxUtilization = -1;

    /**
     * Default constructor, initializes the problem instance.
     *
     * @param inputProto Proto class containing information about the instance to be scheduled
     * @throws CustomException In case of initialization error, see instanceInit for more details
     */
    public ProblemInstance(InputFile.InputFileProto inputProto) throws CustomException {
        instanceID = inputProto.getId();
        links = new ArrayList<>();
        streams = new ArrayList<>();
        streamInstances = new ArrayList<>();
        nodes = new ArrayList<>();
        instanceInit(inputProto);
    }

    /**
     * Converts data in proto classes to custom classes defined in network_and_traffic_model package,
     * sets up the network graph, counts streams hyper period and creates routing for streams.
     *
     * @param inputProto Proto class containing information about the instance to be scheduled
     * @throws CustomException In case the instance could not be initialized correctly, typically error caused by incorrectly defined input data file.
     */
    private void instanceInit(InputFile.InputFileProto inputProto) throws CustomException {
        readNodesProto(inputProto.getNodes());
        readLinksProto(inputProto.getLinks());
        setUpNodeNeighbors();

        readStreamsProto(inputProto.getStreams());
        HP = StaticMath.listLCM(streams.stream().map(Stream::getPeriod).collect(Collectors.toList()));

        createRouting();
        createReoccurredStreamInstances();
    }

    /**
     * Converts Proto class to inner model representation.
     *
     * @param nodeListProto Proto representing network nodes
     */
    private void readNodesProto(NodeList.NodeListProto nodeListProto) {
        int nodeID = 0;
        for (NodeList.NodeProto nodeProto : nodeListProto.getNodeList()) {
            nodes.add(new Node(nodeProto.getName(), nodeID, nodeProto.getTimeLag()));
            nodeID++;
        }
    }

    /**
     * Converts Proto class to inner model representation.
     *
     * @param linkListProto Proto representing network links
     */
    private void readLinksProto(LinkList.LinkListProto linkListProto) throws InvalidInputDataException, ArrayNotInitializedException {
        int linkID = 0;
        int target, origin;
        for (LinkList.LinkProto linkProto : linkListProto.getLinkList()) {
            origin = getNodeIDFromString(linkProto.getOrigin());
            target = getNodeIDFromString(linkProto.getTarget());
            links.add(new Link(linkID, linkProto.getName(), origin, target, linkProto.getWeight(), linkProto.getTimeLag(), linkProto.getOrigin() + " -> " + linkProto.getTarget()));
            linkID++;
            links.add(new Link(linkID, linkProto.getName(), target, origin, linkProto.getWeight(), linkProto.getTimeLag(), linkProto.getTarget() + " -> " + linkProto.getOrigin()));
            linkID++;
        }
    }

    /**
     * Converts Proto class to inner model representation.
     *
     * @param streamListProto Proto representing network communication
     */
    private void readStreamsProto(StreamList.StreamListProto streamListProto) throws InvalidInputDataException, ArrayNotInitializedException {
        int streamID = 0;
        Node target, origin;
        for (StreamList.StreamProto streamProto : streamListProto.getStreamList()) {
            origin = nodes.get(getNodeIDFromString(streamProto.getOrigin()));
            target = nodes.get(getNodeIDFromString(streamProto.getTarget()));
            streams.add(new Stream(this, streamID, streamProto.getName(), origin,
                    target, streamProto.getDuration(), streamProto.getPeriod(),
                    streamProto.getRelease(), streamProto.getDeadline()));
            streamID++;
        }
    }

    /**
     * Counts utilization of each link in the network, calculates maximal and average utilization.
     *
     * @throws UtilizationException In case the utilization exceeded 100 % for some link.
     */
    public double[] getUtilization() throws UtilizationException {
        double[] utilization = new double[links.size()];
        long totalTime;
        long allLinksTime = 0;
        boolean isSchedulable = true;
        List<ReoccurredStreamInstance> friList;

        maxUtilization = 0;

        for (Link link : links) {
            totalTime = 0;
            friList = getAllReoccurredStreamInstancesForLink(link.getId());
            for (ReoccurredStreamInstance fri : friList) {
                totalTime += fri.getDuration();
            }

            utilization[link.getId()] = totalTime / (HP * 1.0);
            allLinksTime += totalTime;

            if (utilization[link.getId()] > maxUtilization) maxUtilization = utilization[link.getId()];
            if (utilization[link.getId()] > 1) isSchedulable = false;
        }

        avgUtilization = allLinksTime / (HP * 1.0 * links.size());

        if (!isSchedulable) {
            StringBuilder utilString = new StringBuilder("---------- Utilization on resources ----------\n");
            for (int j = 0; j < links.size(); j++) {
                utilString.append(links.get(j)).append(": ").append(utilization[j]).append("\n");
            }
            throw new UtilizationException("Instance is not schedulable\n" + utilString.toString());
        }
        return utilization;
    }

    /**
     * Converts node name to node instanceID.
     *
     * @param name Name of the converted node
     * @return Id of the converted node
     * @throws InvalidInputDataException    in case the node list was initialized and does not contain the specified node
     * @throws ArrayNotInitializedException in case the node list was not initialized
     */
    public int getNodeIDFromString(String name) throws InvalidInputDataException, ArrayNotInitializedException {
        int listIndex = -1;
        if (nodes.size() == 0)
            throw new ArrayNotInitializedException("Node array was not initialized (in getNodeIDFromString)");
        for (Node node : nodes) {
            if (name.equals(node.getName())) {
                listIndex = node.getId();
                break;
            }
        }
        if (listIndex == -1) throw new InvalidInputDataException("Node " + name + " not found");
        return listIndex;
    }

    /**
     * Initializes neighbors and connected links of all nodes in the network.
     */
    private void setUpNodeNeighbors() {
        int fromIndex, toIndex;
        for (Link link : links) {
            fromIndex = link.getFrom();
            toIndex = link.getTo();
            nodes.get(fromIndex).addNeighbor(nodes.get(toIndex));
            nodes.get(fromIndex).getConnectedLinksIDs().add(link.getId());
        }
    }

    /**
     * Finds path of all streams in the network.
     * {@inheritDoc}
     */
    private void createRouting() throws StreamRoutingException, ImplementationException, InputIntegerOverFlowException, TopologyInitializationException {
        for (Stream stream : streams) {
            findShortestPaths(stream);
        }
    }

    /**
     * @param stream Stream for which the path is found and stream instances initialized
     * @throws StreamRoutingException          in case the path was not found
     * @throws InputIntegerOverFlowException   in case the stream instances could not be initialized for given stream
     * @throws TopologyInitializationException in case the stream instances could not be initialized for given stream
     * @throws ImplementationException         in case the stream instances could not be initialized for given stream
     */
    private void findShortestPaths(Stream stream) throws StreamRoutingException, InputIntegerOverFlowException, TopologyInitializationException, ImplementationException {
        boolean[] visited = new boolean[nodes.size()];
        Node[] predecessors = new Node[nodes.size()];
        Node toNode = stream.getTarget();
        LinkedList<Node> q = new LinkedList<>();
        boolean pathFound = false;

        q.add(stream.getOrigin());
        visited[stream.getOrigin().getId()] = true;

        while (!q.isEmpty()) {
            Node curNode = q.poll();
            if (curNode == null) break;
            if (curNode.equals(toNode)) {
                pathFound = true;
                break;
            }
            for (Node child : curNode.getNeighbors()) {
                if (!visited[child.getId()]) {
                    visited[child.getId()] = true;
                    predecessors[child.getId()] = curNode;
                    q.add(child);
                }
            }
        }
        if (pathFound) {
            createStreamInstances(toNode, stream, predecessors);
        } else {
            throw new StreamRoutingException("Routing ERROR in findShortestPaths(): path from "
                    + stream.getOrigin() + " to " + toNode + " was not found!");
        }
    }

    /**
     * Creates stream instances for specified stream based on its path
     *
     * @param lastNode     Last node on the path
     * @param stream       Stream for which the sequence of stream instances is initialized
     * @param predecessors List of predecessor of the lastNode
     * @throws InputIntegerOverFlowException   in case Stream instance cannot be created
     * @throws TopologyInitializationException in case link cannot be found based on its origin and target
     * @throws ImplementationException         in case the predecessors list was not initialized properly
     */
    private void createStreamInstances(Node lastNode, Stream stream, Node[] predecessors) throws InputIntegerOverFlowException,
            TopologyInitializationException, ImplementationException {

        Node curNode = lastNode;
        List<Node> path = stream.getPath();

        while (!curNode.equals(stream.getOrigin())) {
            path.add(curNode);
            if (curNode.getId() >= predecessors.length) throw new ImplementationException(
                    "IMPLEMENTATION ERROR: createStreamInstances - indexing predecessors list out of bounds");
            curNode = predecessors[curNode.getId()];
        }

        path.add(curNode);
        Collections.reverse(path);
        Node fromNode = path.get(0);

        for (int i = 1; i < path.size(); i++) {
            List<StreamInstance> fli = stream.getStreamInstanceList();
            Node toNode = path.get(i);
            Link link = getLinkFromNodes(fromNode.getName(), toNode.getName());
            StreamInstance predecessor = null;
            if (fli.size() > 0) {
                predecessor = fli.get(fli.size() - 1);
            }
            StreamInstance streamInstance = new StreamInstance(streamInstances.size(), link, stream, predecessor);
            fli.add(streamInstance);
            streamInstances.add(streamInstance);
            fromNode = toNode;
        }
    }

    /**
     * Creates reoccurred stream instances for all streams.
     */
    private void createReoccurredStreamInstances() {
        for (Stream stream : streams) {
            for (StreamInstance fli : stream.getStreamInstanceList()) {
                fli.createReoccurredStreamInstances();
            }
        }
    }

    /**
     * Returns link that leads form specified origin to specified target
     *
     * @param from Origin node
     * @param to   Target node
     * @return Link of specified features
     * @throws TopologyInitializationException In case link of specified features could not be found in the network
     */
    public Link getLinkFromNodes(String from, String to) throws TopologyInitializationException {
        int fromID = -1;
        int toID = -1;

        for (int i = 0; i < nodes.size(); i++) {
            if (nodes.get(i).getName().equals(from)) {
                fromID = i;
            }
            if (nodes.get(i).getName().equals(to)) {
                toID = i;
            }
            if (toID != -1 && fromID != -1) break;
        }

        if (fromID == -1 || toID == -1) {
            throw new TopologyInitializationException("Node not found -> ProblemInstance -> getLinkFromNodes");
        }

        for (Link link : links) {
            if (link.fromToEquals(fromID, toID)) return link;
        }

        throw new TopologyInitializationException("Link not found: ProblemInstance -> getLinkFromNodes");
    }


    /**
     * Returns list of all reoccurred stream instances in the network
     *
     * @return list of all reoccurred stream instances in the network
     */
    private List<ReoccurredStreamInstance> getAllReoccurredStreamInstances() {
        return streamInstances.stream().map(StreamInstance::getRepStreamInstances).collect(ArrayList::new, List::addAll, List::addAll);
    }

    /**
     * Returns list of all unscheduled reoccurred stream instances in the network
     *
     * @return list of all unscheduled reoccurred stream instances in the network
     */
    public List<ReoccurredStreamInstance> getAllUnscheduledReoccurredStreamInstances() {
        return getAllReoccurredStreamInstances().stream().filter(rsi -> rsi.getStart() == SchedulerConstants.DEFAULT_START_TIME).collect(Collectors.toList());
    }

    /**
     * Returns list of all reoccurred stream instances for given link
     *
     * @return list of all reoccurred stream instances for given link
     */
    public List<ReoccurredStreamInstance> getAllReoccurredStreamInstancesForLink(int linkID) {
        return getAllReoccurredStreamInstances().stream().filter(rsi -> rsi.getParent().getLink().getId() == linkID).collect(Collectors.toList());
    }
}























