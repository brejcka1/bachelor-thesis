package cz.cvut.ciirc.gui;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Represents static class of constants used throughout the GUI.
 */
@SuppressWarnings("SpellCheckingInspection")
public class GUIConstants {
    //source: https://stackoverflow.com/questions/1168260/algorithm-for-generating-unique-colors
    public static final String[] colors = {"#00FF00", "#0000FF", "#FF0000", "#01FFFE", "#FFA6FE", "#FFDB66", "#006401",
            "#010067", "#95003A", "#007DB5", "#FF00F6", "#FFEEE8", "#774D00", "#90FB92", "#0076FF", "#D5FF00",
            "#FF937E", "#6A826C", "#FF029D", "#FE8900", "#7A4782", "#7E2DD2", "#85A900", "#FF0056", "#A42400",
            "#00AE7E", "#683D3B", "#BDC6FF", "#263400", "#BDD393", "#00B917", "#9E008E", "#001544", "#C28C9F",
            "#FF74A3", "#01D0FF", "#004754", "#E56FFE", "#788231", "#0E4CA1", "#91D0CB", "#BE9970", "#968AE8",
            "#BB8800", "#43002C", "#DEFF74", "#00FFC6", "#FFE502", "#620E00", "#008F9C", "#98FF52", "#7544B1",
            "#B500FF", "#00FF78", "#FF6E41", "#005F39", "#6B6882", "#5FAD4E", "#A75740", "#A5FFD2", "#FFB167",
            "#009BFF", "#E85EBE"};

    public static final String APP_NAME = "CIIRC Communication Scheduler";
    public static final Font large = new Font("TimesRoman", Font.PLAIN, 30);
    public static final Font largeBold = new Font("TimesRoman", Font.BOLD, 30);
    public static final Font medium = new Font("TimesRoman", Font.PLAIN, 15);
    public static final Font mediumBold = new Font("TimesRoman", Font.BOLD, 15);
    public static final Font small = new Font("TimesRoman", Font.PLAIN, 12);
    public static final Font smallBold = new Font("TimesRoman", Font.BOLD, 12);


    public static final String lightGreen = "#8bd154";
    public static final String midGreen = "#578335";
    public static final String darkGreen = "#111a0b";

    public static final Color warning = Color.decode(midGreen);


    public static final Border border = BorderFactory.createEmptyBorder(10, 10, 10, 10);

    public static final boolean showLabels = true;
}
