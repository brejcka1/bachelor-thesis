package cz.cvut.ciirc.exceptions;

public class ProgramArgumentsException extends CustomException {

    public ProgramArgumentsException(String s) {
        super(s);
    }

    public ProgramArgumentsException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
