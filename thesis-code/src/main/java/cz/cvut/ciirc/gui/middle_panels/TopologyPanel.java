package cz.cvut.ciirc.gui.middle_panels;

import cz.cvut.ciirc.gui.MiddlePanel;
import cz.cvut.ciirc.gui.middle_panels.middle_components.TopologyView;

import javax.swing.*;
import java.awt.*;

/**
 * Represents one of the screens in the second phase of the GUI.
 * Shows instance topology.
 */
public class TopologyPanel extends JPanel {
    private final MiddlePanel parent;
    private JPanel topologyPanel = null;
    private JPanel panel = null;


    public TopologyPanel(MiddlePanel parent) {
        super(new BorderLayout());
        this.parent = parent;
        init();
    }

    private void init() {
        topologyPanel = new JPanel(new BorderLayout());
        add(topologyPanel, BorderLayout.CENTER);
    }

    public void updatePanel() {
        if (panel == null) {
            panel = new TopologyView(parent.getSolution().getProblemInstance());
            topologyPanel.add(panel);
        } else {
            topologyPanel.remove(panel);

            panel = new TopologyView(parent.getSolution().getProblemInstance());
            topologyPanel.add(panel);
            topologyPanel.validate();
            topologyPanel.repaint();
        }
    }

}
