package cz.cvut.ciirc;

import cz.cvut.ciirc.exceptions.CustomException;
import cz.cvut.ciirc.exceptions.InputFileNotFoundException;
import cz.cvut.ciirc.exceptions.SchedulingConstraintsException;
import cz.cvut.ciirc.helper.ProtoHandler;
import cz.cvut.ciirc.scheduling.Solution;
import cz.cvut.ciirc.scheduling.Statistics;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

/**
 * Main class to be run for larger experiments on server.
 * Allows to solve several folders with instances in parallel.
 * Before each run, the class attributes should be revised and changed based on current needs.
 */
public class ParallelScheduler {
    /**
     * Number of threads that program runs on (number of directories that are solved in parallel).
     */
    private static final int numThreads = 20;

    /**
     * ID of first file in the folders to be scheduled. Typically is 0.
     */
    private static final int firstInstance = 0;

    /**
     * ID of last file in the folders to be scheduled. Typically is equal to number of instances in the folder minus one.
     */
    private static final int lastInstance = 99;
    /**
     * True if schedules are intended to be saved during the experiment. Typically not necessary, set to false.
     */
    private static final boolean saveSchedules = false;
    /**
     * ID of first directory to be scheduled.
     */
    private static int firstDir = 6000;
    /**
     * ID of last directory to be scheduled.
     */
    private static int lastDir = 6540;
    /**
     * List of algorithms to be run on server.
     */
    private static AlgorithmType[] algorithms = SchedulerConstants.solverMethods;

    /**
     * Main class that initializes the thread pool and runs thread function for each thread.
     *
     * @param args Should be empty, are not parsed
     */
    public static void main(String[] args) {
        firstDir = Integer.parseInt(args[0]);
        lastDir = Integer.parseInt(args[1]);

        long start = System.nanoTime();

        final BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(lastDir - firstDir);
        for (int dirID = firstDir; dirID < lastDir; dirID++) {
            queue.add(dirID);
        }

        ExecutorService pool = Executors.newFixedThreadPool(numThreads);

        for (int i = 0; i < numThreads; i++) {
            final int threadID = i;
            Runnable r = () -> {
                while (!queue.isEmpty()) {
                    threadSchedule(start, threadID, queue.poll());
                }
            };
            pool.execute(r);
        }
        pool.shutdown();

        try {
            pool.awaitTermination(60, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Duration of solving: " + ((System.nanoTime() - start) / (Math.pow(10, 9) * 1.0)) / 3600.0 + " h");
    }

    /**
     * Method that runs each thread. It initializes paths, runs scheduler for all instances in the folder and saves statistics.
     *
     * @param start    System start time of the program
     * @param threadID ID of current thread
     * @param dirID    Directory ID of instances being scheduled
     */
    private static void threadSchedule(long start, int threadID, int dirID) {
        ArrayList<Statistics> statistics = new ArrayList<>();
        Solution solution;

        try {
            final Path statsPath = ProtoHandler.getStatsPath(dirID);
            List<AlgorithmType> algorithmTypes = new ArrayList<>();
            algorithmTypes.addAll(Arrays.asList(algorithms));

            Statistics.saveHeader(statsPath, algorithmTypes, dirID);
            for (int instanceID = firstInstance; instanceID <= lastInstance; instanceID++) {
                final Path resultsPath = ProtoHandler.getResultFilePath(dirID, instanceID);
                System.out.println("\n------------------> Scheduling instance: " + instanceID + "/" + lastInstance +
                        " from folder: " + dirID + " by thread: " + threadID + " time up: " +
                        (((System.nanoTime() - start) / (Math.pow(10, 9) * 1.0)) / 60.0) + " min");
                try {
                    Scheduler scheduler = new Scheduler(true);
                    scheduler.schedule(statistics, algorithmTypes, statsPath, instanceID, dirID);
                    solution = scheduler.getBestSolution();

                    if (saveSchedules) {
                        ProtoHandler.writeProto(resultsPath, solution.getOutputProto());
                    }

                } catch (CustomException e) {
                    if (!(e instanceof InputFileNotFoundException)) {
                        System.err.println("\n------------------> Scheduling instance: " + instanceID + "/" + lastInstance +
                                " from folder: " + dirID + " by thread: " + threadID + " time up: " +
                                (((System.nanoTime() - start) / (Math.pow(10, 9) * 1.0)) / 60.0) + " min");
                        throw e;
                    }
                }
            }
            Statistics.saveTotalStats(statistics, algorithmTypes, statsPath);
        } catch (CustomException e) {
            e.printStackTrace();
            if ((e instanceof SchedulingConstraintsException)) {
                System.err.println("Scheduling constraint violation detected");
                System.exit(-1);
            }
        }
    }
}
