package cz.cvut.ciirc.gui.middle_panels.middle_components;

import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.gui.GUIConstants;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import cz.cvut.ciirc.network_and_traffic_model.Node;
import cz.cvut.ciirc.network_and_traffic_model.ReoccurredStreamInstance;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.scheduling.Solution;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Main class to visually represent the Gantt chart.
 * Inspired by: https://stackoverflow.com/a/40305146
 */
public class IntervalChart {
    private final JFreeChart chart;
    private final ProblemInstance problemInstance;
    private String[] symbolAxisContent;

    public IntervalChart(Solution solution) {
        problemInstance = solution.getProblemInstance();
        XYIntervalSeriesCollection dataset = createXYIntervalDataset(problemInstance);

        chart = createIntervalStackedChart(dataset);
        chart.getLegend().setItemFont(GUIConstants.small);


        TextTitle t = new TextTitle(
                "Solver: " + solution.getSolverType() +
                        "     Elapsed time: " + (int) Math.ceil(solution.getElapsedTime()) + " s" +
                        "     Objective: " + solution.getObjValue() +
                        "     |     HP: " + solution.getProblemInstance().getHP() + " μs" +
                        "     Nodes count: " + solution.getProblemInstance().getNodes().size() +
                        "     Streams count: " + solution.getProblemInstance().getStreams().size() +
                        "     |     instance: " + solution.getProblemInstance().getInstanceID());
        t.setFont(GUIConstants.medium);
        chart.addSubtitle(t);
    }

    /**
     * Returns initialized chart panel.
     *
     * @return initialized chart panel
     */
    public JPanel getPanel() {
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setMaximumDrawHeight(3000);
        chartPanel.setMaximumDrawWidth(3000);
        chartPanel.setMinimumDrawHeight(100);
        chartPanel.setMinimumDrawWidth(100);
        return chartPanel;
    }

    /**
     * Sets up the chart.
     *
     * @return The created chart
     */
    private JFreeChart createIntervalStackedChart(XYIntervalSeriesCollection dataset) {
        XYBarRenderer xyRend = new XYBarRenderer();

        xyRend.setShadowVisible(false);
        xyRend.setUseYInterval(true);
        xyRend.setBarPainter(new StandardXYBarPainter());

        if (GUIConstants.showLabels) {
            xyRend.setDefaultItemLabelGenerator(new StandardXYItemLabelGenerator("{0}: {2}"));
            xyRend.setDefaultItemLabelsVisible(true);

            final ItemLabelPosition p = new ItemLabelPosition(
                    ItemLabelAnchor.OUTSIDE11, TextAnchor.BOTTOM_LEFT,
                    TextAnchor.TOP_LEFT, -Math.PI / 100
            );
            xyRend.setDefaultPositiveItemLabelPosition(p);

        }

        for (int i = 0; i < dataset.getSeriesCount(); i++) {
            xyRend.setSeriesPaint(i, Color.decode(GUIConstants.colors[i % GUIConstants.colors.length]));
        }

        ValueAxis dateAxis = new NumberAxis();
        dateAxis.setVerticalTickLabels(true);
        dateAxis.setLabel("Time [μs]");

        XYPlot plot = new XYPlot(dataset, new SymbolAxis("", symbolAxisContent), dateAxis, xyRend);
        plot.setOrientation(PlotOrientation.HORIZONTAL);
        plot.setBackgroundPaint(Color.LIGHT_GRAY);
        plot.getRangeAxis().setRange(0, problemInstance.getHP() + 10);
        plot.addRangeMarker(new ValueMarker(0));
        plot.addRangeMarker(new ValueMarker(problemInstance.getHP()));

        plot.getDomainAxis().setLabel("Link");

        xyRend.setDefaultItemLabelFont(GUIConstants.small);
        plot.getRangeAxis().setTickLabelFont(GUIConstants.medium);
        plot.getDomainAxis().setTickLabelFont(GUIConstants.medium);
        plot.getRangeAxis().setLabelFont(GUIConstants.medium);
        plot.getDomainAxis().setLabelFont(GUIConstants.medium);
        return new JFreeChart(plot);
    }

    /**
     * Initializes dataset.
     *
     * @param problemInstance Solved problem instance.
     * @return Created dataset
     */
    private XYIntervalSeriesCollection createXYIntervalDataset(ProblemInstance problemInstance) {
        XYIntervalSeriesCollection dataset = new XYIntervalSeriesCollection();
        List<String> states = problemInstance.getStreams().stream().map(Stream::getName).collect(Collectors.toList());
        XYIntervalSeries[] series = new XYIntervalSeries[problemInstance.getStreams().size()];
        symbolAxisContent = new String[problemInstance.getLinks().size()];

        for (int i = 0; i < states.size(); i++) {
            series[i] = new XYIntervalSeries(states.get(i));
            dataset.addSeries(series[i]);
        }

        List<ReoccurredStreamInstance> linkSchedule;
        List<Node> nodes = problemInstance.getNodes();


        for (Link link : problemInstance.getLinks()) {
            linkSchedule = problemInstance.getAllReoccurredStreamInstancesForLink(link.getId());
            linkSchedule.sort(Comparator.comparingInt(ReoccurredStreamInstance::getStart));
            symbolAxisContent[link.getId()] = nodes.get(link.getFrom()) + " -> " + nodes.get(link.getTo());
            for (ReoccurredStreamInstance reStreamInst : linkSchedule) {
                series[reStreamInst.getParent().getStream().getId()]
                        .add(link.getId(), link.getId() - 0.2, link.getId() + 0.2,
                                reStreamInst.getStart(), reStreamInst.getStart(), reStreamInst.getEnd());
            }

        }

        return dataset;
    }
}