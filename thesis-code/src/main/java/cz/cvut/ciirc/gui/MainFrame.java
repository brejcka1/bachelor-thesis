package cz.cvut.ciirc.gui;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.Scheduler;
import cz.cvut.ciirc.exceptions.CustomException;
import cz.cvut.ciirc.helper.ProtoHandler;
import cz.cvut.ciirc.scheduling.Solution;
import cz.cvut.ciirc.scheduling.Statistics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static javax.swing.JOptionPane.showMessageDialog;


/**
 * Main frame representing all the GUI features. Consists of initialization methods and action performers.
 */
class MainFrame extends JFrame {
    private final Path instancePath;
    private TopPanel topPanel;
    private MiddlePanel middlePanel;
    private BottomPanel bottomPanel;
    private int dirID = -1;
    private int fileID = -1;

    MainFrame() throws HeadlessException {
        super(GUIConstants.APP_NAME);
        instancePath = Paths.get(System.getProperty("user.dir"), "..", "instances");
        basicInit();
    }

    void run() {
        //startWithInstanceShowing(1, 0, AlgorithmType.EDF_MRT_FF);
        setVisible(true);
    }

    private void basicInit() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 500);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        componentsInit();
        addActionListeners();
    }

    private void componentsInit() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        topPanel = new TopPanel();
        middlePanel = new MiddlePanel(instancePath);
        bottomPanel = new BottomPanel();

        cp.add(topPanel, BorderLayout.PAGE_START);
        cp.add(middlePanel, BorderLayout.CENTER);
        cp.add(bottomPanel, BorderLayout.PAGE_END);
    }

    private void startWithInstanceShowing(int dirID, int fileID, AlgorithmType algorithmType) {
        try {
            Solution solution = runScheduler(dirID, fileID, algorithmType);

            middlePanel.setSolution(solution);
            middlePanel.switchToInstancePanel();
            bottomPanel.showSecondPhaseButtons();
        } catch (Exception ex) {
            showMessageDialog(null, ex.getMessage());
            if (!(ex instanceof CustomException)) ex.printStackTrace();
        }
    }

    private void addActionListeners() {
        middlePanel.getFilePanel().getJfc().addActionListener((ActionEvent evt) -> {
            fileChooserActionPerformed(evt);
            bottomPanel.showFirstPhaseButtons(true);
        });
        bottomPanel.getTopologyBtn().addActionListener((ActionEvent evt) -> middlePanel.switchToInstancePanel());
        bottomPanel.getScheduleBtn().addActionListener((ActionEvent evt) -> middlePanel.switchToSchedulePanel());
        bottomPanel.getInfoBtn().addActionListener((ActionEvent evt) -> middlePanel.switchToStatisticsPanel());
        bottomPanel.getSolveBtn().addActionListener((ActionEvent evt) -> solverChooserActionPerformed());
        bottomPanel.getResetBtn().addActionListener((ActionEvent evt) -> resetActionPerformed());
        bottomPanel.getResetSolverBtn().addActionListener((ActionEvent evt) -> resetSolverActionPerformed());
    }

    private void resetSolverActionPerformed() {
        bottomPanel.showFirstPhaseButtons(true);

        middlePanel.getSolverPanel().getTeamGroup().clearSelection();
        middlePanel.switchToSolverPanel();
    }

    private void resetActionPerformed() {
        bottomPanel.showFirstPhaseButtons(false);
        middlePanel.getFilePanel().getJfc().setCurrentDirectory(instancePath.toFile());
        middlePanel.getSolverPanel().getTeamGroup().clearSelection();


        middlePanel.switchToFilePanel();
    }

    private void solverChooserActionPerformed() {
        AlgorithmType selectedAlgorithm = middlePanel.getSolverPanel().getSelected();
        if (selectedAlgorithm == null) {
            showMessageDialog(null, "Solver must be selected.");
        } else {
            startWithInstanceShowing(dirID, fileID, selectedAlgorithm);
        }
    }

    private void fileChooserActionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(javax.swing.JFileChooser.APPROVE_SELECTION)) {
            Path selectedFile = Paths.get(middlePanel.getFilePanel().getJfc().getSelectedFile().getPath());
            if (!containsCorrectFileName(selectedFile.getFileName().toString())) {
                showMessageDialog(null, "Correct input file \"instance_ID.pb\" must be selected.");
            } else {
                dirID = Integer.parseInt(selectedFile.getParent().toString().split("_")[1]);
                String secondPart = selectedFile.getFileName().toString().split("_")[1];
                fileID = Integer.parseInt(secondPart.split(".pb")[0]);
                middlePanel.switchToSolverPanel();
            }
        } else if (e.getActionCommand().equals(javax.swing.JFileChooser.CANCEL_SELECTION)) {
            middlePanel.getFilePanel().setFolder();
        }
    }

    private boolean containsCorrectFileName(String fileName) {
        return fileName.contains(".pb");
    }

    private Solution runScheduler(int dirID, int fileID, AlgorithmType algorithmType) throws Exception {
        ArrayList<Statistics> stats = new ArrayList<>();
        List<AlgorithmType> algorithms = new ArrayList<>();
        algorithms.add(algorithmType);

        Scheduler scheduler = new Scheduler();

        Path statsPath = ProtoHandler.getStatsPath(dirID);
        Path resPath = ProtoHandler.getResultFilePath(dirID, fileID);

        Solution solution = scheduler.schedule(stats, algorithms, statsPath, fileID, dirID);
        ProtoHandler.writeProto(resPath, solution.getOutputProto());

        return solution;
    }
}
