package cz.cvut.ciirc.gui.middle_panels.middle_components;

import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.gui.GUIConstants;
import cz.cvut.ciirc.network_and_traffic_model.Link;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

import static cz.cvut.ciirc.gui.GUIConstants.smallBold;

public class LinksInfoPanel extends JScrollPane {

    public LinksInfoPanel(ProblemInstance problemInstance) {
        JTable jTable;

        DefaultTableModel model = new DefaultTableModel();
        String[] columnNames = {"id", "fromNode", "toNode", "weight", "timeLag"};
        model.setColumnIdentifiers(columnNames);
        List<Link> links = problemInstance.getLinks();


        for (Link link : links) {
            List<String> list = new ArrayList<>();
            list.add(link.getId() + "");
            list.add(problemInstance.getNodes().get(link.getFrom()) + "");
            list.add(problemInstance.getNodes().get(link.getTo()) + "");
            list.add(link.getWeight() + "");
            list.add(link.getTimeLag() + "");
            model.addRow(list.toArray());
        }

        jTable = new JTable(model);

        jTable.getTableHeader().setFont(smallBold);
        jTable.setFont(GUIConstants.small);
        jTable.setRowHeight(GUIConstants.small.getSize() + 5);

        jTable.setFillsViewportHeight(true);
        getViewport().add(jTable);
    }
}
