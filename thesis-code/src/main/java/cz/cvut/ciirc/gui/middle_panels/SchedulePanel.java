package cz.cvut.ciirc.gui.middle_panels;

import cz.cvut.ciirc.gui.GUIConstants;
import cz.cvut.ciirc.gui.MiddlePanel;
import cz.cvut.ciirc.gui.middle_panels.middle_components.IntervalChart;

import javax.swing.*;
import java.awt.*;

import static cz.cvut.ciirc.gui.GUIConstants.largeBold;
import static cz.cvut.ciirc.gui.GUIConstants.warning;

/**
 * Represents one of the second phase screens containing the Gantt chart of created run.
 */
public class SchedulePanel extends JPanel {
    private final MiddlePanel parent;
    private JPanel ganttPanel = null;
    private JLabel notFoundLabel;


    public SchedulePanel(MiddlePanel parent) {
        super(new BorderLayout());
        setBorder(GUIConstants.border);
        this.parent = parent;
    }

    public void updatePanel() {
        if (!(ganttPanel == null)) remove(ganttPanel);
        if (!(notFoundLabel == null)) remove(notFoundLabel);
        notFoundLabel = null;
        ganttPanel = null;
        if (parent.getSolution().isScheduleFound()) {
            IntervalChart chart = new IntervalChart(parent.getSolution());
            ganttPanel = chart.getPanel();
            add(ganttPanel);
        } else {
            notFoundLabel = new JLabel("Schedule could not be found!");
            notFoundLabel.setAlignmentX(CENTER_ALIGNMENT);
            notFoundLabel.setHorizontalAlignment(JLabel.CENTER);
            notFoundLabel.setFont(largeBold);
            notFoundLabel.setForeground(warning);
            add(notFoundLabel, BorderLayout.CENTER);
        }
    }
}
