package cz.cvut.ciirc.scheduling;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.exceptions.ResultsIOException;
import cz.cvut.ciirc.scheduling.baseline_methods.CheckGenerator;
import lombok.Getter;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class used for comparing different solvers in large tests.
 * Saves information about the solver quality continuously to file.
 */
public class Statistics {
    /**
     * Number of characters to which the output columns are padded.
     */
    private static Integer PADDING_LIMIT = 18;
    /**
     * Instance ID
     */
    @Getter
    private int id;
    /**
     * Average utilization of network links
     */
    @Getter
    private double avgUtilization;
    /**
     * Maximal utilization of network links
     */
    @Getter
    private double maxUtilization;
    /**
     * True if solution was found by any of the methods, false otherwise
     */
    @Getter
    private boolean solutionFound = false;
    /**
     * List of solutionFound for each method
     */
    @Getter
    private ArrayList<Boolean> success = new ArrayList<>();
    /**
     * List of objective values for each method
     */
    private ArrayList<Integer> objValues = new ArrayList<>();
    /**
     * List of all methods names
     */
    private ArrayList<String> methodNames = new ArrayList<>();
    /**
     * List of elapsed time of each solver method
     */
    private ArrayList<Double> elapsedTimes = new ArrayList<>();
    /**
     * Path to which the stats are saved.
     */
    private Path statsPath;

    /**
     * Default constructor
     *
     * @param statsPath      Path to which the data is saved
     * @param id             ID of the instance
     * @param avgUtilization Average utilization of all network links
     * @param maxUtilization Maximal utilization of all network links
     * @param numSolvers     Number of used solvers
     */
    public Statistics(Path statsPath, int id, double avgUtilization, double maxUtilization, int numSolvers) {
        this.id = id;
        this.avgUtilization = avgUtilization;
        this.maxUtilization = maxUtilization;
        this.statsPath = statsPath;
        for (int i = 0; i < numSolvers; i++) {
            success.add(false);
            objValues.add(-2);
            methodNames.add("");
            elapsedTimes.add(-1.0);
        }
    }

    /**
     * Saves header of the stats file.
     *
     * @param statsPath Path to which the text is saved
     * @param classes   List containing all algorithms for solving the instance
     * @param folderID  Index of the directory containing the instances
     * @throws ResultsIOException in case the results could not be saved to file
     */
    public static void saveHeader(Path statsPath, List<AlgorithmType> classes, int folderID) throws ResultsIOException {
        StringBuilder out = new StringBuilder(String.format("%n"));
        int offset;
        out.append(String.format("%n--------------------%n"));
        out.append(String.format("%nInstance folder ID: %d%n", folderID));
        out.append("Day and time of experiment: ").append(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(Calendar.getInstance().getTime()));
        out.append(String.format("%n--------------------%n"));
        offset = out.length();
        out.append("ID");
        padStringBuilder(out, offset + PADDING_LIMIT / 3 - 1);
        offset = out.length();
        out.append("|  avgU  maxU");
        padStringBuilder(out, offset + PADDING_LIMIT);
        for (AlgorithmType thisClass : classes) {
            offset = out.length();
            out.append("| ").append(thisClass.getMethodClass().getSimpleName());
            padStringBuilder(out, offset + PADDING_LIMIT);
        }
        appendStatsFile(statsPath, out.toString());
    }

    /**
     * Saves common instance info (utilization).
     *
     * @param filePath       Path to which the text is saved
     * @param id             Id of the instance
     * @param avgUtilization Average utilization over all links
     * @param maxUtilization Maximal utilization of all links
     * @throws ResultsIOException In case the info could not be saved
     */
    public static void saveCommonInstanceInfo(Path filePath, int id, double avgUtilization,
                                              double maxUtilization) throws ResultsIOException {
        StringBuilder out = new StringBuilder(String.format("%n%d", id));
        padStringBuilder(out, PADDING_LIMIT / 3);
        int offset = out.length();
        out.append(String.format("|  %.2f  %.2f", avgUtilization, maxUtilization));
        padStringBuilder(out, offset + PADDING_LIMIT);
        appendStatsFile(filePath, out.toString());
    }

    /**
     * Adds additional spaces to string to pad it.
     *
     * @param out   Text to be padded
     * @param limit Limit to which the text is padded
     */
    private static void padStringBuilder(StringBuilder out, int limit) {
        int lim = limit - out.length();
        for (int i = 0; i < lim; i++) {
            out.append(" ");
        }
    }

    /**
     * Appends the stats file with given text.
     *
     * @param filePath Path to which the text is saved
     * @param out      Text that is saved to file
     * @throws ResultsIOException In case the stats could not be saved
     */
    private static void appendStatsFile(Path filePath, String out) throws ResultsIOException {
        try {
            FileWriter fw = new FileWriter(filePath.toFile(), true);
            fw.write(out);
            fw.close();
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
            throw new ResultsIOException("Statistics file was not updated");
        }

    }

    /**
     * After all instances are solved, saves summed results.
     *
     * @param stats    List of stats
     * @param classes  List of methods
     * @param filePath Path to which the stats are saved
     * @throws ResultsIOException In case the results could not be saved
     */
    public static void saveTotalStats(ArrayList<Statistics> stats, List<AlgorithmType> classes, Path filePath) throws ResultsIOException {
        StringBuilder out = new StringBuilder();

        int numInstances = stats.size();
        List<Boolean> totalSuccess = stats.stream().map(Statistics::isSolutionFound).filter(val -> val).collect(Collectors.toList());
        out.append(String.format("%n%nSolution found for: %d out of %d%n", totalSuccess.size(), numInstances));

        if (stats.size() != 0 && stats.get(0).getSuccess().size() == classes.size()) {
            for (int i = 0; i < classes.size(); i++) {
                final int id = i;
                List<Boolean> successForMethod = stats.stream().map(s -> s.getSuccess().get(id)).filter(b -> b).collect(Collectors.toList());
                out.append(classes.get(id).getMethodClass().getSimpleName()).append(String.format(": %d%n", successForMethod.size()));
            }
        }
        List<Integer> clear = stats.stream().filter(p -> !p.getSuccess().contains(false)).map(Statistics::getId).collect(Collectors.toList());
        out.append(String.format("Num instances scheduled by all: %d%n%n", clear.size()));
        appendStatsFile(filePath, out.toString());

    }

    /**
     * Saves results of one method on one instance.
     *
     * @param methodName  Name of the method
     * @param found       True if solution was found
     * @param objValue    Objective value of the found solution
     * @param elapsedTime Time it took the solver to solve the instance (or default time limit)
     * @param classID     ID of the solver class
     * @throws ResultsIOException In case the stats could not be saved
     */
    public void addResultsAndSave(String methodName, boolean found, int objValue,
                                  double elapsedTime, int classID) throws ResultsIOException {
        String[] parts = methodName.split("\\.");

        methodNames.set(classID, parts[parts.length - 1]);
        success.set(classID, found);
        objValues.set(classID, objValue);
        elapsedTimes.set(classID, elapsedTime);
        if (!methodName.equals(CheckGenerator.class.getName())) {
            solutionFound = solutionFound || found;
        }

        StringBuilder out = new StringBuilder(String.format("| %d  %d  %.2f", found ? 1 : 0, objValue, elapsedTime));
        padStringBuilder(out, PADDING_LIMIT);
        appendStatsFile(statsPath, out.toString());
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder(String.format(
                "%d\t|\t%.2f\t|\t%.2f\t|\t%b\t||\t ",
                id, avgUtilization, maxUtilization, solutionFound));
        for (int i = 0; i < methodNames.size(); i++) {
            s.append((success.get(i) ? 1 : 0)).append("\t|\t").append(objValues.get(i));
            if (i != methodNames.size() - 1) s.append("\t||\t");
        }
        return s.toString();
    }
}
