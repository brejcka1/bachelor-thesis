package cz.cvut.ciirc.scheduling.helper_classes;

import lombok.Getter;
import lombok.Setter;

/**
 * General class for sorting elements in Priority queue (needed in solverMethods). Up to 4 criteria can be used.
 * First will be Objects with low criteria value.
 */
@Getter
@Setter
public class ObjectPriorities<F> implements Comparable<ObjectPriorities<F>> {
    private final F object;
    private int criteria1;
    private int criteria2;
    private int criteria3;
    private int criteria4;


    public ObjectPriorities(F object, int criteria1, int criteria2, int criteria3, int criteria4) {
        this.object = object;
        this.criteria1 = criteria1;
        this.criteria2 = criteria2;
        this.criteria3 = criteria3;
        this.criteria4 = criteria4;
    }

    public ObjectPriorities(F object, int criteria1, int criteria2, int criteria3) {
        this(object, criteria1, criteria2, criteria3, 0);
    }

    public ObjectPriorities(F object, int criteria1, int criteria2) {
        this(object, criteria1, criteria2, 0);
    }

    public ObjectPriorities(F object, int criteria1) {
        this(object, criteria1, 0);
    }

    @Override
    public int compareTo(ObjectPriorities<F> other) {
        if (this.criteria1 != other.criteria1) {
            return Integer.compare(this.criteria1, other.criteria1); // first low periods
        } else if (this.criteria2 != other.criteria2) {
            return Integer.compare(this.criteria2, other.criteria2); // first low periods
        } else if (this.criteria3 != other.criteria3) {
            return Integer.compare(this.criteria3, other.criteria3); // first low periods
        } else {
            return Integer.compare(this.criteria4, other.criteria4); // first low periods
        }
    }

    @Override
    public String toString() {
        return "ObjectPriorities{" +
                "object=" + object +
                ", criteria1=" + criteria1 +
                ", criteria2=" + criteria2 +
                ", criteria3=" + criteria3 +
                ", criteria4=" + criteria4 +
                '}';
    }
}

