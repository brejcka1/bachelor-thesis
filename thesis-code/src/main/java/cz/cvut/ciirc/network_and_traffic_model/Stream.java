package cz.cvut.ciirc.network_and_traffic_model;

import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple class representing the stream in network model.
 * For more information, see the Problem Statement.
 */

@Getter
public class Stream {
    private final Node origin;
    private final Node target;
    private final int duration;
    private final int period;
    private final int release;
    private final int deadline;

    private final String name;
    private final int id;

    private List<Node> path;
    private List<StreamInstance> streamInstanceList;
    private ProblemInstance problemInstance;

    public Stream(ProblemInstance problemInstance, int id, String name, Node origin, Node target, int duration, int period, int release, int deadline) {
        this.problemInstance = problemInstance;
        this.origin = origin;
        this.target = target;
        this.duration = duration;
        this.period = period;
        this.name = name.replaceAll("stream", "s");
        this.id = id;
        this.release = release;
        this.deadline = deadline;
        this.path = new ArrayList<>();
        this.streamInstanceList = new ArrayList<>();
    }

    /**
     * Returns reoccurred stream instance of the stream in the given period on the last link.
     *
     * @param hpID period index
     * @return reoccurred stream instance of the stream in the given period on the last link
     * @throws ArrayNotInitializedException In case of faulty initialized problem instance
     */
    public ReoccurredStreamInstance getLastReSIInPeriod(int hpID) throws ArrayNotInitializedException {
        if (streamInstanceList.size() == 0)
            throw new ArrayNotInitializedException("Stream Instance list not initialized");
        return streamInstanceList.get(streamInstanceList.size() - 1).getReSIInHP(hpID);
    }

    /**
     * Returns reoccurred stream instance of the stream in the given period on the first link.
     *
     * @param hpID period index
     * @return reoccurred stream instance of the stream in the given period on the first link
     * @throws ArrayNotInitializedException In case of faulty initialized problem instance
     */
    public ReoccurredStreamInstance getFirstReSIInPeriod(int hpID) throws ArrayNotInitializedException {
        if (streamInstanceList.size() == 0)
            throw new ArrayNotInitializedException("Stream Instance list not initialized");
        return streamInstanceList.get(0).getReSIInHP(hpID);
    }

    /**
     * Returns minimal duration of the stream (from origin to target) transfer in one period.
     *
     * @return total duration of stream in one period
     * @throws ArrayNotInitializedException in case the problem instance was faulty initialized
     */
    public int getMinimalRequiredTime() throws ArrayNotInitializedException {
        int val = 0;
        for (StreamInstance fli : streamInstanceList) {
            val += getFirstReSIInPeriod(0).getDurationWithTimeLag();
            val += problemInstance.getNodes().get(fli.getLink().getTo()).getTimeLag();
        }
        val -= problemInstance.getNodes().get(streamInstanceList.get(streamInstanceList.size() - 1)
                .getLink().getTo()).getTimeLag();
        return val;
    }

    /**
     * Returns duration of the stream (from origin to target) transfer in one period.
     *
     * @return total duration of stream in one period
     */
    public int getTotalRequiredTime() throws ArrayNotInitializedException {
        return (getLastReSIInPeriod(0).getEnd() - getFirstReSIInPeriod(0).getStart());
    }

    @Override
    public String toString() {
        return "Stream{" +
                "origin=" + origin +
                ", target=" + target +
                ", duration=" + duration +
                ", period=" + period +
                ", release=" + release +
                ", deadline=" + deadline +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    /**
     * Returns number of stream repetitions in the hyper period.
     *
     * @return number of stream repetitions in the hyper period.
     * @throws ArrayNotInitializedException in case the problem instance was not initialized correctly
     */
    public int getNumHPInstances() throws ArrayNotInitializedException {
        if (streamInstanceList.size() > 0) {
            return streamInstanceList.get(0).getNumInHP();
        } else {
            throw new ArrayNotInitializedException("Stream Instance list not initialized");
        }
    }
}