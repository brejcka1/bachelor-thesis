package cz.cvut.ciirc.gui.middle_panels;

import cz.cvut.ciirc.gui.MiddlePanel;
import cz.cvut.ciirc.gui.middle_panels.middle_components.LinksInfoPanel;
import cz.cvut.ciirc.gui.middle_panels.middle_components.NodesInfoPanel;
import cz.cvut.ciirc.gui.middle_panels.middle_components.StreamsInfoPanel;

import javax.swing.*;
import java.awt.*;

import static cz.cvut.ciirc.gui.GUIConstants.mediumBold;

/**
 * One of the second phase screens showing the instance details.
 */
public class InfoPanel extends JPanel {
    private final MiddlePanel parent;
    private final Dimension space = new Dimension(0, 15);
    private JLabel nodesLabel;
    private NodesInfoPanel nodesInfoPanel;
    private JLabel linksInfoLabel;
    private LinksInfoPanel linksInfoPanel;
    private JLabel streamsInfoLabel;
    private StreamsInfoPanel streamsInfoPanel;

    public InfoPanel(MiddlePanel parent) {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.parent = parent;
        initLabels();
    }

    private void initLabels() {
        nodesLabel = new JLabel("Nodes");
        streamsInfoLabel = new JLabel("Streams");
        linksInfoLabel = new JLabel("Links");
        nodesLabel.setAlignmentX(CENTER_ALIGNMENT);
        nodesLabel.setFont(mediumBold);
        streamsInfoLabel.setAlignmentX(CENTER_ALIGNMENT);
        streamsInfoLabel.setFont(mediumBold);
        linksInfoLabel.setAlignmentX(CENTER_ALIGNMENT);
        linksInfoLabel.setFont(mediumBold);
    }


    public void updatePanel() {
        removeAll();
        add(Box.createRigidArea(space));

        add(streamsInfoLabel, CENTER_ALIGNMENT);
        streamsInfoPanel = new StreamsInfoPanel(parent.getSolution().getProblemInstance().getStreams());
        add(streamsInfoPanel);

        add(Box.createRigidArea(space));

        add(linksInfoLabel, CENTER_ALIGNMENT);
        linksInfoPanel = new LinksInfoPanel(parent.getSolution().getProblemInstance());
        add(linksInfoPanel);

        add(Box.createRigidArea(space));

        add(nodesLabel, CENTER_ALIGNMENT);
        nodesInfoPanel = new NodesInfoPanel(parent.getSolution().getProblemInstance());
        add(nodesInfoPanel);
    }
}
