package cz.cvut.ciirc.exceptions;

public class GeneratorException extends CustomException {

    public GeneratorException(String s) {
        super(s);
    }
}
