package cz.cvut.ciirc.exceptions;

public class ArrayNotInitializedException extends CustomException {

    public ArrayNotInitializedException(String s) {
        super(s);
    }

    public ArrayNotInitializedException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
