package cz.cvut.ciirc.exceptions;

public class ImplementationException extends CustomException {

    public ImplementationException(String s) {
        super(s);
    }

    public ImplementationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
