package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;
import cz.cvut.ciirc.scheduling.helper_classes.StreamInstanceInfo;

import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Earliest Deadline Latest Start Time First Fit Heuristic</h3>
 * <p>
 * One pass heuristic method. Stream instances are sorted by deadline and then increasing latest start time.
 * <p>
 * Each stream instance is placed to first available slot.
 */
public class DF_LST_FF extends Heuristics {
    public DF_LST_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.DF_LST_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object obj) throws ImplementationException {
        if (!(obj instanceof StreamInstanceInfo)) {
            throw new ImplementationException("ERROR in " + getClass().getName() + " solverMethods updatePQ - using incorrect Object type");
        }
        StreamInstanceInfo sii = (StreamInstanceInfo) obj;
        Iterator<ObjectPriorities> iterator = pQ.iterator();
        ObjectPriorities current = null;
        boolean control = false;

        // searches for element to be removed
        while (iterator.hasNext()) {
            current = iterator.next();
            if (current.getObject().equals(sii)) {
                control = true;
                break;
            }
        }

        if (!control) {
            throw new ImplementationException("ERROR in " + getClass().getName() + " solverMethods updatePQ - cannot find item");
        } else {
            // updates the value for given stream instance
            getFliIndexAndRemoveFromPQ(pQ, sii, current);
            current.setCriteria1(sii.getCurSI().getStream().getDeadline() / 100);
            current.setCriteria2(sii.getLatestStart());
            pQ.add(current);
        }
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        List<StreamInstanceInfo> siiList = initializeStreamInfoList();

        for (StreamInstanceInfo curSii : siiList) {
            pQ.add(new ObjectPriorities<>(curSii, curSii.getCurSI().getStream().getDeadline() / 100, curSii.getLatestStart()));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamInstancesFirstFit(pQ);
    }
}