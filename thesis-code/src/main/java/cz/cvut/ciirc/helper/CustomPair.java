package cz.cvut.ciirc.helper;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * Simple class representing pair of two typed values and optionally two priority values.
 */

public class CustomPair<F, S> implements Comparable<CustomPair<F, S>> {
    @Getter
    private final F first;

    @Getter
    private final S second;

    @Setter
    @Getter
    private int firstPriorityValue = -1;

    @Setter
    @Getter
    private int secondPriorityValue = -1;

    public CustomPair(F first, S second, int firstPriorityValue, int secondPriorityValue) {
        this.first = first;
        this.second = second;
        this.firstPriorityValue = firstPriorityValue;
        this.secondPriorityValue = secondPriorityValue;
    }

    @Override
    public String toString() {
        return first + " -> " + second + ": " +
                "firstPriorityValue=" + firstPriorityValue +
                ", secondPriorityValue=" + secondPriorityValue;
    }

    @Override
    public int compareTo(CustomPair<F, S> other) {
        if (this.firstPriorityValue != other.firstPriorityValue) {
            return Integer.compare(this.firstPriorityValue, other.firstPriorityValue);
        } else {
            return Integer.compare(this.secondPriorityValue, other.secondPriorityValue);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomPair<?, ?> schedulerPair = (CustomPair<?, ?>) o;
        return Objects.equals(first, schedulerPair.first) &&
                Objects.equals(second, schedulerPair.second);
    }

    @Override
    public int hashCode() {
        return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode());
    }
}