package cz.cvut.ciirc.generator;

import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.data_format.InputFile;
import cz.cvut.ciirc.data_format.LinkList;
import cz.cvut.ciirc.data_format.NodeList;
import cz.cvut.ciirc.data_format.StreamList;
import cz.cvut.ciirc.exceptions.*;
import cz.cvut.ciirc.helper.CustomPair;
import cz.cvut.ciirc.helper.ProtoHandler;
import cz.cvut.ciirc.helper.StaticMath;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import cz.cvut.ciirc.network_and_traffic_model.Node;
import cz.cvut.ciirc.scheduling.helper_classes.LinkTimeSlots;

import java.nio.file.Path;
import java.util.*;

/**
 * Main class for generating experiment instances,
 */
public class InstanceGenerator {
    /**
     * Random with a seed to generate reproducible instances.
     */
    private final Random randomGenerator;

    /**
     * Instance generator constructor.
     *
     * @param randomGenerator Random with a seed to generate reproducible instances
     */
    InstanceGenerator(Random randomGenerator) {
        this.randomGenerator = randomGenerator;
    }

    /**
     * Main method to run - can switch between main_local() or main_server()
     *
     * @param args No arguments expected
     */
    public static void main(String[] args) {
        main_local();
    }

    /**
     * Generates instances in one folder. Good for debugging.
     */
    private static void main_local() {
        long start = System.nanoTime();
        int numInstances = 1;
        int numReStreamInstances = 2400;
        int dirID = 1;

        try {
            InstanceGenerator generator = new InstanceGenerator(new Random(1357));
            for (int i = 0; i < numInstances; i++) {
                System.out.println("Generating instance " + i + "/" + numInstances);
                int fileID = ProtoHandler.findFirstFreeFileID(dirID);
                generator.generateInstance(fileID, ProtoHandler.getInstanceFilePath(dirID, fileID),
                        ParametersBG.LINE_TOPOLOGY, ParametersBG.LARGE_INSTANCE, numReStreamInstances, 1);
            }
            printAlgDuration(start, numInstances);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Generates instances in several folders. Good for generating instances of different parameters.
     */
    private static void main_server() {
        long start = System.nanoTime();
        int numInstances = 1;
        int dirID = 667;
        int reStreamInstanceCountDiff = 3;

        List<Integer> params;
        int addTo, base, maxNumReStreamInstances;
        Random random = new Random();
        random.setSeed(1564589);

        int totalNumDirs = 3 * 3 * reStreamInstanceCountDiff * 3;
        try {
            for (int topSizeID = 0; topSizeID < 3; topSizeID++) {
                for (int topologyMode = 0; topologyMode < 3; topologyMode++) {
                    params = ParametersBG.getBaseAndAddTo(topologyMode, topSizeID, reStreamInstanceCountDiff);
                    addTo = params.get(1);
                    base = params.get(0);
                    maxNumReStreamInstances = base + addTo * reStreamInstanceCountDiff;
                    for (int periodMode = 0; periodMode < 3; periodMode++) {
                        for (int numReStreamInstances = base; numReStreamInstances < maxNumReStreamInstances; numReStreamInstances += addTo) {
                            InstanceGenerator generator = new InstanceGenerator(random);
                            for (int instanceID = 0; instanceID < numInstances; instanceID++) {
                                printCurrentInstanceParams(numInstances, dirID, totalNumDirs, topSizeID, topologyMode, periodMode, numReStreamInstances, instanceID);
                                int fileID = ProtoHandler.findFirstFreeFileID(dirID);
                                generator.generateInstance(fileID, ProtoHandler.getInstanceFilePath(dirID, fileID),
                                        topologyMode, ParametersBG.topologySizes[topSizeID], numReStreamInstances, periodMode);
                            }
                            //dirID++;
                        }
                    }
                }
            }

            printAlgDuration(start, numInstances);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Prints currently generated instance info.
     *
     * @param numInstances         Total number of instances to generate
     * @param dirID                Directory to which the instances are generated
     * @param totalNumDirs         Total number of directories to generate
     * @param topSizeID            Size of the instances
     * @param topologyMode         Type of the topology
     * @param periodMode           Period set to choose
     * @param numReStreamInstances Total number of reoccurred stream instance to generate
     * @param fileID               Currently generated file
     */
    static void printCurrentInstanceParams(int numInstances, int dirID, int totalNumDirs, int topSizeID, int topologyMode, int periodMode, int numReStreamInstances, int fileID) {
        System.out.println("Dir: " + (dirID) + "/" + totalNumDirs + " Generating instance " + fileID + "/" + numInstances + " numReStreamInstances: "
                + numReStreamInstances + " periodsMode: " + periodMode + " topology mode: " + topologyMode + " topology size: " + topSizeID);
    }

    /**
     * Prints the total time it took to generate instance.
     *
     * @param start        Generation start time
     * @param numInstances Total number of instances
     */
    protected static void printAlgDuration(long start, int numInstances) {
        long finishTime = System.nanoTime();
        long duration = (long) Math.floor((finishTime - start) / (Math.pow(10, 9)));
        System.out.println("Duration of generating " + numInstances + " instances: " + duration + " s");
    }

    /**
     * Tries to generate input instance of specified parameters and saves it if successful.
     *
     * @param instanceID           ID of the generated instance
     * @param path                 Path to which the instance is saved
     * @param topologyMode         Type of the network topology
     * @param sizeMode             Size of the network topology
     * @param numReStreamInstances Number of reoccurred stream instances
     * @param periodsMode          Type of period set used for generating streams
     * @throws UnspecifiedException in case the proto could not be saved
     */
    InputFile.InputFileProto generateInstance(int instanceID, Path path, int topologyMode, int sizeMode, int numReStreamInstances, int periodsMode) throws GeneratorException, UnspecifiedException {
        int[] topologyParams = ParametersBG.getParams(sizeMode);
        boolean control = false;
        InputFile.InputFileProto.Builder inputFile = InputFile.InputFileProto.newBuilder();
        inputFile.setId(instanceID);
        List<Integer> leafs;
        try {
            switch (topologyMode) {
                case ParametersBG.TREE_TOPOLOGY:
                    leafs = generateTreeTopology(inputFile, topologyParams[1], topologyParams[0]);
                    for (int attemptID = 0; attemptID < 100; attemptID++) {
                        control = generateStreamsByNumReoccurredStreamInstances(inputFile, leafs, periodsMode, numReStreamInstances);
                        if (control) break;
                    }
                    break;
                case ParametersBG.RING_TOPOLOGY:
                    leafs = generateRingTopology(inputFile, topologyParams[2], topologyParams[3]);
                    for (int attemptID = 0; attemptID < 100; attemptID++) {
                        control = generateStreamsByNumReoccurredStreamInstances(inputFile, leafs, periodsMode, numReStreamInstances);
                        if (control) break;
                    }
                    break;
                case ParametersBG.LINE_TOPOLOGY:
                    leafs = generateLineTopology(inputFile, topologyParams[4], topologyParams[5]);
                    for (int attemptID = 0; attemptID < 100; attemptID++) {
                        control = generateStreamsByNumReoccurredStreamInstances(inputFile, leafs, periodsMode, numReStreamInstances);
                        if (control) break;
                    }
                    break;
                default:
                    System.err.println("Incorrect topology mode");
                    System.exit(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            control = false;
        }

        //if (control) ProtoHandler.writeProto(path, inputFile.build());
        if (!control)
            throw new GeneratorException("Could not create instance of given parameters {topologyMode, topologySize, numReStreamInstances, periodsMode}"
                    + " = {" + topologyMode + " " + sizeMode + " " + numReStreamInstances + " " + periodsMode + "}");

        return inputFile.build();
    }

    /**
     * Generates schedulable streams based on required number of reoccurred stream instances. First initializes period set,
     * then initializes priority queue containing all possible pairs of leafs that are intended to be origins or targets of
     * the stream. Then randomly chooses the stream duration and tries to place it to the run, if not successful,
     * decreases the duration and iterates in this way until is able to place the stream into the run or reaches
     * lower bound on stream duration. If the stream was placed successfully, iterates until the required total number
     * of reoccurred stream instances is created. If the stream was not placed, tries another combination of origin, target
     * and period. In case there are no such available combinations terminates and returns false.
     *
     * @param inputProto           Proto representing the created instance
     * @param leafs                Leafs in the network
     * @param periodsMode          ID of the period set to be chosen
     * @param numReStreamInstances Total number of reoccurred stream instances to be generated
     * @return true if the streams were successfully generated, false otherwise
     * @throws Exception In case of Problem Instance initialization error
     */
    private boolean generateStreamsByNumReoccurredStreamInstances(InputFile.InputFileProto.Builder inputProto, List<Integer> leafs, int periodsMode, int numReStreamInstances) throws Exception {
        int reStreamInstanceID = 0;
        int curP, numInHP;
        int streamID = 0;
        CustomPair<Integer, Integer> curLeafPair;
        boolean success;
        List<Link> linkRoute;
        List<Integer> periods = ParametersBG.getPeriods(periodsMode);
        int HP = StaticMath.listLCM(periods);
        periods = ParametersBG.getPeriods(periodsMode);

        StreamList.StreamListProto.Builder streamListBuilder = StreamList.StreamListProto.newBuilder();
        ProblemInstance baseInstance = new ProblemInstance(inputProto.build());
        ArrayList<LinkTimeSlots> linkSchedule = linkScheduleInit(baseInstance.getLinks(), HP);
        PriorityQueue<CustomPair<Integer, Integer>> fromToCombinations = getPQ(leafs, periods.get(0), baseInstance);


        while (reStreamInstanceID < numReStreamInstances) {
            curP = selectPeriod(reStreamInstanceID, numReStreamInstances, periods, false);
            if (fromToCombinations.isEmpty() || curP < fromToCombinations.peek().getFirstPriorityValue()) {
                System.err.println("Progress: " + reStreamInstanceID + "/" + numReStreamInstances);
                return false;
            }

            curLeafPair = fromToCombinations.poll();
            if (curLeafPair == null) {
                System.err.println("Progress: " + reStreamInstanceID + "/" + numReStreamInstances);
                return false;
            }

            numInHP = HP / curP;
            linkRoute = findLinkRoute(curLeafPair, baseInstance);
            success = tryToPlace(linkRoute, linkSchedule, curP, numInHP, streamListBuilder, streamID, baseInstance.getNodes());

            if (success) {
                streamID++;
                reStreamInstanceID += numInHP * linkRoute.size();
                curLeafPair.setFirstPriorityValue(curP);
                curLeafPair.setSecondPriorityValue(reStreamInstanceID);
                fromToCombinations.add(curLeafPair);
            } else if (curP != periods.get(periods.size() - 1)) {
                curLeafPair.setFirstPriorityValue(
                        selectPeriod(reStreamInstanceID, numReStreamInstances, periods, true));
                curLeafPair.setSecondPriorityValue(reStreamInstanceID);
                fromToCombinations.add(curLeafPair);
            }
        }

        inputProto.setStreams(streamListBuilder.build());
        return true;
    }


    /**
     * Generates ring network (nodes and links) and adds it to the InputProto initializer.
     *
     * @param inputBuilder   Proto describing the created instance.
     * @param numBranches    Number of branches from each centre node.
     * @param numCentreNodes Number of centre nodes (in the central ring).
     */
    private List<Integer> generateRingTopology(InputFile.InputFileProto.Builder inputBuilder, int numCentreNodes, int numBranches) {
        int linkID = 0;
        int target = numCentreNodes;
        List<Integer> leafs = new ArrayList<>();

        NodeList.NodeListProto.Builder nodeListBuilder = NodeList.NodeListProto.newBuilder();
        LinkList.LinkListProto.Builder linkListBuilder = LinkList.LinkListProto.newBuilder();

        for (int centreID = 0; centreID < numCentreNodes; centreID++) {
            ProtoHandler.addLinkToProto(linkListBuilder, linkID, centreID, (centreID + 1) % numCentreNodes, ParametersBG.BETTER_LINK_WEIGHT, ParametersBG.LINK_TIME_LAG);
            ProtoHandler.addNodeToProto(nodeListBuilder, centreID, ParametersBG.NODE_TIME_LAG);
            linkID++;

            for (int branchID = 0; branchID < numBranches; branchID++) {
                ProtoHandler.addLinkToProto(linkListBuilder, linkID, centreID, target, ParametersBG.WORSE_LINK_WEIGHT, ParametersBG.LINK_TIME_LAG);
                linkID++;
                ProtoHandler.addNodeToProto(nodeListBuilder, target, ParametersBG.NODE_TIME_LAG);
                leafs.add(target);
                target++;
            }
        }

        inputBuilder.setLinks(linkListBuilder.build());
        inputBuilder.setNodes(nodeListBuilder.build());
        return leafs;
    }


    /**
     * Generates ring network (nodes and links) and adds it to the InputProto initializer.
     *
     * @param inputBuilder Proto describing the created instance.
     * @param onLine       Number of branches on the centre line, including the controlling node
     * @param outBranching Number of branches connected to each centre node on the line
     */
    private List<Integer> generateLineTopology(InputFile.InputFileProto.Builder inputBuilder, int onLine, int outBranching) {
        int linkID = 0;
        int target = onLine;
        int previous;
        List<Integer> leafs = new ArrayList<>();

        NodeList.NodeListProto.Builder nodeListBuilder = NodeList.NodeListProto.newBuilder();
        LinkList.LinkListProto.Builder linkListBuilder = LinkList.LinkListProto.newBuilder();

        ProtoHandler.addNodeToProto(nodeListBuilder, -1, ParametersBG.NODE_TIME_LAG);
        leafs.add(-1);

        for (int onLineID = 1; onLineID < onLine; onLineID++) {
            if (onLineID == 1) {
                previous = -1;
            } else {
                previous = onLineID - 1;
            }
            ProtoHandler.addLinkToProto(linkListBuilder, linkID, onLineID, previous, ParametersBG.BETTER_LINK_WEIGHT, ParametersBG.LINK_TIME_LAG);
            linkID++;
            ProtoHandler.addNodeToProto(nodeListBuilder, onLineID, ParametersBG.NODE_TIME_LAG);
            for (int branchID = 0; branchID < outBranching; branchID++) {
                ProtoHandler.addLinkToProto(linkListBuilder, linkID, onLineID, target, ParametersBG.WORSE_LINK_WEIGHT, ParametersBG.LINK_TIME_LAG);
                linkID++;
                leafs.add(target);
                ProtoHandler.addNodeToProto(nodeListBuilder, target, ParametersBG.NODE_TIME_LAG);
                target++;
            }
        }

        inputBuilder.setLinks(linkListBuilder.build());
        inputBuilder.setNodes(nodeListBuilder.build());
        return leafs;
    }


    /**
     * Generates tree network (nodes and links) and adds it to the InputProto initializer.
     *
     * @param inputBuilder Proto describing the created instance.
     * @param numBranches  Number of branches in the tree.
     * @param depth        Depth of the tree.
     */
    private List<Integer> generateTreeTopology(InputFile.InputFileProto.Builder inputBuilder, int numBranches, int depth) {
        int linkID = 0;
        int linkWeight = ParametersBG.BETTER_LINK_WEIGHT;
        int numLayerNodes;
        int curParent, curChild;
        List<Integer> leafs = new ArrayList<>();

        NodeList.NodeListProto.Builder nodeListBuilder = NodeList.NodeListProto.newBuilder();
        LinkList.LinkListProto.Builder linkListBuilder = LinkList.LinkListProto.newBuilder();

        curParent = 0;
        curChild = 1;
        ProtoHandler.addNodeToProto(nodeListBuilder, curParent, ParametersBG.NODE_TIME_LAG);

        for (int depthID = 0; depthID < depth - 1; depthID++) {
            if (depthID == depth - 2) linkWeight = ParametersBG.WORSE_LINK_WEIGHT;
            numLayerNodes = (int) Math.pow(numBranches, depthID);
            for (int parentID = 0; parentID < numLayerNodes; parentID++) {
                for (int branchID = 0; branchID < numBranches; branchID++) {
                    ProtoHandler.addLinkToProto(linkListBuilder, linkID, curParent, curChild, linkWeight, ParametersBG.LINK_TIME_LAG);
                    ProtoHandler.addNodeToProto(nodeListBuilder, curChild, ParametersBG.NODE_TIME_LAG);
                    if (depthID == depth - 2) leafs.add(curChild);
                    linkID++;
                    curChild++;
                }
                curParent++;
            }
        }

        inputBuilder.setLinks(linkListBuilder.build());
        inputBuilder.setNodes(nodeListBuilder.build());
        return leafs;
    }


    /**
     * Selects period from the period set based on algorithm progress (periods are evenly distributed between
     * reoccurred stream instances).
     *
     * @param reStreamInstanceID       Index of the current reoccurred stream instance
     * @param totalNumReStreamInstance Total number of reoccurred stream instances
     * @param periods                  The set of periods
     * @param selectNext               True in case the period right after the current one schould be selected
     * @return selected period
     */
    private int selectPeriod(int reStreamInstanceID, int totalNumReStreamInstance, List<Integer> periods, boolean selectNext) {
        int numChoices = periods.size();
        int bucketSize = totalNumReStreamInstance / numChoices;
        int bucketIndex = Math.min(reStreamInstanceID / bucketSize, numChoices - 1);
        if (selectNext) {
            bucketIndex++;
            if (bucketIndex > numChoices - 1) {
                return Integer.MAX_VALUE;
            }
        }
        return periods.get(bucketIndex);
    }


    /**
     * Finds shortest path from origin to target node.
     *
     * @param fromTo   Pair containing the origin and target node.
     * @param instance Problem instance containing the network graph.
     * @return Found path represented by sequence of links
     * @throws TopologyInitializationException in case of routing problems in the network graph
     */
    private List<Link> findLinkRoute(CustomPair fromTo, ProblemInstance instance) throws TopologyInitializationException {
        int fromNode = (int) fromTo.getFirst();
        int toNode = (int) fromTo.getSecond();
        return findLinksOnShortestPaths(fromNode, toNode, instance);
    }

    /**
     * Generates random number for duration between pre specified bounds.
     *
     * @return the generated duration
     */
    private int getRandomAcceptableDuration() {
        return randomGenerator.nextInt(ParametersBG.UPPER_BOUND_STREAM_DURATION -
                ParametersBG.LOWER_BOUND_STREAM_DURATION) + ParametersBG.LOWER_BOUND_STREAM_DURATION;
    }

    /**
     * Returns random number for release with respect to proposed start time, so that it corresponds
     * to specified percentage of the period.
     *
     * @param period         Period of generated stream
     * @param startTime      Start time of generated stream
     * @param earliestFinish Earliest finish of the generated stream
     * @return Random number for release time based on described constraints, -1 if it was not possible to generate it
     */
    private int generateRelease(int period, int startTime, int earliestFinish) {
        int gap = (int) Math.floor(ParametersBG.UPPER_BOUND_RELEASE_DEADLINE_GAP * period - (earliestFinish - startTime));
        if (gap < 0) {
            return -1;
        }
        gap = Math.min(gap, startTime);

        if (gap == 0) return startTime;
        return startTime - randomGenerator.nextInt(gap);
    }

    /**
     * Returns random number for deadline with respect to proposed release time, so that it corresponds
     * to specified percentage of the period.
     *
     * @param period         Period of generated stream
     * @param release        Release time of generated stream
     * @param earliestFinish Earliest finish of the generated stream
     * @return Random number for deadline time based on described constraints, -1 if it was not possible to generate it
     */
    private int generateDeadline(int period, int release, int earliestFinish) {
        int upperGap = (int) Math.floor(ParametersBG.UPPER_BOUND_RELEASE_DEADLINE_GAP * period);
        int lowerGap = Math.max((int) Math.floor(ParametersBG.LOWER_BOUND_RELEASE_DEADLINE_GAP * period), (earliestFinish - release));


        upperGap = Math.min(upperGap, period - release);

        if (upperGap < lowerGap) {
            return -1;
        }

        if (upperGap == lowerGap) return earliestFinish;

        int randomIntLength = randomGenerator.nextInt(upperGap - lowerGap) + lowerGap;

        return release + randomIntLength;
    }

    /**
     * Tries to place stream of specified route and period to the run. If successful saves the stream to the stream list.
     *
     * @param linkRoute       Route of the stream
     * @param linkSchedule    Current run for links on the route
     * @param period          Period of the stream
     * @param numStreamRep    Number of stream repetitions in the hyper period
     * @param streamListProto Proto structure to which the stream is saved
     * @param streamID        Index of the stream
     * @param nodes           NodesInfoPanel in the network
     * @return True id the stream was successfully placed to run nad saved, false otherwise
     * @throws ImplementationException in case of invalid schedule slot assignment
     */
    private boolean tryToPlace(List<Link> linkRoute, List<LinkTimeSlots> linkSchedule, int period, int numStreamRep,
                               StreamList.StreamListProto.Builder streamListProto, int streamID, List<Node> nodes) throws ImplementationException {
        List<LinkTimeSlots> routeSchedules = new ArrayList<>();
        List<Integer> earliestStartTimes = new ArrayList<>();
        int duration, release, lastWeight, deadline, earliestFinish, lastTimeLag;

        for (Link link : linkRoute) {
            routeSchedules.add(linkSchedule.get(link.getId()));
            earliestStartTimes.add(-1);
        }

        duration = findSlots(earliestStartTimes, routeSchedules, period, numStreamRep, nodes);
        if (duration == -1) return false;

        lastWeight = routeSchedules.get(routeSchedules.size() - 1).getLink().getWeight();
        lastTimeLag = routeSchedules.get(routeSchedules.size() - 1).getLink().getTimeLag();
        earliestFinish = earliestStartTimes.get(earliestStartTimes.size() - 1) + (int) Math.ceil(duration * lastWeight) + lastTimeLag;

        if (ParametersBG.UPPER_BOUND_RELEASE_DEADLINE_GAP == -1 || ParametersBG.LOWER_BOUND_RELEASE_DEADLINE_GAP == -1) {
            release = 0;
            deadline = Integer.MAX_VALUE;
        } else {
            release = generateRelease(period, earliestStartTimes.get(0), earliestFinish);
            deadline = generateDeadline(period, release, earliestFinish);
        }
        if (release == -1 || deadline == -1) return false;

        markIntervalsFull(earliestStartTimes, routeSchedules, linkSchedule, duration, period, numStreamRep);
        ProtoHandler.addStreamToProto(streamListProto, streamID, nodes.get(linkRoute.get(0).getFrom()).getName(),
                nodes.get(linkRoute.get(linkRoute.size() - 1).getTo()).getName(), duration, period, release, deadline);

        return true;
    }

    /**
     * Initializes the priority queue with all possible combinations of leaf nodes
     *
     * @param leafNodes   List od end systems
     * @param firstPeriod First period from the period set
     * @return Priority queue of end system combinations
     */
    private PriorityQueue<CustomPair<Integer, Integer>> getPQ(List<Integer> leafNodes, int firstPeriod, ProblemInstance instance) throws InvalidInputDataException, ArrayNotInitializedException {
        PriorityQueue<CustomPair<Integer, Integer>> pQ = new PriorityQueue<>();
        int from, to;
        if (leafNodes.size() > 0 && leafNodes.get(0) == -1) {
            from = instance.getNodeIDFromString(ProtoHandler.nodePrefix + leafNodes.get(0));
            for (int i = 1; i < leafNodes.size(); i++) {
                to = instance.getNodeIDFromString(ProtoHandler.nodePrefix + leafNodes.get(i));
                pQ.add(new CustomPair<>(from, to, firstPeriod, randomGenerator.nextInt(1000) - 1000));
                pQ.add(new CustomPair<>(to, from, firstPeriod, randomGenerator.nextInt(1000) - 1000));
            }
        } else {
            for (int i = 0; i < leafNodes.size(); i++) {
                for (int j = i + 1; j < leafNodes.size(); j++) {
                    from = instance.getNodeIDFromString(ProtoHandler.nodePrefix + leafNodes.get(i));
                    to = instance.getNodeIDFromString(ProtoHandler.nodePrefix + leafNodes.get(j));
                    pQ.add(new CustomPair<>(from, to, firstPeriod, randomGenerator.nextInt(1000) - 1000));
                    pQ.add(new CustomPair<>(to, from, firstPeriod, randomGenerator.nextInt(1000) - 1000));
                }
            }
        }
        return pQ;
    }

    /**
     * Marks run intervals unavailable for the currently scheduled stream.
     *
     * @param earliestStartTimes Start times of the stream instances in the first period
     * @param routeSchedules     Schedules of the links on the route
     * @param linkSchedule       Schedules of all links in the network
     * @param streamDuration     Duration of the stream
     * @param period             Period of the stream
     * @param numInHP            Number of stream repetitions
     * @throws ImplementationException in case of invalid schedule slot assignment
     */
    private void markIntervalsFull(List<Integer> earliestStartTimes, List<LinkTimeSlots> routeSchedules,
                                   List<LinkTimeSlots> linkSchedule, int streamDuration, int period, int numInHP) throws ImplementationException {
        for (int j = 0; j < routeSchedules.size(); j++) {
            Link link = routeSchedules.get(j).getLink();
            int duration = (int) Math.ceil(streamDuration * link.getWeight() + link.getTimeLag());
            for (int k = 0; k < numInHP; k++) {
                linkSchedule.get(link.getId())
                        .removeFreeTimeSlot(earliestStartTimes.get(j) + k * period, duration);
            }
        }

    }

    /**
     * For all links on stream route checks schedules and find first available slot to which the stream fits in all its periods
     * (zero jitter constraint applies).
     *
     * @param earliestStartTimes List of earliest start times of stream on links in the first period
     * @param routeSchedules     Schedules of links on streams route
     * @param period             Period of the stream
     * @param numInHP            Number of repetitions in the hyper period
     * @param nodes              List of all nodes in the network
     * @return stream duration in case of success, -1 otherwise
     */
    private int findSlots(List<Integer> earliestStartTimes, List<LinkTimeSlots> routeSchedules, int period, int numInHP, List<Node> nodes) {
        boolean found = false;
        int prevEnd, duration;
        Link link;
        int streamDuration = getRandomAcceptableDuration();

        while (!found && streamDuration >= ParametersBG.LOWER_BOUND_STREAM_DURATION) {
            found = true;
            link = routeSchedules.get(0).getLink();
            duration = streamDuration * link.getWeight() + link.getTimeLag();
            earliestStartTimes.set(0, routeSchedules.get(0).getFirstAllFitSlotID(duration, period, numInHP, 0, period, 0));

            if (earliestStartTimes.get(0) == -1) {
                found = false;
                streamDuration -= 1;
                continue;
            }

            prevEnd = (earliestStartTimes.get(0) + duration) + nodes.get(link.getTo()).getTimeLag();

            for (int j = 1; j < routeSchedules.size(); j++) {
                link = routeSchedules.get(j).getLink();
                duration = streamDuration * link.getWeight() + link.getTimeLag();
                earliestStartTimes.set(j, routeSchedules.get(j).getFirstAllFitSlotID(duration, period, numInHP, prevEnd, period, 0));
                if (earliestStartTimes.get(j) == -1) {
                    found = false;
                    streamDuration -= 1;
                    break;
                }
                prevEnd = (earliestStartTimes.get(j) + duration) + nodes.get(link.getTo()).getTimeLag();
            }
        }

        return found ? streamDuration : -1;
    }

    /**
     * Initializes the run to have all time intervals in the hyper period available on all links.
     *
     * @param links All links in the network
     * @param HP    Hyper period of all stream periods
     * @return list of all links run
     */
    private ArrayList<LinkTimeSlots> linkScheduleInit(List<Link> links, int HP) {
        ArrayList<LinkTimeSlots> linkSchedule = new ArrayList<>();
        for (Link link : links) {
            LinkedList<Integer> freeSlotStarts = new LinkedList<>();
            LinkedList<Integer> freeSlotEnds = new LinkedList<>();
            freeSlotStarts.add(0);
            freeSlotEnds.add(HP);
            linkSchedule.add(new LinkTimeSlots(freeSlotStarts, freeSlotEnds, link));
        }
        return linkSchedule;
    }

    /**
     * Finds route from origin node to target node using BFS.
     *
     * @param origin   Origin node of the stream
     * @param target   Target node of the stream
     * @param instance Problem instance
     * @return Sequence of links being crossed on the route from origin to destination
     * @throws TopologyInitializationException in case of network initialization error
     */
    private List<Link> findLinksOnShortestPaths(int origin, int target, ProblemInstance instance) throws TopologyInitializationException {
        List<Node> nodes = instance.getNodes();
        boolean[] visited = new boolean[nodes.size()];
        Node[] predecessors = new Node[nodes.size()];
        List<Link> pathLinks = new ArrayList<>();
        LinkedList<Node> q = new LinkedList<>();
        List<Node> path = new ArrayList<>();
        boolean pathFound = false;
        Node curNode;

        q.add(nodes.get(origin));
        visited[nodes.get(origin).getId()] = true;

        while (!q.isEmpty()) {
            curNode = q.poll();
            if (curNode == null) break;
            if (curNode.equals(nodes.get(target))) {
                pathFound = true;
                break;
            }
            for (Node child : curNode.getNeighbors()) {
                if (!visited[child.getId()]) {
                    q.add(child);
                    predecessors[child.getId()] = curNode;
                    visited[child.getId()] = true;
                }
            }
        }
        if (pathFound) {
            curNode = nodes.get(target);
            while (!curNode.equals(nodes.get(origin))) {
                path.add(curNode);
                curNode = predecessors[curNode.getId()];
            }
            path.add(curNode);
            Collections.reverse(path);
            for (int i = 0; i < path.size() - 1; i++) {
                pathLinks.add(instance.getLinkFromNodes(path.get(i).getName(), path.get(i + 1).getName()));
            }
        }

        return pathLinks;
    }
}