package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;

import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Most Required Time Earliest Deadline First Fit Heuristics</h3>
 * <p>
 * One pass heuristic method. Streams are sorted by decreasing number of total required time on resources and then by deadline.
 * <p>
 * Each stream instance is placed to first available slot.
 */

public class MRT_EDF_FF extends Heuristics {
    public MRT_EDF_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.MRT_EDF_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) {
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        int value;
        for (Stream stream : getProblemInstance().getStreams()) {
            value = stream.getProblemInstance().getHP() - stream.getMinimalRequiredTime() * stream.getNumHPInstances();
            pQ.add(new ObjectPriorities<>(stream, value, stream.getDeadline()));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamsFirstFit(pQ);
    }

}