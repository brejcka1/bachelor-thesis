package cz.cvut.ciirc.exceptions;

/**
 * Parent class to all checked Exceptions.
 * The child exceptions are very simple, documented only by its name.
 */
public class CustomException extends Exception {

    public CustomException(String s) {
        super(s);
    }

    public CustomException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
