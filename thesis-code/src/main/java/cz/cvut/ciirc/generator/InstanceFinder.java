package cz.cvut.ciirc.generator;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.Scheduler;
import cz.cvut.ciirc.SchedulerConstants;
import cz.cvut.ciirc.data_format.InputFile;
import cz.cvut.ciirc.data_format.LinkList;
import cz.cvut.ciirc.data_format.NodeList;
import cz.cvut.ciirc.data_format.StreamList;
import cz.cvut.ciirc.exceptions.*;
import cz.cvut.ciirc.helper.CustomPair;
import cz.cvut.ciirc.helper.ProtoHandler;
import cz.cvut.ciirc.helper.StaticMath;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import cz.cvut.ciirc.network_and_traffic_model.Node;
import cz.cvut.ciirc.scheduling.Solution;
import cz.cvut.ciirc.scheduling.Solver;
import cz.cvut.ciirc.scheduling.Statistics;
import cz.cvut.ciirc.scheduling.baseline_methods.CBJ_BM;
import cz.cvut.ciirc.scheduling.helper_classes.LinkTimeSlots;
import cz.cvut.ciirc.scheduling.multi_pass.CBJ_BM_D;

import java.nio.file.Path;
import java.util.*;

/**
 * Main class for generating experiment instances,
 */
public class InstanceFinder extends InstanceGenerator {

    /**
     * Instance generator constructor.
     *
     * @param randomGenerator Random with a seed to generate reproducible instances
     */
    private InstanceFinder(Random randomGenerator) {
        super(randomGenerator);
    }

    /**
     * Main method to run - can switch between main_local() or main_server()
     *
     * @param args No arguments expected
     */
    public static void main(String[] args) {
        try {
            main_local();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates instances in one folder. Good for debugging.
     */
    private static void main_local() throws CustomException {
        long start = System.nanoTime();
        int numInstances = 1000000;
        int numReStreamInstances;
        int maxNumRSI = 50;
        int dirID = 666;
        InstanceFinder instanceFinder;
        boolean found = false;

        List<AlgorithmType> classes = new ArrayList<>();
        Path resPath, tmpPath;

        classes.addAll(Arrays.asList(SchedulerConstants.cbj));


        for (numReStreamInstances = 5; numReStreamInstances < maxNumRSI; numReStreamInstances += 5) {
            //if (found) break;
            for (int tryID = 0; tryID < numInstances; tryID++) {
                //if (found) break;
                instanceFinder = new InstanceFinder(new Random(numReStreamInstances * (tryID + 1)));
                if (numReStreamInstances % 100 == 0) System.out.println("Generating instance with " + numReStreamInstances + " RSI - try: " + tryID + "/" + numInstances);
                int fileID = ProtoHandler.findFirstFreeFileID(dirID);
                InputFile.InputFileProto inputProto = instanceFinder.generateInstance(fileID, ProtoHandler.getInstanceFilePath(dirID, fileID),
                        tryID % 3, ParametersBG.SMALL_INSTANCE, numReStreamInstances, tryID % 4);

                Solution solution1 = instanceFinder.schedule(classes.get(0), inputProto);
                Solution solution2 = instanceFinder.schedule(classes.get(1), inputProto);

                if (solution1 != null && solution2 != null &&
                        solution1.getObjValue() != -1 && solution2.getObjValue() != -1 &&
                        solution1.getObjValue() > solution2.getObjValue()) {
                    System.out.println("-----------------> Objective values of fID:" + fileID + "/dir:" + dirID + " are "+ solution1.getObjValue() + " and " + solution2.getObjValue());
                    resPath = ProtoHandler.getResultFilePath(dirID, fileID);
                    tmpPath = resPath.getParent();
                    ProtoHandler.writeProto(tmpPath.resolve("result_" + fileID + "_CBJ_BM.pb"), solution1.getOutputProto());
                    ProtoHandler.writeProto(tmpPath.resolve("result_" + fileID + "_CBJ_BM_D.pb"), solution2.getOutputProto());
                    ProtoHandler.writeProto(ProtoHandler.getInstanceFilePath(dirID, fileID), inputProto);
                    //found = true;
                }
            }
        }
        printAlgDuration(start, numInstances);
    }

    public Solution schedule(AlgorithmType algorithmType, InputFile.InputFileProto inputFileProto) throws CustomException {
        Solver solver;
        Solution solution;

        ProblemInstance instance = new ProblemInstance(inputFileProto);
        try {
            instance.getUtilization();
        } catch (UtilizationException e) {
            System.err.println("Skipping FILE:" + e.getMessage());
            return null;
        }

        instance = new ProblemInstance(inputFileProto);
        solver = algorithmType.getSolver(instance);
        solver.solve(SchedulerConstants.DEFAULT_TIME_LIMIT);
        solution = solver.getSolution();
        solution.process();

        return solution;
    }
}