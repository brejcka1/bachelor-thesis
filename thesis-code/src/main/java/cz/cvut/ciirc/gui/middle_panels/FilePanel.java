package cz.cvut.ciirc.gui.middle_panels;

import cz.cvut.ciirc.gui.GUIConstants;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;

import static cz.cvut.ciirc.gui.GUIConstants.mediumBold;
import static cz.cvut.ciirc.gui.GUIConstants.small;

/**
 * Class that represents one of the first screens that allow user to choose instance from the file manager.
 */
public class FilePanel extends JPanel {
    private final Path instancePath;
    private JFileChooser jfc;

    public FilePanel(Path instancePath) {
        this.instancePath = instancePath;
        init();
    }

    private void init() {
        setLayout(new BorderLayout());
        setBorder(GUIConstants.border);

        JLabel label = new JLabel("<HTML>Please choose: folder + instance file<HTML>", SwingConstants.CENTER);
        label.setFont(mediumBold);
        label.setForeground(GUIConstants.warning);

        jfc = new JFileChooser(instancePath.toString());
        jfc.setPreferredSize(new Dimension(1200, 800));
        setFileChooserFont(jfc.getComponents());

        add(jfc, BorderLayout.CENTER);
        add(label, BorderLayout.PAGE_START);
    }

    private void setFileChooserFont(Component[] comp) {
        for (int x = 0; x < comp.length; x++) {
            if (comp[x] instanceof Container) setFileChooserFont(((Container) comp[x]).getComponents());
            try {
                comp[x].setFont(small);
            } catch (Exception e) {
            }//do nothing
        }
    }


    public JFileChooser getJfc() {
        return jfc;
    }

    public void setFolder() {
        this.jfc.setCurrentDirectory(instancePath.toFile());
    }
}
