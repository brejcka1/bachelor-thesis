package cz.cvut.ciirc.scheduling.baseline_methods;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.GurobiException;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import cz.cvut.ciirc.network_and_traffic_model.ReoccurredStreamInstance;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import cz.cvut.ciirc.scheduling.Solution;
import cz.cvut.ciirc.scheduling.Solver;
import gurobi.*;

import java.util.List;
import java.util.Optional;

/**
 * Integer Linear Program model using Gurobi solver.
 * <p>
 * Gurobi must be installed and license obtained to run this piece of code.
 */

public class ILP extends Solver {
    public ILP(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.ILP);
    }

    @Override
    public void solve(int timeLimit) throws RuntimeException, GurobiException, ArrayNotInitializedException {
        long start = System.nanoTime();
        boolean success;
        try {
            success = schedule(timeLimit);
        } catch (GRBException e) {
            throw new GurobiException("Gurobi error, please see the cause");
        }
        setElapsedTime((System.nanoTime() - start) / (1.0 * (Math.pow(10, 9))));
        setSolution(new Solution(getProblemInstance(), getAlgorithmType(), success, getElapsedTime()));
    }

    private boolean schedule(int timeLimitSec) throws GRBException, ArrayNotInitializedException {
        boolean found = false;
        GRBEnv env = null;
        GRBModel model = null;

        // model init ----------------------------------------------------------
        try {
            synchronized (this) {
                env = new GRBEnv();  // create new environment
            }
            model = new GRBModel(env);  // create empty optimization model

            model.set(GRB.IntParam.OutputFlag, 0); // set to 1 for debug output
            model.set(GRB.IntParam.Threads, 1);
            model.set(GRB.DoubleParam.TimeLimit, timeLimitSec);


            // variables init ------------------------------------------------------

            int bigM = getProblemInstance().getHP();
            int rightSide;
            int cID = 0;
            GRBLinExpr expr;
            Optional<Integer> minPeriod = getProblemInstance().getStreams().stream().map(Stream::getPeriod).min(Integer::compareTo);
            if (!minPeriod.isPresent())
                throw new ArrayNotInitializedException("Stream array or streams periods not initialized or ");
            int maxHPid = getProblemInstance().getHP() / minPeriod.get();
            GRBVar[][][] starts = new GRBVar[getProblemInstance().getStreams().size()][getProblemInstance().getLinks().size()][maxHPid];

            for (Stream stream : getProblemInstance().getStreams()) {
                for (int siID = 0; siID < stream.getStreamInstanceList().size(); siID++) {
                    StreamInstance si = stream.getStreamInstanceList().get(siID);
                    int obj = 0;
                    if (siID == 0) {
                        obj = -1;
                    } else if (siID == stream.getStreamInstanceList().size() - 1) {
                        obj = 1;
                    }
                    for (int hpID = 0; hpID < si.getNumInHP(); hpID++) {
                        starts[stream.getId()][si.getLink().getId()][hpID] =
                                model.addVar(stream.getRelease() + hpID * stream.getPeriod(),
                                        stream.getDeadline() + hpID * stream.getPeriod() - si.getReSIInHP(hpID).getDurationWithTimeLag(),
                                        obj, GRB.INTEGER, "rsi_j" + stream.getId() + "_k" + si.getLink().getId() + "_l" + hpID);
                        obj = 0;
                    }
                }
            }

            // constraint (1) - Link constraint
            List<ReoccurredStreamInstance> reSIOnLink;
            for (Link link : getProblemInstance().getLinks()) {
                reSIOnLink = getProblemInstance().getAllReoccurredStreamInstancesForLink(link.getId());
                for (int i = 0; i < reSIOnLink.size(); i++) {
                    for (int j = i + 1; j < reSIOnLink.size(); j++) {
                        ReoccurredStreamInstance reSI_i = reSIOnLink.get(i);
                        ReoccurredStreamInstance reSI_j = reSIOnLink.get(j);
                        // start_i + dur_i <= start_j + M*z
                        // start_i - start_j - M*z <= -dur_i
                        GRBVar z = model.addVar(0, 1, 0, GRB.BINARY,
                                "z_j" + reSI_i.getParent().getStream().getId()
                                        + "_l" + reSI_i.getHpID()
                                        + "_h" + reSI_j.getParent().getStream().getId()
                                        + "_i" + reSI_j.getHpID()
                                        + "_k" + link.getId()
                        );
                        expr = new GRBLinExpr();
                        expr.addTerm(1.0, starts[reSI_i.getParent().getStream().getId()][reSI_i.getParent().getLink().getId()][reSI_i.getHpID()]);
                        expr.addTerm(-1.0, starts[reSI_j.getParent().getStream().getId()][reSI_j.getParent().getLink().getId()][reSI_j.getHpID()]);
                        expr.addTerm(-bigM, z);
                        model.addConstr(expr, GRB.LESS_EQUAL, -reSI_i.getDuration(), "lc1_" + cID);
                        cID++;
                        // start_j + dur_j <= start_i + M * (1 - z)
                        // start_j - start_i + M*z <= M - dur_j
                        expr = new GRBLinExpr();
                        expr.addTerm(1.0, starts[reSI_j.getParent().getStream().getId()][reSI_j.getParent().getLink().getId()][reSI_j.getHpID()]);
                        expr.addTerm(-1.0, starts[reSI_i.getParent().getStream().getId()][reSI_i.getParent().getLink().getId()][reSI_i.getHpID()]);
                        expr.addTerm(bigM, z);
                        model.addConstr(expr, GRB.LESS_EQUAL, bigM - reSI_j.getDuration(), "lc2_" + cID);
                        cID++;
                    }
                }
            }
            model.update();

            cID = 0;
            // constraint (2) - precedence constraint
            for (Stream stream : getProblemInstance().getStreams()) {
                for (int siID = 1; siID < stream.getStreamInstanceList().size(); siID++) {
                    StreamInstance curSI = stream.getStreamInstanceList().get(siID);
                    StreamInstance prevSI = stream.getStreamInstanceList().get(siID - 1);
                    // previous start + previous duration + node time lag <= current start
                    // cur_start - prev_start >= prev_dur_lag + node_lag
                    for (int hpID = 0; hpID < prevSI.getNumInHP(); hpID++) {
                        expr = new GRBLinExpr();
                        expr.addTerm(1.0, starts[stream.getId()][curSI.getLink().getId()][hpID]);
                        expr.addTerm(-1.0, starts[stream.getId()][prevSI.getLink().getId()][hpID]);
                        rightSide = prevSI.getReSIInHP(hpID).getDurationWithTimeLag() +
                                getProblemInstance().getNodes().get(prevSI.getLink().getTo()).getTimeLag();
                        model.addConstr(expr, GRB.GREATER_EQUAL, rightSide, "pc_" + cID);
                        cID++;
                    }
                }
            }

            // constraint (3) - Zero Jitter
            cID = 0;
            for (Stream stream : getProblemInstance().getStreams()) {
                for (int siID = 0; siID < stream.getStreamInstanceList().size(); siID++) {
                    StreamInstance curSI = stream.getStreamInstanceList().get(siID);
                    // current start = prev start + period
                    // cur_start - prev_start = period
                    for (int hpID = 1; hpID < curSI.getNumInHP(); hpID++) {
                        expr = new GRBLinExpr();
                        expr.addTerm(1.0, starts[stream.getId()][curSI.getLink().getId()][hpID]);
                        expr.addTerm(-1.0, starts[stream.getId()][curSI.getLink().getId()][hpID - 1]);
                        model.addConstr(expr, GRB.EQUAL, stream.getPeriod(), "zjc_" + cID);
                        cID++;
                    }
                }
            }


            // constraint (4) - Release
            cID = 0;
            for (Stream stream : getProblemInstance().getStreams()) {
                // start_[first si, hpID=0] >= release
                expr = new GRBLinExpr();
                expr.addTerm(1.0, starts[stream.getId()][stream.getStreamInstanceList().get(0).getLink().getId()][0]);
                model.addConstr(expr, GRB.GREATER_EQUAL, stream.getRelease(), "rc_" + cID);
                cID++;
            }

            // constraint (5) - Deadline
            cID = 0;
            for (Stream stream : getProblemInstance().getStreams()) {
                // end_[last si, hpID=0] <= deadline
                // start_[last si, hpID=0] <= deadline - cur_dur_lag
                expr = new GRBLinExpr();
                int lastSIID = stream.getStreamInstanceList().size() - 1;
                expr.addTerm(1.0, starts[stream.getId()][stream.getStreamInstanceList().get(lastSIID).getLink().getId()][0]);
                model.addConstr(expr, GRB.LESS_EQUAL, stream.getDeadline() - stream.getStreamInstanceList().get(lastSIID).getReSIInHP(0).getDurationWithTimeLag(), "dc_" + cID);
                cID++;
            }


            // - set objective
            model.set(GRB.IntAttr.ModelSense, GRB.MINIMIZE);

            // call the solver ------------------------------------------------
            model.optimize();
            int status = model.get(GRB.IntAttr.Status);

            // decide what to do based on status
            if (status == GRB.Status.OPTIMAL || (status == GRB.Status.TIME_LIMIT && model.get(GRB.IntAttr.SolCount) > 0)) {
                //System.out.println("The optimal objective is " + model.get(GRB.DoubleAttr.ObjVal));
                int startTime;
                for (int i = 0; i < getProblemInstance().getStreams().size(); ++i) {
                    Stream stream = getProblemInstance().getStreams().get(i);
                    for (StreamInstance si : stream.getStreamInstanceList()) {
                        for (int hpID = 0; hpID < si.getNumInHP(); hpID++) {
                            startTime = (int) Math.round(starts[stream.getId()][si.getLink().getId()][hpID].get(GRB.DoubleAttr.X));
                            si.getReSIInHP(hpID).setStart(startTime);
                        }
                    }
                }
                found = true;
            } else {
                if (status == GRB.Status.INFEASIBLE) System.out.println("Instance infeasible");
                else if (status == GRB.Status.TIME_LIMIT) System.out.println("Time limit reached, no solution found");
                else System.out.println("Terminated from unknown reason, no solution found");
            }

            // write model to file
            // model.write("my_model.lp");
        } finally {
            // dispose model from memory
            if (model != null) model.dispose();
            if (env != null) env.dispose();
        }

        return found;
    }
}
