package cz.cvut.ciirc.gui.middle_panels.middle_components;

import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.gui.GUIConstants;
import cz.cvut.ciirc.network_and_traffic_model.Node;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

import static cz.cvut.ciirc.gui.GUIConstants.smallBold;

public class NodesInfoPanel extends JScrollPane {

    public NodesInfoPanel(ProblemInstance problemInstance) {

        JTable jTable;
        DefaultTableModel model = new DefaultTableModel();
        String[] columnNames = {"nodeID", "nodeName", "nodeTimeLag"};
        model.setColumnIdentifiers(columnNames);

        for (Node node : problemInstance.getNodes()) {
            List<String> list = new ArrayList<>();
            list.add(node.getId() + "");
            list.add(node.getName());
            list.add(node.getTimeLag() + "");
            model.addRow(list.toArray());
        }

        jTable = new JTable(model);

        jTable.getTableHeader().setFont(smallBold);
        jTable.setFont(GUIConstants.small);
        jTable.setRowHeight(GUIConstants.small.getSize() + 5);


        jTable.setFillsViewportHeight(true);
        getViewport().add(jTable);
    }
}
