package cz.cvut.ciirc.exceptions;

public class InputIntegerOverFlowException extends CustomException {

    public InputIntegerOverFlowException(String s) {
        super(s);
    }

    public InputIntegerOverFlowException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
