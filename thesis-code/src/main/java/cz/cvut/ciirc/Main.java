package cz.cvut.ciirc;

import cz.cvut.ciirc.exceptions.CustomException;
import cz.cvut.ciirc.exceptions.ProgramArgumentsException;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 * Main class to run the program (locally and without GUI).
 * Reads input from input files and saves output to output file.
 * The file APIs are defined in proto buffer format.
 * The custom_path is specified by the location of your project.
 *
 * <p><h3>Recommended usage</h3>
 * It is not necessary to run this class to use the Scheduler. Depending on the intended usage
 * it may be more convenient to approach the code as a library and initialize the Scheduler
 * with Proto Buffer classes. In such case there are no IO operations regarding the input or output files.
 * On the other hand in case of larger tests with different problem instances, it may be more convenient to use
 * the option with IO access to input and output files. This Main allows to schedule several instances from the same
 * folder with IDs from firstInstance to lastInstance.
 *
 *
 * <p><h3>Program arguments</h3>
 * directoryID firstInstance lastInstance
 *
 * <p><h3>Input directory structure</h3>
 * The goal is to have the "instances" folder in custom_path. The folder "instances" contains
 * several folders named "instance_(directoryID)". Each folder "instance_(directoryID)" contains
 * input files named instance_(ID).pb.
 *
 * <p><h3>Output directory structure</h3>
 * The output files will be saved to the folder custom_path/results/result_(directoryID).
 * The name of the output file is result_(ID).pb.
 */
public class Main {

    /**
     * Parses arguments and runs the scheduler.
     *
     * @param args in format {directoryID, firstInstance, lastInstance}
     */
    public static void main(String[] args) {
        try {
            List<Integer> arguments = processInput(args);
            Scheduler scheduler = new Scheduler(true);
            scheduler.run(arguments.get(0), arguments.get(1), arguments.get(2));
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parse the string input arguments to integer list.
     *
     * @param args Program arguments
     * @return List of parsed arguments
     * @throws ProgramArgumentsException In case the arguments were not in proper format.
     */
    private static List<Integer> processInput(String[] args) throws ProgramArgumentsException {
        List<Integer> arguments = new ArrayList<>();
        if (args.length == 3) {
            try {
                arguments.add(Integer.parseInt(args[0]));
                arguments.add(Integer.parseInt(args[1]));
                arguments.add(Integer.parseInt(args[2]));
            } catch (Exception e) {
                throw new ProgramArgumentsException("Conversion String -> Int exception, all arguments must be integers separated by space", e);
            }
        } else {
            throw new ProgramArgumentsException("Invalid user input\n" +
                    "Not enough program arguments -> must set: directoryID firstInstance lastInstance");
        }
        return arguments;
    }
}
