package cz.cvut.ciirc.scheduling.baseline_methods;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;

import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;


/**
 * <p>One pass heuristic method that is useful for double checking that the generator generates schedulable
 * instances - everything should be schedulable by this solverMethods.
 *
 * <p>Streams are sorted by increasing ID.
 * For each stream each stream instance is placed to first available slot.
 */

public class CheckGenerator extends Heuristics {
    public CheckGenerator(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.BENCHMARK_GENERATOR_HEURISTICS);
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        int value;
        for (Stream stream : getProblemInstance().getStreams()) {
            value = stream.getId();
            pQ.add(new ObjectPriorities<>(stream, value));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamsFirstFit(pQ);
    }
}