package cz.cvut.ciirc.scheduling.multi_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.baseline_methods.CBJ_BM;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;
import cz.cvut.ciirc.scheduling.helper_classes.StreamInstanceInfo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * Conflicted backjumping and backmarking described with enhancements described in the text: domain reduction, clever queue sorting, step size based on scheduling progress
 */
@SuppressWarnings("Duplicates")
public class CBJ_BM_ID extends CBJ_BM {
    public CBJ_BM_ID(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.CBJ_BM_ID);
    }

    @Override
    protected int getStep(StreamInstanceInfo curVariable, int siiID, int numSII) {
        return 1 + 30 * siiID / numSII;
    }
}