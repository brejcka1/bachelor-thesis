package cz.cvut.ciirc.gui.components;

import javax.swing.*;
import java.awt.*;

import static cz.cvut.ciirc.gui.GUIConstants.small;

/**
 * Class MyButton
 *
 * @author KaterinaBrejchova
 * Created on 12/25/18
 */
public class MyButton extends JButton {
    public MyButton(String s) {
        super(s);
        setFont(small);
        setForeground(Color.BLACK);
        setBackground(Color.lightGray);
    }
}
