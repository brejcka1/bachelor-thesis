package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;
import cz.cvut.ciirc.scheduling.helper_classes.StreamInstanceInfo;

import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Earliest Deadline First Most Total Successors First Fit Heuristics</h3>
 * <p>
 * One pass heuristic method. Stream Instances are sorted by deadline and then by decreasing number of successors.
 * <p>
 * Each stream instance is placed to first available slot.
 */

public class EDF_MTS_FF extends Heuristics {
    public EDF_MTS_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.EDF_MTS_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) {
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        List<StreamInstanceInfo> siiList = initializeStreamInfoList();
        int value;

        for (StreamInstanceInfo curSii : siiList) {
            value = getProblemInstance().getLinks().size() - (curSii.getCurSI().getStream().getStreamInstanceList().size() - 1 - curSii.getIDinStreamList());
            pQ.add(new ObjectPriorities<>(curSii, curSii.getCurSI().getStream().getDeadline(), value));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamInstancesFirstFit(pQ);
    }
}