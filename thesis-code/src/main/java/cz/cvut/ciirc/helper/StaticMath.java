package cz.cvut.ciirc.helper;

import cz.cvut.ciirc.exceptions.InputIntegerOverFlowException;

import java.util.List;

public class StaticMath {

    /**
     * Method that counts least common multiple of all numbers in the list.
     * <p>
     * source: https://www.geeksforgeeks.org/lcm-of-given-array-elements/
     *
     * @param element_array In case of too large LCM (integer overflow)
     * @return Least common multiple of given list
     */
    public static int listLCM(List<Integer> element_array) throws InputIntegerOverFlowException {
        int curLCM = 1;
        int divisor = 2;

        while (true) {
            int counter = 0;
            boolean divisible = false;

            for (int i = 0; i < element_array.size(); i++) {
                if (element_array.get(i) == 0) {
                    return 0;
                } else if (element_array.get(i) < 0) {
                    element_array.set(i, element_array.get(i) * (-1));
                }
                if (element_array.get(i) == 1) {
                    counter++;
                }
                if (element_array.get(i) % divisor == 0) {
                    divisible = true;
                    element_array.set(i, element_array.get(i) / divisor);
                }
            }
            if (divisible) {
                try {
                    curLCM = Math.multiplyExact(curLCM, divisor);
                } catch (ArithmeticException e) {
                    throw new InputIntegerOverFlowException("LCM of periods is too large, Integer overflow", e);
                }
            } else {
                divisor++;
            }
            if (counter == element_array.size()) {
                return curLCM;
            }
        }
    }
}
