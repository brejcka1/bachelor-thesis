\section{Proposed Solution} \label{sec:solution}

In this chapter, we describe the proposed algorithms and provide pseudocode for better understandability. Two baseline algorithms were implemented to measure the quality of the algorithms -- ILP (exact method) and Random Heuristic.

The algorithms are evaluated by two different quality measures -- \textit{schedulability} and \textit{objective value} reached in the given time limit. Schedulability describes whether a consistent solution was found, meaning that all reoccurred stream instances have start time assigned and all constraints are satisfied. The usage of the time limit is especially important for the exact method to ensure that it finishes within a reasonable time. The calculation of the objective value is shown in the equation (\ref{obj}). The goal of the proposed algorithms is to be faster than the exact ILP method and to yield better results (with respect to schedulability or objective value) than the random method.

It is important to understand that ILP is a complete method -- in case it proves that the instance is not schedulable, no other method can find a solution. Similarly, in case the instance is schedulable, and ILP proves optimality, no other solution would have lower objective value. However, in the case a time limit is set, and ILP is preempted, the currently best solution is~yielded. Since it did not search the whole solution space, it gives us no guarantees about the optimality of the solution.

Paths for all streams were precomputed by breadth-first search algorithm to correspond to the first found paths with the lowest number of links. 

\subsection{Schedulability Conditions}

A necessary condition for the schedulability of the problem is that utilization of no link in the network exceeds 100 \%. Let us denote $S^{e_k}$ as a set of all streams routed through the link $e_k$. Then the following condition must apply:

\begin{equation*}
\forall e_k \in \mathcal{E}: \text{util}(e_k) \leq 1
\end{equation*}
where
\begin{equation}\label{util}
\text{util}(e_k) = \sum_{s_i \in {S^{e_k}}} {\frac{(e_k.w \cdot s_i.p)}{s_i.T}}
\end{equation}

\subsection{Integer Linear Programming} \label{solILP}

As described in Section \ref{ILP}, Integer Linear Programming solves optimization problem on discrete variables and linear constraints. To apply it to our problem, all the constraints must be~linearized to be in a form $\mathbf{a}^T \mathbf{x}\leq b$ where $b$ is a constant, $\mathbf{a}$ is a vector of constant values and $\mathbf{x}$ is a vector of variables.

Constraints (\ref{zjc}), (\ref{pc}), (\ref{rc}) and (\ref{dc}) are already in a linear form. The only constraint that needs to be linearized is \textit{Link Constraint} (\ref{lc}) which is in a disjunctive form. It can be modeled using a big-$M$ (a positive large enough constant) and a binary variable $y \in \{0, 1\}$ so that it~can "switch off" one of the inequalities.

Let us substitute
\begin{align*}
x_1 &\gets s_{j, l}^{e_k}.\phi\\
x_2 &\gets s_{h, i}^{e_k}.\phi\\
a &\gets s_j.p \cdot e_k.w\\
b &\gets s_h.p \cdot e_k.w
\end{align*}

The variables $x_1$, $x_2$ represent start times for reoccurred stream instances. Then \textit{Link Constraint} (\ref{lc}) for one valid triplet of $\{e_k, s_{j, l}^{e_k}, s_{h, i}^{e_k}\}$ is represented as $x_1 + a \leq x_2 \lor x_2 + b \leq x_1$. Using a big-$M$ notation:
\begin{align*}
	x_1 + a &\leq x_2  + M \cdot y\\
	x_2 + b &\leq x_1 + M \cdot (1 - y)
\end{align*}
and the derived \textit{Link constraint} is:
\begin{align*} \label{lc_lin}
	&\forall e_k \in \E, \forall s_{j, l}^{e_k}, s_{h, i}^{e_k}, (j, l) \neq (h, i):\\
		&z_{j, l, h, i}^{e_k} \in \{0, 1\}\\
		&s_{j, l}^{e_k}.\phi + s_j.p \cdot e_k.w \leq s_{h, i}^{e_k}.\phi + M \cdot z_{j, l, h, i}^{e_k}\\
		&s_{h, i}^{e_k}.\phi + s_h.p \cdot e_k.w \leq s_{j, l}^{e_k}.\phi + M \cdot (1 - z_{j, l, h, i}^{e_k})
\end{align*} 

Since the scope of the schedule is a hyper period ($HP$), no start time can be larger than the hyper period and we can use it as big-$M$ ($M = HP$).

\subsection{One Pass Heuristics}

One Pass Heuristics are methods that perform one iteration to create a schedule based on a priority sequence of streams (Algorithm \ref{onepass}). In such heuristics, streams are sequentially added to the schedule, and in case any of the streams cannot be placed to the schedule, the heuristics discard the instance and yield \textit{"no solution."} A well-sorted sequence of the streams is~crucial for the method success rate. Since the minimized objective is the end-to-end latency, Equation (\ref{obj}), we are attempting to place the streams as early to the schedule as possible which corresponds to the serial scheduling scheme described in Section \ref{ff}.

\begin{algorithm}[th]
\caption{One Pass Heuristic}\label{onepass}
\begin{algorithmic}[1]
\Procedure{schedule}{$problemInstance$}
	\State $PQ \gets priorityQueueInit(problemInstance)$
	\State $schedule \gets createScheduleFirstFit(PQ)$
	\State \Return schedule
\EndProcedure
\end{algorithmic}
\end{algorithm}

\subsubsection{First Fit Methods}
There are two main approaches to adopt for the first fit method: \textit{scheduling streams} and \textit{scheduling stream instances}. The first approach is called First Fit Streams (further referred to as FFS) and is described in Algorithm \ref{ffs}. It creates a priority queue containing the streams (the prioritizing rules will be discussed in the text below) and then polls (removes from the top of the queue) streams from the priority queue one after another and places all stream instances of the currently processed stream to the first available time slot in the schedule.

The second approach is called First Fit Stream Instances (further referred to as FFSI) and is described in Algorithm \ref{ffsi}. It also starts by creating the priority queue but this time the queue consists of stream instances, not the whole streams. This means that stream instances of the same stream are not necessarily placed right next to each other in the queue. However, as~mentioned in Chapter \ref{sec:background}, it is necessary to keep precedences in between the stream instances. If the stream instance $a$ is dependent on stream instance $b$, the stream instance $b$ must be placed above the stream instance $b$ in the priority queue. On the other hand, scheduling single stream instances provides more variability, allowing us to simply (with not much computational cost) update the priority queue. FFSI proceeds similarly as FFS~--~it~takes stream instances from the priority queue and places them to the first available slot in the schedule. Additionally, each time a stream instance is placed to the schedule, the priority queue is updated (in the case the currently placed stream instance affected the criteria value of other stream instances in the queue).

Both of the approaches use list \texttt{freeSlots} while searching for an empty space for the current stream instance. Each link has one instance of this list that corresponds to the sequence of free intervals, e.g. $\{0 - 10, 40 - 150, 180-200\}$. Considering this sample list, it would mean that we can schedule stream instance of duration at most 10 to the first slot, stream instance of duration at most 110 to the second slot, etc. In the worst case, the length of the list is~$HP/2$ which corresponds to unit-length slots separated by unit-length space. 

\begin{algorithm}[th]
\caption{FFS -- First Fit Stream}\label{ffs}
\begin{algorithmic}[1]
\Procedure{createScheduleFirstFitStream}{$PQ$}
	\State $schedule \gets$ initialize empty schedule for each link
	\State $freeSlots \gets$ initialize all links available throughout whole hyper period 	
	\While{$!PQ.empty()$}
		\State $stream \gets PQ.pop()$
		\For{$streamInstance \in stream$}
			\State $start = findFirstSlotForAllPeriods(streamInstance)$
			\If{start == -1} \Return null \EndIf
			\State $addToSchedule(streamInstance, start, stream.period)$
			\State $removeFromFreeSlots(streamInstance, start, stream.period)$
		\EndFor
	\EndWhile
	\State \Return schedule
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{algorithm}[th]
\caption{FFSI -- First Fit Stream Instance}\label{ffsi}
\begin{algorithmic}[1]
\Procedure{createScheduleFirstFitStreamInstance}{$PQ$}
	\State $schedule \gets$ initialize empty schedule for each link
	\State $freeSlots \gets$ initialize all links available throughout whole hyper period   
	\While{$!PQ.empty()$}
		\State $streamInstance \gets PQ.pop()$
		\State $start = findFirstSlotForAllPeriods(streamInstance)$
		\If{$start$ == -1} \Return null \EndIf
		\State $addToSchedule(streamInstance, start, stream.period)$
		\State $removeFromFreeSlots(streamInstance, start, stream.period)$
		\State $updatePQ(streamInstance, start, PQ)$
	\EndWhile
	\State \Return schedule
\EndProcedure
\end{algorithmic}
\end{algorithm}

\subsubsection{Priority Queue Ordering} \label{pqo}

As we mentioned above, the way in which the priority queue is sorted is very important~--~simply for the reason that one stream instance that cannot fit into the schedule causes the whole heuristic to fail. The priority queue is sorted by the lowest value of the first criterion and then in case of a draw by the lowest value of the second criterion. Since we quickly noticed that the deadline of the stream strongly affects the scheduling process, we decided that it will always play a role in one of the criteria.

More specifically, we designed two slightly different criteria that are using the deadline. The EDF (earliest deadline first) criterion corresponds to the value of $s_j.\tilde{d}$, and DF (deadline first) corresponds to the value of the deadline with lowered granularity $\lceil s_j.\tilde{d} / 100 \rceil$. Lowering the granularity of the deadline enables aggregating values that are close by to the same criterion value, and then the heuristic rule can more likely decide by the second criterion.

As a complement to these two deadline criteria, six other criteria were implemented. These criteria are formally described in Tables \ref{tab:rulesS} and \ref{tab:rulesSI}. The criteria values are calculated based on~different parameters like stream period, duration, utilization of the resources, number of~precedences or earliest/latest start time. Method $est(s_j^{e_k})$ is used to calculate the earliest start time of stream instance $s_j^{e_k}$ with respect to its predecessors and release time. Method $lst(s_j^{e_k})$ is used to calculate the latest start time of $s_j^{e_k}$ with respect to its successors and deadline. Both of the methods are defined recursively using the stream path definition $R_j~=~(R_{j, 0}, ... , R_{j, i}, ... R_{j, last})$.
\[ est(s_j^{R_{j, i}}) =
  \begin{cases}
	est(s_j^{R_{j, i - 1}}) + s_j.p \cdot R_{j, i - 1}.w + R_{j, i - 1}.l + R_{j, i - 1}.to.l   & \quad \text{if } i \geq 1 \\
	s_j.r  & \quad \text{if } i = 0 
  \end{cases}
\]
\[ lst(s_j^{R_{j, i}}) =
  \begin{cases}
	lst(s_j^{R_{j, i + 1}}) - R_{j, i}.w \cdot s_j.p - R_{j, i}.l - R_{j, i}.to.l& \quad \text{if } i \neq last \\
	s_j.\tilde{d} - R_{j, last}.w \cdot s_j.p - R_{j, last}.l   & \quad \text{if } i = last
  \end{cases}
\]


In total, we have two criteria suitable for FFS and four criteria suitable for FFSI. The following criteria use streams for calculating the criterion value. The criterion Most Required Time (MRT) calculates the minimal end-to-end latency (the minimal time it takes to transfer the stream from the origin to the target node throughout the network). The minimal end-to-end latency is subtracted from the hyper period to ensure that the stream with the largest end-to-end latency has the highest priority. The criterion Resource Equivalent Duration (RED) is similar to MRT with the difference that the stream transmission duration on each link is multiplied by a utilization coefficient determined by the given link.

The following criteria use stream instances for calculating the criterion value. The criterion Most Total Successors calculates the number of stream instances of the same stream that need to be scheduled after the current stream instance. The value is subtracted from the total number of links to ensure that the stream instance with the most successors has the highest priority. Since this value does not change throughout the scheduling process, the priority queue is not being updated. The formal definition uses stream instance id which is calculated based on stream path definition as $id(s_j^{R_{j, i}}) = i$. The criterion Earliest Start Time (EST) calculates the earliest possible start time based on the release time of the stream and duration of the preceding stream instances. This value is updated each time a preceding stream instance is~scheduled. The criterion Latest Start Time (LST) calculates the latest possible start time based on the deadline of the stream, the stream instance duration and the duration of~the succeeding stream instances. The criterion Minimum Slack (MSLK) calculates the length of~the interval during which the stream instance can start based on EST and LST.


As already mentioned above, the heuristic rule is created by two criteria and has one of~the three following structures:

\begin{itemize}
	\item 1. EDF, 2. Complement Criterion
	\item 1. Complement Criterion, 2. EDF
	\item 1. DF, 2. Complement Criterion
\end{itemize}

When creating all the possible rules corresponding to these structures (3 possible structures, 6 possible complement criteria) we end up with $6 \cdot 3 = 18$ one pass heuristics in total. We do not consider the rule structure \textit{1. Complement Criterion 2. DF} since it would yield very similar results as \textit{1. Complement Criterion, 2. EDF} rule structure.

\begin{table}[th]
	\centering
	\begin{tabular}{| l l  | }
	\hline
		\textbf{Shortcut} & \textbf{Rule name} \\ & \textbf{Criterion calculation}\\
		\hline \hline
		MRT & Most Required Time  \\ & $HP - HP/s_j.T \cdot \sum_{e_k \in R_j} (e_k.w \cdot s_j.p + e_k.l + e_k.to.l)
		  + R_{j, last}.to.l$\\
		  \hline
		RED & Resource Equivalent Duration  \\ & $10 \cdot HP - \sum_{e_k \in R_j} \lceil 10 \cdot util(e_k) \rceil \cdot (e_k.w \cdot s_j.p + e_k.l)$ \\
		\hline
	\end{tabular}
	\caption{List of complement criteria for $s_j$ and FFS scheduling}
	\label{tab:rulesS}
\end{table}

\begin{table}[th]
	\centering
	\begin{tabular}{| l l l | }
	\hline
		\textbf{Shortcut} & \textbf{Rule name} & \textbf{Criterion calculation}\\
		\hline \hline
		MTS & Most Total Successors & $|\E| - (|R_j| - id(s_j^{e_k}))$ \\
		EST & Earliest Start Time &  $est(s_j^{e_k})$ \\
		LST & Latest Start Time &  $lst(s_j^{e_k})$ \\
		MSLK & Minimum Slack &  $lst(s_j^{e_k})$ - $est(s_j^{e_k})$ \\
		\hline
	\end{tabular}
	\caption{List of complement criteria for $s_j^{e_k}$ and FFSI scheduling}
	\label{tab:rulesSI}
\end{table}


\subsubsection{Complexity Analysis}

For analyzing the complexity, we use common knowledge that the complexity of adding an element to priority queue is $\mathcal{O}(\log n)$ and retrieving an element from the queue is $\mathcal{O}(1)$. Further, we use graph diameter $d$ which corresponds to the maximum eccentricity in the graph, i.e., the length of the longest shortest path between any two nodes in the network graph. Other complexity parameters are the number of streams in the network $|S|$, hyper period $HP$ and the maximal period of any stream in the network $T$. Since we will often work with expression $|S| \cdot d$ corresponding to the maximal total number of stream instances in the network, we will substitute this expression as $|SI|$.

The complexity analysis of FFSI follows the pseudocode described in Algorithm \ref{ffsi}. The~steps of the algorithm that require non-linear time are: priority queue initialization and iterating over all stream instances while searching for a free slot and updating the priority queue. We~will calculate the complexity of the algorithm from the complexity of these steps.

Initialization of the $schedule$ and $freeSlots$ has linear complexity and is negligible compared to the priority queue initialization. In the iteration cycle, popping an element from the priority queue is $\mathcal{O}(1)$ and methods $addToSchedule(...)$ and $removeFromFreeSlots(...)$ work with time slots already found by method $findFirstSlotForAllPeriods(...)$ and their time complexity is constant. 

The complexity is calculated as follows:
\begin{itemize}
	\item \textbf{Priority queue init}:
		$|SI| \cdot \log |SI| + |SI| \approx \mathcal{O}(|SI| \cdot \log |SI|) $
			\\The member $|SI| \cdot \log |SI|$ corresponds to inserting all stream instances to the priority queue. The member $|SI|$ corresponds to calculating the criteria value which is linear with respect to the total number of stream instances in the network.
	\item \textbf{Iterate over all stream instances}: $|SI|$
		\begin{itemize}
			\item \textbf{Find free slots}: $(T/2) \cdot (HP/2) \approx \mathcal{O}(T \cdot HP)$ 
			\\The method goes through all the free slots in the list corresponding to the first periodical occurrence of the stream instance -- the largest possible start time is~at~most~$T-1$. The number of visited slots while searching for the correct slot is~at~most~$T/2$. Then it goes through the rest of the time slots in the lists and checks if there are the required free time slots available for the other reoccured stream instances in the hyper period. The total number of the time slots checked can be at most $HP / 2$.

			\item \textbf{Update priority queue}: $2 \cdot (d - 1) + (d - 1) \cdot \log |SI| \approx \mathcal{O}(d \cdot \log |SI|)$
			\\The method removes all the predecessors or successors (based on criterion type) of the currently scheduled stream instance from the priority queue. Since there are at most $d$ stream instances in each stream, the total number of such predecessors or successors can be at most $d - 1$. Then we update the criterion value of these removed stream instances which is again linear. Finally, we return them to the priority queue with complexity $\mathcal{O}((d-1)\cdot \log |SI|)$.
		\end{itemize}
\end{itemize}


Then the complexity of FFSI:
\begin{align*}
& \mathcal{O}(|SI| \cdot \log |SI| + |SI| \cdot (T \cdot HP + d \cdot \log |SI|)) \\
& \approx \mathcal{O}(|S| \cdot d \cdot (\log (d \cdot |S|) + T \cdot HP + d \cdot \log (d \cdot |S|)))\\
& \approx \mathcal{O} (|S| \cdot d \cdot (T \cdot HP + d \cdot \log (d \cdot |S|)))
\end{align*}

For FFS algorithm, the queue has only $S$ members. The number of iterations is then $S$, and finding of free slots runs $d$ times because we need to assign a start time to each stream instance of the stream. This results in the time complexity of $ \mathcal{O} (|S| \cdot (\log |S| + d \cdot T \cdot HP))$.

The complexity of the algorithms depends on the values of $T$, $HP$, $d$ and $|S|$. Hence, the algorithms belong to pseudo-polynomial complexity class. However, we must mention that especially the step \textit{find free slots} works with a very pessimistic upper bound on the size of the \texttt{freeSlots} list. In reality, the runtime of these methods is very low as described in Chapter~\ref{sec:experiments}.

\subsubsection{Random Heuristic}

The random heuristic is the second baseline method used for evaluating the performance of the other heuristics. The implementation is rather straightforward, it uses the First Fit Stream (FFS) method and priority queue where the criterion is equal to a random number.


\subsection{Multiple Pass Heuristics} \label{sec:multiplepass}

Multiple pass heuristics are methods that perform several iterations over the sequence of~streams, usually by using some form of backtracking. We based our multiple pass heuristics on the \textit{CBJ\_BM} exact method described in Section \ref{sec:csp}. Moreover, we enhanced the method by several techniques introduced in the text above -- \textit{priority rules}, \textit{est} and \textit{lst} functions, and enlarged granularity of the time units. The multiple pass heuristic is shown in~Algorithm~\ref{alg:multiplepass}. The contributions to the original code are marked by a comment in the Algorithm. In the following paragraphs, we will first describe the specifics of the \textit{CBJ\_BM} iterative method proposed by \cite{vlk_dp} and secondly show the applied heuristical enhancements in detail. Furthermore, we have implemented both the original version of the algorithm \textit{CBJ\_BM} and the multiple pass heuristics and compared their performance in Section \ref{sec:resMultiple}.
\begin{algorithm}[th]
\caption{Heuristic based on Conflict-Directed Backjumping with Backmarking by \cite{vlk_dp}}\label{alg:multiplepass}
\begin{algorithmic}[1]
\Procedure{Cbj-bm-based-heuristics}{$sInsts$} \textbf{returns} the solution or false
	\State $sInsts \gets$ sort($sInsts$, $\{DF, MRT, ID\}$) \Comment{One pass sorting}
	\State $siID \gets 0$
	\While {$siID < |sInsts|$}
		\If{$nextStart[siID] == 0$} \label{alg:start}
			\State $startTime, conflicts \gets getEstAndConflicts(sInsts, siID)$
			\State $confSet[siID] \gets confSet[siID] \cup conflicts$
		\Else
			\State $startTime \gets nextStart[siID]$ 
		\EndIf
		\State $successful \gets false$
		\While {$\neg successful \land startTime \leq lst(sInsts[siID])$} \Comment{Reduce domains}
			\If {$Mark[siID][startTime] < BackTo[siID][startTime]$} \label{alg:typeA}
				\State $confSet[siID] \gets confSet[siID] \cup \{Mark[siID][startTime]\}$
				\State $step \gets getStep()$ \Comment{Larger time granularity}
				\State $startTime \gets startTime + step$
				\Continue
			\EndIf
			\State $sInsts[siID] \gets startTime$
			\State $fail \gets false$
			\For {$j \gets BackTo[siID][startTime]$ \textbf{to} $siID - 1$} \label{alg:typeB}
				\If {$\neg Consistent(sInsts[j], sInsts[siID])$} \label{alg:consistent}
					\State $confSet[siID] \gets confSet[siID] \cup \{j\}$ \label{alg:conf}
					\State $Mark[siID][startTime] \gets j$
					\State $fail \gets true$
					\Break
				\EndIf
			\EndFor
			\If {$\neg fail$} \label{alg:updateFields}
				\State $Mark[siID][startTime] \gets siID - 1$
				\State $successful \gets true$
			\EndIf
			\State $BackTo[siID][startTime] \gets siID$
			\State $step \gets getStep()$ \Comment{Larger time granularity}
			\State $startTime \gets startTime + step$
		\EndWhile
		\If{$\neg successful$} \label{alg:back}
			\If{$confSet[siID] == \emptyset$} \label{alg:finish}
				\Return $false$
			\EndIf
			\State $j \gets Max(confSet[siID])$
			\State $confSet[j] \gets confSet[j] \cup confSet[siID] \setminus \{j\}$
			\For {$k \gets j + 1$ \textbf{to} $|sInsts| - 1$}
				\For {$v \gets 0$ \textbf{to} $HP - 1$}
					\State $BackTo[k][v] \gets Min(BackTo[k][v], j)$
				\EndFor
			\EndFor
			\While {$siID > j$}
				\State $nextStart[siID] \gets 0$ \Comment{Reduce domains}
				\State $confSet[siID] \gets \emptyset$
				\State $siID \gets siID - 1$
			\EndWhile
		\Else
			\State $nextStart[siID] \gets startTime$
			\State $siID \gets siID + 1$
		\EndIf
	 \EndWhile
	 \State \Return $sInsts$
\EndProcedure
\end{algorithmic}
\end{algorithm}

The core of the \textit{CBJ\_BM} algorithm is the while cycle, which is iterating over the sequence of stream instances $sInsts$ until all of them are scheduled or the solution is not found. The~algorithms workflow is similar to the CSP backtracking algorithm shown in Algorithm~\ref{alg:backtrack}~-- stream instances are sequentially being assigned start times from their domains, and in case there is a collision, the algorithm backtracks to the previously assigned stream instance. Additional features specific to \textit{CBJ\_BM} are applied. The original values of variables that are not initialized in the pseudocode are zero or empty set.

\subsubsection{Backjumping}

The field $conf[siID]$ consists of the stream instances that conflicted with $sInsts[siID]$. In case the algorithm finds a conflict, the set $conf[siID]$ is updated (line \ref{alg:conf}), and the next $startTime$ is selected for the current stream instance. In case it is necessary to backtrack (line \ref{alg:back}), we select the highest-indexed stream instance $j$ from the conflict set and backjump to it. This backjump allows us to skip the backtracking steps that would result in a redundant search; but at the same time no feasible solution is skipped. However, the reason why $sInsts[siID]$ could not be scheduled may be even some earlier scheduled stream instance than $sInsts[j]$. Therefore the conflict set $conf[j]$ is updated to contain all the plausible causes.

In case there is no stream instance in the conflict set of the currently scheduled stream instance, there may be two different scenarios. Firstly, the domain of the stream instance was empty, then the problem instance is infeasible. Secondly, we backtracked to $siID = 0$ and could not find a start time that would result into a feasible solution. Then, if we are working with $step = 1$, the scheduling problem is infeasible. Otherwise, if $step >1$, the problem may be infeasible or we may skip some solution.

\subsubsection{Backmarking}

\sloppypar
The field $Mark$ is a $|sInsts| \times HP$ array initialized to 0. Each time there is a conflict we update the array so that the $Mark[siID][startTime]$ corresponds to the lowest-indexed stream instance preventing the $startTime$ from being assigned to the $sInsts[siID]$.

\sloppypar
The field $BackTo$ is $|sInsts| \times HP$ array initially set to 0. The field $BackTo[siID][startTime]$ corresponds to the lowest-indexed stream instance which was reassigned after the current $sInsts[siID]$ was assigned with $startTime$.

These two fields help to reduce the number of consistency checks. The condition on the line~\ref{alg:typeA} is called \textit{type A saving}, it checks for a particular start time and stream instance, whether there is a variable for which the consistency checks already failed and still has the same value. In such a case, it would not make sense to continue because we know there is a conflict. The~for loop on the line \ref{alg:typeB} is called \textit{type B saving}. In this case, the $BackTo[siID][startTime]$ field represents up to which index the consistency checks already passed and we do not need to recheck them.


\subsubsection{Heuristic Enhancements}

To reduce the domains of the variables we use the $est(s_j^{e_k})$ and $lst(s_j^{e_k})$ functions introduced in Section \ref{pqo}. This reduction allows us to speed up the searching because only plausible start times (with respect to constraints (\ref{pc}), (\ref{rc}) and (\ref{dc})) are contained in the domains of~the stream instances. The variable $nextStart[siID]$ represents the first plausible start time from the domain that can be assigned to stream instance with $siID$ and is equal to either $est(s_j^{e_k})$ or the latest successfully assigned $startTime$ with added $step$ for the given stream instance $sInsts[siID]$. The upper bound on stream instance variable domain is set by $lst(s_j^{e_k})$. Since we are trying to minimize the end-to-end latency, we do not try to apply any domain ordering, because similarly as in the first fit methods introduced in the text above, we are trying to~place the stream instance to the first available time slot.

When we calculate the $startTime$ (line \ref{alg:start}), the algorithm either just backtracked to this place or we proceeded forward from the previously assigned variable. In case the algorithm backtracked, we already calculated the start time before and we will now use its updated value that is saved in $nextStart[siID]$. In case we proceeded forward, the $nextStart[siID]$ variable is initialized to zero and we need to calculate the earliest meaningful start time to avoid the redundant searches. The start time is set to $est(sInsts[siID])$, which may be further influenced by the already assigned preceeding stream instances of the same stream. We add such stream instances to the conflict set of $siID$.

At the beginning of the algorithm, we sort the stream instances based on one of the well performing one pass heuristics. The corresponding criteria would be \textit{DF} and \textit{MRT}. Since \textit{MRT} is criterion suitable for scheduling streams and in the \textit{CBJ\_BM} algorithm we are scheduling stream instances, we added the third criterion equal to the id of the stream instance in the stream instance sequence for the given stream $id(s_j^{e_k})$. This ensures that the stream instance precedences are kept in the created priority queue. Such sorting positively impacts the scheduling process because it helps to reduce the search time (see Section~\ref{sec:resMultiple}).


To speed up the search (which is necessary especially for large problems), we use a larger time granularity introduced by variable $step$. This implies a domain reduction that can possibly skip some solution (the algorithm is not complete) but on the other hand, it enables us to search the solution space faster. We evaluated three different methods for calculating the $step$ size based on:
\begin{enumerate}
	\item stream instance $s_j^{e_k}$ duration: $\lceil e_k.w \cdot s_j.p / 100 \rceil$ -- we call this method \textit{CBJ\_BM\_D}
	\item stream instance $s_j^{e_k}$ period: $\lfloor s_j.T / 500 \rfloor$ -- we call this method \textit{CBJ\_BM\_P}
	\item scheduling progress: $1 + \lfloor 30 \cdot siID / |sInsts| \rfloor$ -- we call this method \textit{CBJ\_BM\_ID}
\end{enumerate}

As a result of the applied enhancements, the consistency checking can be vastly reduced. The only constraint we need to check is the link overlapping, Constraint~(\ref{lc}). Constraint~(\ref{zjc}) can be skipped because we are scheduling the whole stream instance at a time, which is enforcing the zero jitter by deriving the start times of the respective reoccurred stream instances from the first periodical occurrence. Constraint (\ref{pc}) can be skipped because of the fact that we are always scheduling stream instances of the same stream in order that is keeping the precedences and we are using the $est(s_j^{e_k})$ function for domain reduction. Constraint (\ref{rc}) does not need to~be checked because it is already integrated in the $est(s_j^{e_k})$ function used for domain reduction. Similarly, Constraint (\ref{dc}) is also part of the domain reduction in the $lst(s_j^{e_k})$ function.



Additionally, we implemented the algorithm with $step = 1$ (called \textit{CBJ\_BM}) and also the algorithm based on the original pseudocode in \cite{vlk_dp} (referred to as \textit{CBJ\_BM\_NOH} where \textit{NOH} stands for "no heuristic") to compare the performance of the multiple pass heuristics with their exact version. The \textit{CBJ\_BM\_NOH} algorithm uses random sorting that keeps stream instance precedences, and the stream instances domains are represented by the interval $[s_j.r, s_j.\tilde{d}]$.






