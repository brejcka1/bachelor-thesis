package cz.cvut.ciirc.network_and_traffic_model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple class representing the node in network model.
 * For more information, see the Problem Statement.
 */
@Getter
public class Node {
    private final String name;
    private final int id;
    private int timeLag;
    private List<Integer> connectedLinksIDs;
    private List<Node> neighbors;


    public Node(String name, int id, int timeLag) {
        this.name = name;
        this.id = id;
        this.timeLag = timeLag;
        neighbors = new ArrayList<>();
        connectedLinksIDs = new ArrayList<>();
    }

    public void addNeighbor(Node n) {
        neighbors.add(n);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Node)) return false;
        Node item = (Node) o;

        return id == item.id;
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Method not implemented");
    }

    @Override
    public String toString() {
        return name;
    }
}