package cz.cvut.ciirc.scheduling;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.GurobiException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.exceptions.UtilizationException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Parent class for both solverMethods and baseline_methods methods.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PROTECTED)
public class Solver {
    /**
     * Problem instance to be solved
     */
    ProblemInstance problemInstance;
    /**
     * Found solution
     */
    @Getter(AccessLevel.PUBLIC)
    Solution solution = null;
    /**
     * Class describing the used algorithm
     */
    AlgorithmType algorithmType = null;
    /**
     * Time it took the algorithm to solve the instance
     */
    private double elapsedTime = -1;

    /**
     * Default constructor
     *
     * @param problemInstance Problem instance to be solved
     */
    public Solver(ProblemInstance problemInstance) {
        this.problemInstance = problemInstance;
    }

    /**
     * Method that solves the specified instance, must be overridden by the child methods
     *
     * @param timeLimitSec time limit for the solver
     * @throws RuntimeException             in case the method was not overridden
     * @throws ArrayNotInitializedException in case of initialization error
     * @throws ImplementationException      in case of initialization error
     * @throws UtilizationException         in case utilization was exceeded
     * @throws GurobiException              in case of problems with gurobi installation
     */
    public void solve(int timeLimitSec) throws RuntimeException, ArrayNotInitializedException, ImplementationException, UtilizationException, GurobiException {
        throw new RuntimeException("solve() must be overridden");
    }
}
