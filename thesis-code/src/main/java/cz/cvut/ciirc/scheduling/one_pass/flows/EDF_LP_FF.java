package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;

import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Earliest Deadline First Lowest Period First Fit Heuristics</h3>
 * <p>
 * One pass heuristic method. Streams are sorted by earliest deadline and then by increasing period.
 * <p>
 * For each stream each stream instance is placed to first available slot.
 */

public class EDF_LP_FF extends Heuristics {
    public EDF_LP_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.EDF_LP_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) {
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        int value;
        for (Stream stream : getProblemInstance().getStreams()) {
            value = stream.getPeriod();
            pQ.add(new ObjectPriorities<>(stream, stream.getDeadline(), value));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamsFirstFit(pQ);
    }
}