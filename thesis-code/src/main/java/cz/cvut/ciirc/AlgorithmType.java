package cz.cvut.ciirc;

import cz.cvut.ciirc.exceptions.SolverConstructorException;
import cz.cvut.ciirc.scheduling.Solver;
import cz.cvut.ciirc.scheduling.baseline_methods.CheckGenerator;
import cz.cvut.ciirc.scheduling.baseline_methods.ILP;
import cz.cvut.ciirc.scheduling.baseline_methods.RandomHeuristic;
import cz.cvut.ciirc.scheduling.one_pass.*;
import lombok.Getter;

import java.lang.reflect.Constructor;

/**
 * Enum aggregating all solver algorithms.
 * Contains link to its class and description.
 */
public enum AlgorithmType {
    BENCHMARK_GENERATOR_HEURISTICS(CheckGenerator.class, "Heuristics used for checking the correctness of Benchmark generator"),
    RANDOM_HEURISTICS(RandomHeuristic.class, "Heuristics used for comparing the correctness of non-random solverMethods"),

    LP_EDF_FF(LP_EDF_FF.class, "Least Period - Earliest deadline first - First Fit Heuristics"),
    MRT_EDF_FF(MRT_EDF_FF.class, "Most Required Time - Earliest deadline first - First Fit Heuristics"),
    MTS_EDF_FF(MTS_EDF_FF.class, "Most Total Successors - Earliest deadline first - First Fit Heuristics"),
    EST_EDF_FF(EST_EDF_FF.class, "Earliest start time - Earliest deadline first - First Fit Heuristics"),
    LST_EDF_FF(LST_EDF_FF.class, "Latest start time - Earliest deadline first - First Fit Heuristics"),
    MSLK_EDF_FF(MSLK_EDF_FF.class, "Minimum slack - Earliest deadline first - First Fit Heuristics"),
    RED_EDF_FF(RED_EDF_FF.class, "Minimum resource equivalent duration - Earliest deadline first - First Fit Heuristics"),
    SRED_EDF_FF(SRED_EDF_FF.class, "Minimum strong resource equivalent duration - Earliest deadline first - First Fit Heuristics"),
    RED_SI_EDF_FF(RED_SI_EDF_FF.class, "Minimum resource equivalent duration - Earliest deadline first - First Fit Heuristics"),
    SRED_SI_EDF_FF(SRED_SI_EDF_FF.class, "Minimum strong resource equivalent duration - Earliest deadline first - First Fit Heuristics"),


    EDF_LP_FF(EDF_LP_FF.class, "Earliest deadline first - Least Period - First Fit Heuristics"),
    EDF_MRT_FF(EDF_MRT_FF.class, "Earliest deadline first - Most Required Time - First Fit Heuristics"),
    EDF_MTS_FF(EDF_MTS_FF.class, "Earliest deadline first - Most Total Successors - First Fit Heuristics"),
    EDF_EST_FF(EDF_EST_FF.class, "Earliest deadline first - Earliest start time - First Fit Heuristics"),
    EDF_LST_FF(EDF_LST_FF.class, "Earliest deadline first - Latest start time - First Fit Heuristics"),
    EDF_MSLK_FF(EDF_MSLK_FF.class, "Earliest deadline first - Minimum slack - First Fit Heuristics"),
    EDF_RED_FF(EDF_RED_FF.class, "Earliest deadline first - Minimum resource equivalent duration - First Fit Heuristics"),
    EDF_SRED_FF(EDF_SRED_FF.class, "Earliest deadline first - Minimum strong resource equivalent duration - First Fit Heuristics"),
    EDF_RED_SI_FF(EDF_RED_SI_FF.class, "Earliest deadline first - Minimum resource equivalent duration - First Fit Heuristics"),
    EDF_SRED_SI_FF(EDF_SRED_SI_FF.class, "Earliest deadline first - Minimum strong resource equivalent duration - First Fit Heuristics"),


    DF_LP_FF(DF_LP_FF.class, "Earliest deadline first - Least Period - First Fit Heuristics"),
    DF_MRT_FF(DF_MRT_FF.class, "Earliest deadline first - Most Required Time - First Fit Heuristics"),
    DF_MTS_FF(DF_MTS_FF.class, "Earliest deadline first - Most Total Successors - First Fit Heuristics"),
    DF_EST_FF(DF_EST_FF.class, "Earliest deadline first - Earliest start time - First Fit Heuristics"),
    DF_LST_FF(DF_LST_FF.class, "Earliest deadline first - Latest start time - First Fit Heuristics"),
    DF_MSLK_FF(DF_MSLK_FF.class, "Earliest deadline first - Minimum slack - First Fit Heuristics"),
    DF_RED_FF(DF_RED_FF.class, "Earliest deadline first - Minimum resource equivalent duration - First Fit Heuristics"),
    DF_SRED_FF(DF_SRED_FF.class, "Earliest deadline first - Minimum strong resource equivalent duration - First Fit Heuristics"),
    DF_RED_SI_FF(DF_RED_SI_FF.class, "Earliest deadline first - Minimum resource equivalent duration - First Fit Heuristics"),
    DF_SRED_SI_FF(DF_SRED_SI_FF.class, "Earliest deadline first - Minimum strong resource equivalent duration - First Fit Heuristics"),

    CBJ_BM(cz.cvut.ciirc.scheduling.baseline_methods.CBJ_BM.class, "Conflicted backjumping with backmarking - enhanced ()"),
    CBJ_BM_D(cz.cvut.ciirc.scheduling.multi_pass.CBJ_BM_D.class, "Conflicted backjumping with backmarking - enhanced ()"),
    CBJ_BM_P(cz.cvut.ciirc.scheduling.multi_pass.CBJ_BM_P.class, "Conflicted backjumping with backmarking - enhanced ()"),
    CBJ_BM_ID(cz.cvut.ciirc.scheduling.multi_pass.CBJ_BM_ID.class, "Conflicted backjumping with backmarking - enhanced ()"),
    CBJ_BM_NOH(cz.cvut.ciirc.scheduling.baseline_methods.CBJ_BM_NOH.class, "Conflicted backjumping with backmarking - original method"),
    ILP(ILP.class, "Integer Linear Programming method - Baseline method");

    /**
     * Link to solver class
     */
    @Getter
    private final Class methodClass;
    /**
     * Method description
     */
    @Getter
    private final String methodDescription;

    /**
     * Default constructor
     *
     * @param methodClass       link to solver class
     * @param methodDescription method description
     */
    AlgorithmType(final Class methodClass, final String methodDescription) {
        this.methodClass = methodClass;
        this.methodDescription = methodDescription;
    }

    /**
     * Initializes the solver
     *
     * @param problemInstance problem instance to be solved
     * @return returns the solver
     * @throws SolverConstructorException in case the solver could not be initialized
     */
    public Solver getSolver(ProblemInstance problemInstance) throws SolverConstructorException {
        try {
            Constructor<?> constructor = ((Class<?>) methodClass).getDeclaredConstructor(ProblemInstance.class);
            return (Solver) constructor.newInstance(problemInstance);
        } catch (Exception e) {
            throw new SolverConstructorException("ERROR: Could not initialize Solver for given AlgorithmType", e);
        }
    }
}
