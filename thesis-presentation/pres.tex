\documentclass[c,xcolor=dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{listings}
\usepackage{lmodern}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{siunitx}
\usepackage{tabularx}

\usetikzlibrary{decorations}
\usetikzlibrary{decorations.pathreplacing}

\usepackage{textcomp}
\newcommand\us{\,\textmu{}s}

\usepackage[overlay,absolute]{textpos}
\TPGrid{10}{10}

\useoutertheme{cvut}
\usecolortheme{cvut}
\setbeamercolor*{titlelike}{parent=structure}
\setbeamertemplate{navigation symbols}{}

\setbeamercovered{transparent}
\newcommand{\clr}[1]{{\usebeamercolor[fg]{title} #1}}

\title[
  Heuristics for Periodic Scheduling
]{
  Heuristics for Periodic Scheduling
}
\subtitle{}
\author[
  Kateřina Brejchová
]{
  \textbf{Author}: Kateřina Brejchová\\
  \textbf{Supervisor}: Mgr. Marek Vlk \\
}

\institute{
  Czech Technical University in Prague \\
  Faculty of Electrical Engineering \\
  Open Informatics, Informatics and Computer Science \\
}

\date[Bachelor thesis] % (optional, should be abbreviation of conference name)
{
  \begin{small}
  \end{small}
}

\newcommand\includeanim[2][]{{\def\gfxattrs{#1}\input{anim/#2/overlay}}}

\graphicspath{{figures/}}

\begin{document}
\epstopdfDeclareGraphicsRule{.gp}{pdf}{.pdf}{../gp2pdf #1 \OutputFile}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Scheduling}
\begin{itemize}
\item Application: production, transportation, manufacturing, etc.
\item Process of assigning \clr{time intervals} on resources for given \clr{tasks} while following the predefined \clr{constraints}
\item Periodic $\rightarrow$ schedule scope is hyper period
\item $\mathcal{NP}$-complete $\rightarrow$ find fast heuristic method
\end{itemize}
\vfill
\includegraphics[width=0.9\textwidth]{period.pdf}
\begin{center}
\small
Scheduling: 1. Blue task with period 4; 2. Green task with period 2
\end{center}
\end{frame}

\begin{frame}
\frametitle{Thesis Outline}
\begin{itemize}
  \item \clr{Formulation} of the scheduling problem
  \item \clr{Design} of several heuristic methods
  \item \clr{Experiments} setup
  \item \clr{Comparison} with an exact approach
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Problem Statement: Inputs \& Goals}
\begin{table}[t]
\begin{tabularx}{\textwidth}{l l}
  Input & Network topology (nodes and links) \\
  & List of streams (communication)\\
  Goal & Find a schedule for the hyper period\\
  Objective & Minimize end-to-end latency 
\end{tabularx}
\end{table} 
\vfill
    \makebox[\linewidth]{
      \includegraphics[height=0.45\textheight]{network_2.pdf}
      \includegraphics[height=0.45\textheight]{gantt_2.png}
      }
  \begin{tiny}
    \begin{center}
    Input: Network and communication model \& Output: Found schedule for hyper period
    \end{center}
  \end{tiny}

\end{frame}

\begin{frame}
\frametitle{Solution Approaches}
\begin{enumerate}
  \onslide<1->{\item Exact baseline method - Integer Linear Programming
    \begin{itemize}
        \item Model defined in Gurobi solver
    \end{itemize}}
  \onslide<2->{\item One pass heuristics
    \begin{itemize}
        \item First fit approach to add tasks
        \item Criteria to create the task order
    \end{itemize}}
  \onslide<3->{\item Multiple pass heuristics
    \begin{itemize}
        \item Conflict-Directed Backjumping with Backmarking
          \begin{itemize}
            \item Search tree algorithm for CSP
          \end{itemize}
        \item Additional enhancements from one pass heuristics
          \begin{itemize}
            \item Queue sorting, domain reduction
            \item Larger time granularity
          \end{itemize}
    \end{itemize}}
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Experiments}
\begin{itemize}
  \item Artificially generated instances based on \footnotemark
  \item 540 $\cdot$ 100 instances in total; 60 seconds time limit per method
  \item Parameters: topology type, size, period set, number of packets
\end{itemize}
\begin{center}
  \includegraphics[height=0.5\textheight]{tree.png}
  \hspace{5mm}
  \includegraphics[height=0.5\textheight]{ring.png}

\begin{tiny}
Medium sized tree and ring topology
\end{tiny}
\end{center}

\footnotetext{\tiny Craciunas and Oliver. Combined task- and network-level scheduling for distributed time-triggered systems. Real-Time Syst., 2016.}
\end{frame}

\begin{frame}
\frametitle{Results -- Small topologies}
\begin{center}
  \includegraphics[height=0.8\textheight]{small.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Results -- Large topologies}
\begin{center}
  \includegraphics[height=0.8\textheight]{large.pdf}
\end{center}
\end{frame}


\begin{frame}
\frametitle{Conclusion}
\begin{itemize}
  \item Formal problem definition
  \item Baseline ILP model
  \item Heuristics design
  \item Performance test
\end{itemize}
\end{frame}

\begin{frame}
\Huge{\centerline{Thank you for your attention!}}
\end{frame}

\newcounter{finalframe}
\setcounter{finalframe}{\value{framenumber}}
\setcounter{framenumber}{\value{finalframe}}

\begin{frame}[noframenumbering]
\frametitle{Q0: ILP vs MILP problem definition}
  \begin{itemize}
    \item The problem is formulated as \clr{ILP}
    \item All the ILP variables are discrete
      \begin{itemize}
        \item $x_1$ and $x_2$ represent integer start times
        \item $z_{j,l,h,i}$ is a binary variable
      \end{itemize}
  \end{itemize}

\end{frame}


\begin{frame}[noframenumbering]
  \frametitle{Q1: Joint performance table of all heuristics}
\scriptsize 
\begin{table} 
\begin{minipage}{0.46\textwidth}
\begin{tabular}{lrrr}
\hline
\textbf{Method} & \textbf{Sched} & \textbf{Time} & \textbf{Best}\\
\hline
ILP          & 18885 & 48.53 & 18725 \\
CBJ\_BM\_D   & 44981 & 15.32 & 16358 \\
CBJ\_BM      & 44931 & 15.38 & 7793  \\
EDF\_MRT     & 40345 & 0.03  & 1040  \\
EDF\_RED     & 40343 & 0.06  & 1014  \\
DF\_EST      & 20918 & 0.01  & 547   \\
CBJ\_BM\_NOH & 4637  & 55.01 & 466   \\
DF\_RED      & 40267 & 0.06  & 384   \\
CBJ\_BM\_P   & 41780 & 14.35 & 354   \\
DF\_MRT      & 40199 & 0.03  & 308   \\
DF\_MTS      & 20385 & 0.01  & 271   \\
EDF\_LST     & 20788 & 0.01  & 223   \\
\vdots & \vdots & \vdots  & \vdots \\ 
    &&&\\
\hline
\end{tabular}
\end{minipage} \hfill
\begin{minipage}{0.46\textwidth}
\begin{tabular}{lrrr}
\hline
\textbf{Method} & \textbf{Sched} & \textbf{Time} & \textbf{Best}\\
\hline
\vdots & \vdots & \vdots & \vdots \\
MTS\_EDF     & 12811 & 0.01  & 216   \\
EDF\_EST     & 20796 & 0.01  & 214   \\
EDF\_MTS     & 20785 & 0.01  & 207   \\
CBJ\_BM\_ID  & 40731 & 15.35 & 183   \\
DF\_LST      & 20871 & 0.01  & 84    \\
LST\_EDF     & 20804 & 0.01  & 69    \\
RND          & 3352  & 0.00   & 12    \\
EST\_EDF     & 7631  & 0.01  & 3     \\
EDF\_MSLK    & 117   & 0.00   & 1     \\
MRT\_EDF     & 9102  & 0.00   & 0     \\
MSLK\_EDF    & 96    & 0.00   & 0     \\
RED\_EDF     & 3657  & 0.03  & 0     \\
DF\_MSLK     & 136   & 0.00   & 0    \\
\hline
\end{tabular}
\end{minipage}
\caption{Performance measures of methods on 54000 instances -- Number of scheduled instances, Average solving time, Best objective score}
\end{table}



\end{frame}

\begin{frame}[noframenumbering]
  \begin{block}{Q2: Percentage of optimal solutions found by Gurobi (ILP)}
    \begin{itemize}
    \item In total: 54000 instances 
    \item ILP found solution for: 18885 instances
    \item ILP had best objective value score for: 18725 instances
    \item ILP proved optimal solution for: 12811 instances
    \end{itemize}
  \end{block}
  \begin{block}{Q3: Objective value comparison on instances where ILP found the optimal solution}
    \begin{itemize}
      \item Heuristics did not find optimal solution on any of the instances
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}[noframenumbering] 
\frametitle{Difference in objective values on instances where ILP found optimal solution}
\scriptsize
\begin{table} 
\begin{minipage}{0.46\textwidth}
\begin{tabular}{lrr}
\hline
\textbf{Method} & \textbf{Sched} & \textbf{Opt\_diff [\%]}\\
\hline
ILP          & 12811 & 0.00      \\
MSLK\_EDF    & 96    & 23.05    \\
EDF\_MSLK    & 113   & 25.88    \\
DF\_MSLK     & 128   & 26.12    \\
RND          & 2690  & 51.58    \\
CBJ\_BM\_NOH & 3689  & 62.11    \\
RED\_EDF     & 2886  & 71.41    \\
MRT\_EDF     & 7015  & 88.80     \\
EST\_EDF     & 6930  & 90.81    \\
EDF\_MRT     & 12669 & 100.88   \\
EDF\_RED     & 12669 & 100.88   \\
CBJ\_BM      & 12796 & 101.54   \\
\vdots & \vdots & \vdots   \\ 
    &&\\
\hline
\end{tabular}
\end{minipage} \hfill
\begin{minipage}{0.46\textwidth}
\begin{tabular}{lrr}
\hline
\textbf{Method} & \textbf{Sched} & \textbf{Opt\_diff [\%]}\\
\hline
\vdots & \vdots & \vdots  \\
CBJ\_BM\_D   & 12796 & 101.66   \\
DF\_MRT      & 12610 & 101.70    \\
DF\_RED      & 12606 & 101.94   \\
CBJ\_BM\_ID  & 12756 & 111.34   \\
CBJ\_BM\_P   & 12792 & 112.71   \\
DF\_EST      & 10202 & 121.29   \\
EDF\_EST     & 10176 & 122.19   \\
EDF\_MTS     & 10176 & 122.22   \\
EDF\_LST     & 10176 & 122.22   \\
DF\_LST      & 10178 & 124.05   \\
DF\_MTS      & 10170 & 124.41   \\
LST\_EDF     & 10175 & 126.02   \\
MTS\_EDF     & 8650  & 127.69  \\
\hline
\end{tabular}
\end{minipage}
\caption{Performance measures of methods on 12811 instances where ILP found optimal solution -- Number of scheduled instances, Average difference from objective value of ILP}
\end{table}
\end{frame}


\begin{frame}[noframenumbering] 
\frametitle{Parameters of the generated instances}
\begin{center}
  \includegraphics[height=0.8\textheight]{util.pdf}
\end{center}
\end{frame}

\end{document}
