package cz.cvut.ciirc;

import static cz.cvut.ciirc.AlgorithmType.*;

/**
 * Class holding default constraints for the scheduler.
 */

public class SchedulerConstants {
    /**
     * Default start time for Reoccurred Stream Instances, marking that the Reoccurred Stream Instances was not scheduled.
     */
    public static final int DEFAULT_START_TIME = -1;
    /**
     * Default time limit in seconds for Solver methods.
     */
    public static final int DEFAULT_TIME_LIMIT = 60;
    /**
     * Constant that should prevent creating too many files in some instance folder.
     */
    public static final int MAX_NUM_INSTANCES_IN_FOLDER = 100000;

    /**
     * List of all heuristic methods.
     */
    public static AlgorithmType[] solverMethods = {ILP, RANDOM_HEURISTICS,
            CBJ_BM_NOH, CBJ_BM, CBJ_BM_D, CBJ_BM_P, CBJ_BM_ID,
            MRT_EDF_FF, MTS_EDF_FF,
            EST_EDF_FF, LST_EDF_FF,
            MSLK_EDF_FF, RED_EDF_FF,
            EDF_MRT_FF, EDF_MTS_FF,
            EDF_EST_FF, EDF_LST_FF,
            EDF_MSLK_FF, EDF_RED_FF,
            DF_MRT_FF, DF_MTS_FF,
            DF_EST_FF, DF_LST_FF,
            DF_MSLK_FF, DF_RED_FF
    };

    public static AlgorithmType[] cbj = {CBJ_BM, CBJ_BM_D};

    /**
     * List of all heuristic methods.
     */
    public static AlgorithmType[] solverMethodsGUI = {ILP, RANDOM_HEURISTICS,
            CBJ_BM_NOH, CBJ_BM, CBJ_BM_D, CBJ_BM_P, CBJ_BM_ID,
            MRT_EDF_FF, MTS_EDF_FF,
            EST_EDF_FF, LST_EDF_FF,
            MSLK_EDF_FF, RED_EDF_FF,
            EDF_MRT_FF, EDF_MTS_FF,
            EDF_EST_FF, EDF_LST_FF,
            EDF_MSLK_FF, EDF_RED_FF,
            DF_MRT_FF, DF_MTS_FF,
            DF_EST_FF, DF_LST_FF,
            DF_MSLK_FF, DF_RED_FF
    };
}
