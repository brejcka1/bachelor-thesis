\section{Introduction}

Scheduling is a common optimization problem with many applications. Following definitions in \cite{modra, zelena}, it is a decision-making process used in different areas such as production, transportation, manufacturing and information technology. The goal of a scheduling process is to assign time intervals on resources for given tasks while following the predefined constraints.

\textit{Resources} represent all the available objects for which the schedule is made. Resources may be of different types like machines, space, communication links, vehicles, school rooms, etc. 

\textit{Tasks} are jobs to be performed in accordance with their specification. Each task may have several dependencies (set of tasks that need to be executed in a predefined order), earliest start time, deadline, duration, and periodicity. Example of such task can be an airplane line, which goes from London to Prague and then from Prague to Budapest every week, and due to other usages, the aircraft can be used only on Monday and Tuesday. 

\textit{Constraints} are necessary conditions which ensure the schedule is plausible and fault-free. Correctly defined constraints are essential for the quality of the solution, and the definitions can be very complex. Example of such real-life constraint can be a parking slot where two cars cannot be parked on the same spot at the same time.

Currently, scheduling is widely used for communication in \textit{cyber-physical systems}. The cyber-physical system consists of physical devices and links connecting the devices. Links between the devices allow them to exchange information. Such an exchange of information between the physical devices is called \textit{stream}. However, the physical links also have their limitations and the more communication is distributed between the devices, the more sophisticated algorithm is necessary to control it.

Moreover, in highly critical systems such as automotive, avionics, etc., additional scheduling requirements such as determinism and guaranteed output are demanded \cite{galloway_net}. Any disturbance of these aspects may have severe consequences and cause undesired effects on safety (e.g., if the warning of an incoming person in a self-driving car is not delivered on time, the caused accident can result in people dying).

Typical communication in the network is of a control character -- the device is sending information about its current state which is being transmitted periodically in a regular time cycle. Apart from that, there are following specifics resulting from the nature of the highly critical industrial communication:
\begin{itemize}
  \item \textbf{Time-triggered} -- any operation in the system is determined by the globally synchronized clock and depends on the predefined schedule
  \item \textbf{No preemption} -- critical streams cannot be interrupted
  \item \textbf{Low end-to-end latency} -- response time must fit into $[release, deadline]$ time window representing a fraction of the stream period
  \item \textbf{Zero jitter} -- the variance of the response time must be zero
  \item \textbf{Time synchronized} -- the system is time synchronized with high precision
\end{itemize}

Apart from the considered scheduler determinism, other used resources like wires, vehicles or machines must also act deterministically, e.g., be resistant to external influences, etc. This aspect is not further discussed in this thesis but is considered a necessary condition \cite{galloway_net}.

As described in \cite{np} the zero jitter periodic scheduling with at least two different periods is a strongly NP-complete problem. While exact methods like Integer Linear Programming provide a proved optimal solution, the computational time of such solvers prevents it from being used for large scale systems.


On the other hand, heuristic methods can provide a feasible sub-optimal solution within a significantly smaller time frame. The purpose of this thesis is to introduce several heuristic algorithms that address the described form of the periodic scheduling problem and to compare their performance on the generated experimental setups. In addition, a graphical user interface is implemented to easily show schedule, topology, and setup of the solved problem.

The thesis is logically divided into several chapters as follows -- Chapter \ref{sec:background} overviews the related literature and state-of-the-art approaches and provides an introduction to heuristic methods, exact methods, and possible industrial application. Chapter \ref{sec:problem} formally describes the solved problem. Chapter \ref{sec:solution} is the core of this thesis and consists of algorithms designed for the given problem. Chapter \ref{sec:program} briefly describes the implementation (a full description of~the code -- ReadMe, JavaDoc, API definition and the code itself is provided on the enclosed CD). Chapter \ref{sec:experiments} describes instance setup, evaluation of the tested algorithms and discussion of the results. Chapter \ref{sec:conclusion} summarizes the thesis contribution.