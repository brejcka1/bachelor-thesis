package cz.cvut.ciirc.exceptions;

public class SolverConstructorException extends CustomException {

    public SolverConstructorException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
