\section{Problem Statement} \label{sec:problem}

The problem statement is motivated by communication in industrial networks and is~formulated as follows: Scheduling strictly periodic transmission over network links where each stream has a predefined path and links have different weights corresponding to transmission speeds. To adapt to the real-life situation we use time lags on nodes and links modeling the transmission and fabric switching delays in the industrial systems \cite{hanzalek_profinet}. Note that the domains of the parameters, such as transmission duration, are integral which corresponds to~real scheduling of production where the model is discretized, usually to a unit corresponding to~a~common network clock granularity.

\subsection{Network Model}
The network is modeled as a simple connected directed graph $\G = (\V, \E)$, where $\V$ is a~set of nodes (network devices) and $\E$ is a~set of links representing connections between the nodes.

Each node $v_i \in \V$ is~specified by its time lag $v_i.l \in \N$. Each link $e_k = (v_a, v_b) \in \E$ is~specified by its time lag $e_k.l \in \N$, weight $e_k.w \in \NN$, origin $e_k.from \in \V$, and target $e_k.to \in \V$. The link weight represents a link speed coefficient. The network is full duplex, meaning that $(v_a, v_b) \in \E \iff (v_b, v_a) \in \E$.

\begin{figure}[ht]
\centering
\includegraphics[height=0.33\textheight]{fig/model.png}
\caption{Visualization of the network and communication model}
\label{fig:communication_model}
\end{figure}

\subsection{Communication Model}
A stream is a periodic transmission from the origin node to the target node throughout the network. A set of all streams is denoted by $S$. Each stream $s_j \in S$ is specified by its duration $s_j.p \in \NN$, release date $s_j.r \in \N$, deadline $s_j.\tilde{d} \in \NN$, period $s_j.T \in \NN$, origin $s_j.org \in \V$, and target $s_j.trg \in \V$. Streams may have different periods resulting in common hyper period $HP \in \NN$ which is the least common multiple (\textit{LCM}) of all periods.

We denote the route of the stream $s_j$ as $R_j$ where $R_j = (e_{j^1}, ..., e_{j^n})$ is a sequence of~links that are visited on the way from $s_j.org$ to~$s_j.trg$. The last edge from the sequence $R_j$ is~denoted $R_{j, last}$. The stream instance $s_j^{e_k}$ represents the stream $s_j$ routed through the link $e_k$.

Figure \ref{fig:communication_model} depicts sample network communication. There are seven nodes and six links in~the network. Two streams $s_0$ and $s_1$ are sent from $node_5$ to $node_6$ and from $node_3$ to~$node_6$ respectively. This results in having 4 stream instances $\{s_0^{e_5}, s_0^{e_6},s_1^{e_3},s_1^{e_6}\}$ in the network. Routes of the streams are equal to $R_0 = (e_5, e_6)$ and $R_1 = (e_3, e_6)$.

The scope of the schedule is a hyper period. This results in $HP/s_j.T$ periodic repetitions of the stream instance. After completing the first hyper period, the schedule repeats itself, and it would be redundant to enlarge the scheduler time scope.

Since we are dealing with zero jitter scheduling, it is not theoretically necessary to use other granularity of the stream because each periodic repetition of the stream instance is~determined by its first occurrence. However, for the clearness of the notation, we will denote the reoccurred stream instance as $s_{j, l}^{e_k}$ where $l \in \{0, ..., \frac{HP}{s_j.T} - 1\}$.

\begin{figure}[H]
\centering
\includegraphics[width=0.95\textwidth]{fig/gantt.png}
\caption{Gantt chart of the sample instance}
\label{fig:gantt}
\end{figure}

Figure \ref{fig:gantt} shows a sample schedule of the instance proposed above. In this example, $s_0.T~=~500~\mu~s$ and $s_1.T = 2000~\mu s$ resulting in a hyper period equal to $2000~\mu s$. In~the~vertical axis, each line of the Gantt chart represents the schedule of one network link. The horizontal axis represents discrete time in microseconds. In the schedule, we can see reoccurred stream instances depicted as single colored rectangles. In total, there are eight reoccurred stream instances for $s_0$ (it repeats four times in the hyper period and transmits over two links) and 2 reoccurred stream instances for $s_1$ (it occurs only once in the hyper period and transmits over two links).

\subsection{Scheduling Problem}

The goal is to find a periodic schedule for each link in the network. Our approach is~to~assign a valid value to each reoccurred stream instance marking its start time~$s_{j, l}^{e_k}.\phi \in \N$. The start time assignment is subject to several constraints described below.

\begin{enumerate}

	\item \textbf{Zero Jitter Constraint}

	All reoccurred stream instances of each stream are strictly periodic, meaning there is~zero jitter between period instances.

	\begin{equation} \label{zjc}
		\forall s_j \in S, \forall l \in \{1, ..., HP / s_j.T - 1\}, \forall e_k \in R_j:\\
			s_{j, l}^{e_k}.\phi =  s_{j, l-1}^{e_k}.\phi + s_j.T
	\end{equation}

	\item \textbf{Link Constraint}

	No link can be occupied by more than one stream at the moment.

	\begin{multline} \label{lc}
		\forall e_k \in \E, \forall s_{j, l}^{e_k}, s_{h, i}^{e_k}, (j, l) \neq (h, i):\\
			s_{j, l}^{e_k}.\phi + s_j.p \cdot e_k.w \leq s_{h, i}^{e_k}.\phi~\lor 
			s_{h, i}^{e_k}.\phi + s_h.p \cdot e_k.w \leq s_{j, l}^{e_k}.\phi 
	\end{multline}

	\item \textbf{Precedence Constraint}

	The stream can be processed only after it is fully prepared on the current node.

	\begin{multline} \label{pc}
	\forall s_{j} \in S, \forall h \in \{1,...,len(R_j) - 1\}:\\
		s_{j, 0}^{e_{j}^{h-1}}.\phi + s_j.p \cdot e_{j}^{h-1}.w + e_{j}^{h-1}.l + (e_{j}^{h-1}.to).l
		\leq s_{j, 0}^{e_{j}^{h}}.\phi
	\end{multline}

	\item \textbf{Release \& Deadline Constraint}

	\sloppypar
	The transmission interval for each reoccurred stream instance must fit into the $[release, deadline]$ interval for the given period. Taking into consideration the precedence constraint (\ref{pc}), it is enough to check whether the first stream instance of the stream $s_j$ starts after the release time and the last stream instance of $s_j$ finishes before the deadline. Moreover, due to the zero jitter constraint (\ref{zjc}), it is enough to check it~only in the first period of $s_j$.

	\begin{equation} \label{rc}
		\forall s_j \in S: s_{j, 0}^{R_{j,0}}.\phi \geq s_j.r
	\end{equation} 


	\begin{equation} \label{dc}
		\forall s_j \in S: 
			s_{j, 0}^{R_{j, last}}.\phi + s_j.p \cdot R_{j, last}.w + R_{j, last}.l \leq s_j.\tilde{d}
	\end{equation}

\end{enumerate} 


\subsection{Scheduling Objective}

We are minimizing the sum of the end-to-end latencies of all streams.

\begin{equation} \label{obj}
	\min \displaystyle\sum_{s_j \in S}
	 s_{j, 0}^{R_{j, last}}.\phi + s_j.p \cdot R_{j, last}.w + R_{j, last}.l - s_{j, 0}^{R_{j, 0}}.\phi
\end{equation}

% \doto{neni ekvivalentni, popsat}

% Since the time lags and processing times are treated as constants, we can remove them from the formula

% \begin{equation} 
% 	\min \displaystyle\sum_{\forall s_j \in S}
% 	 s_{j, 0}^{R_{j, last}}.\phi - s_{j, 0}^{R_{j, 0}}
% \end{equation}
