package cz.cvut.ciirc.exceptions;

public class UnspecifiedException extends CustomException {
    public UnspecifiedException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
