package cz.cvut.ciirc.gui;

import cz.cvut.ciirc.gui.components.MyButton;
import lombok.Getter;

import javax.swing.*;
import java.awt.*;

/**
 * Represents bottom GUI panel allowing to control the GUI workflow via buttons.
 */
class BottomPanel extends JPanel {
    private final Dimension space = new Dimension(50, 50);
    @Getter
    private MyButton topologyBtn;
    @Getter
    private MyButton scheduleBtn;
    @Getter
    private MyButton infoBtn;
    @Getter
    private MyButton resetSolverBtn;
    @Getter
    private MyButton resetBtn;
    @Getter
    private MyButton solveBtn;


    BottomPanel() {
        init();
    }

    private void init() {
        setBackground(Color.gray);
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        topologyBtn = new MyButton("Topology");
        scheduleBtn = new MyButton("Schedule");
        infoBtn = new MyButton("Instance info");

        resetSolverBtn = new MyButton("Change solver");
        resetBtn = new MyButton("Reset");
        solveBtn = new MyButton("Solve");

        add(Box.createRigidArea(space));
        add(topologyBtn);
        add(Box.createRigidArea(space));
        add(scheduleBtn);
        add(Box.createRigidArea(space));
        add(infoBtn);

        add(Box.createHorizontalGlue());
        add(solveBtn);
        add(Box.createRigidArea(space));
        add(resetSolverBtn);
        add(Box.createRigidArea(space));
        add(resetBtn);
        add(Box.createRigidArea(space));
        hideAllButtons();
    }

    private void hideAllButtons() {
        topologyBtn.setVisible(false);
        scheduleBtn.setVisible(false);
        infoBtn.setVisible(false);
        resetBtn.setVisible(false);
        solveBtn.setVisible(false);
        resetSolverBtn.setVisible(false);
    }

    private void hideFirstPhaseButtons() {
        solveBtn.setVisible(false);
    }

    private void hideSecondPhaseButtons() {
        topologyBtn.setVisible(false);
        scheduleBtn.setVisible(false);
        infoBtn.setVisible(false);
        resetSolverBtn.setVisible(false);
    }

    void showSecondPhaseButtons() {
        hideFirstPhaseButtons();
        topologyBtn.setVisible(true);
        scheduleBtn.setVisible(true);
        infoBtn.setVisible(true);
        resetBtn.setVisible(true);
        resetSolverBtn.setVisible(true);
    }

    void showFirstPhaseButtons(boolean solveVisible) {
        hideSecondPhaseButtons();
        solveBtn.setVisible(solveVisible);
        resetBtn.setVisible(true);
    }
}
