\section{Program} \label{sec:program}

The algorithms were implemented in Java 8 and designed in a way that aggregates methods common for several solvers and hence allows easy adding of new solver algorithms. General workflow of the implemented program is shown in Figure \ref{fig:workflow}. Proposed algorithms implement the step \texttt{Schedule}; otherwise, the framework is common for all solvers.

\begin{figure}[th]
\centering
\includegraphics[height=0.35\textheight]{fig/workflow.pdf}
\caption{Workflow of the proposed program}
\label{fig:workflow}
\end{figure}

The ILP model was implemented using the Gurobi Optimizer \cite{gurobi} which is a mathematical programming solver known for good performance and easily understandable API. This solver also provides useful outputs about the quality of the solution such as upper bound on distance from the optimum.

The Maven build system is used to build the project. Both input and output data are kept in the Protocol Buffer format, which is a language-neutral tool for serializing structured data. The Protocol Buffer definitions were compiled using \texttt{protoc 3.7} compiler into Java classes. The JavaDoc documentation was autogenerated using Idea IntelliJ. Since the JavaDoc does not support Protocol Buffer format, tool \texttt{protoc-gen-doc} was used for API documentation.

Suitable Java graphical environments for plotting custom graphs are rather scarce. Most of~the available libraries are outdated, not very well documented or contain too advanced features for a simple GUI. In the end, Graph Stream library was used for viewing the network topology, and JFreeChart library was used for the implementation of the Gantt chart. Otherwise, the GUI is based on Java Swing.

Java Lombok plugin (allowing auto-generation of methods like getters, setters, etc.) was used to make the implementation more transparent. To install the program, it should be~sufficient to have Java 8, Gurobi 8.1 (licensed) and Maven installed and build the code using the enclosed \texttt{pom.xml}.


To process the results, we created a Python script \texttt{new\_stats.py} with automated figure plotting. The script assumes folders aggregating instances with the same meta parameters (the folders are indexed in ascending order). Example usage of the script would be \texttt{python new\_stats.py 3000 3360 28 > stats.txt}, which would process the results of 28 different solver methods from folders with indexes from 3000 to 3360 and redirect the text output to~file \texttt{stats.txt}. The figures would be saved to folder \texttt{fig/}. The code is written in Python 3.7, and used libraries are Matplotlib and Pandas.




Further, we show the package structure of the code and describe the content of the packages. Detailed documentation can be found on the enclosed CD in JavaDoc format. There are five runnable classes -- \texttt{Main} for running the scheduler, \texttt{ParallelScheduler} running larger experiments, \texttt{InstanceGenerator} for local generation of a few instances, \texttt{ParallelGenerator} for a parallel generation of experiment datasets and \texttt{GUI} which allows solving one selected instance by one selected method and view its schedule, topology, and setup.
\\
\dirtree{%
.1 cz.cvut.ciirc \dotfill main classes like ProblemInstance and Scheduler.
.2 data\_format \dotfill autogenerated classes from .proto definitions.
.2 data\_format\_definitions \dotfill protocol buffer definitions of API.
.2 exceptions \dotfill custom exceptions.
.2 generator \dotfill instance generator and parameters constants.
.2 gui \dotfill runnable GUI class and all the necessary components. 
.2 helper \dotfill common static methods such as IO handling, etc..
.2 network\_and\_traffic\_model \dotfill classes for inner problem representation.
.2 scheduling \dotfill abstract solver classes, solution class.
.3 baseline\_methods \dotfill ILP, RandomHeuristic, exact CBJ\_BM.
.3 helper\_classes \dotfill classes such as LinkTimeSlots, etc..
.3 multi\_pass \dotfill all multi pass heuristics.
.3 one\_pass \dotfill all one pass heuristics.
}

\subsection{User Manual}

This brief user manual will guide the user throughout the program usage without any need to modify the code. Please note that the following path definitions follow Linux convention, adapt them to your system accordingly. To be able to run the ILP solver, you must have Gurobi installed.
\begin{enumerate}
	\item Create a custom named folder \texttt{CUSTOM\_FOLDER} and place the \texttt{scheduler.jar} into this folder. Create folder \texttt{CUSTOM\_FOLDER/instances/instance\_{dirID}}.

	\item In the created folder, define you protocol buffer input file called \texttt{instance\_{fileID}.pb}. As of May 2019, the protocol buffer can be generated in Java, Python, Objective-C, C++, Dart, Go, Ruby, and C\#. The input file must follow the \texttt{.proto} definitons contained in \texttt{data\_format\_definitions} and described~in~\texttt{API\_documentation.html}.

	\item Run \texttt{java -jar scheduler.jar} from your \texttt{CUSTOM\_FOLDER} to start the program.

	\item Select the instance you would like to solve and press \textit{Open}.

	\begin{figure}[H]
	\includegraphics[width=\textwidth]{fig/gui_file.png}
	\caption{Manual step 1 - Choose the file}
	\label{fig:gui_file}
	\end{figure}

	\item Select the desired solver from the list and press \textit{Solve}. Wait until the instace is solved.

	\begin{figure}[H]
	\includegraphics[width=\textwidth]{fig/gui_solver.png}
	\caption{Manual step 2 - Choose the solver}
	\label{fig:gui_solver}
	\end{figure}

	\item Press \textit{Topology} to view the network topology of the instance.

	\begin{figure}[H]
	\includegraphics[width=\textwidth]{fig/gui_topo.png}
	\caption{Manual step 3 - View the topology}
	\label{fig:gui_topo}
	\end{figure}

	\item Press \textit{Schedule} to view the schedule of the instance. Zoom in the Gantt chart to have a~closer look. If desired it is possible to have labels added to each frame in the Gantt chart as shown in Figure \ref{fig:gantt}. To do so	set \texttt{GUIConstants.showLabels = true} in the~code. 

	\begin{figure}[H]
	\includegraphics[width=\textwidth]{fig/gui_gantt.png}
	\caption{Manual step 4 - View the schedule}
	\label{fig:gui_gantt}
	\end{figure}

	\item Press \textit{Instance info} to view the overview of the input data.

	\begin{figure}[H]
	\includegraphics[width=\textwidth]{fig/gui_instance.png}
	\caption{Manual step 5 - View the instance}
	\label{fig:gui_instance}
	\end{figure}

	\item Press \textit{Reset} to change the instance or press \textit{Change solver} to use different solver for the same instance.

\end{enumerate}
