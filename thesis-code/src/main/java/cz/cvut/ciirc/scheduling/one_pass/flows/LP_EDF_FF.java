package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;

import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Lowest Period Earliest Deadline First Fit Heuristics</h3>
 * <p>
 * One pass heuristic method. Streams are sorted by increasing period and then by deadline.
 * <p>
 * Each stream instance is placed to first available slot.
 */

public class LP_EDF_FF extends Heuristics {
    public LP_EDF_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.LP_EDF_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) {
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        int value;
        for (Stream stream : getProblemInstance().getStreams()) {
            value = stream.getPeriod();
            pQ.add(new ObjectPriorities<>(stream, value, stream.getDeadline()));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamsFirstFit(pQ);
    }
}