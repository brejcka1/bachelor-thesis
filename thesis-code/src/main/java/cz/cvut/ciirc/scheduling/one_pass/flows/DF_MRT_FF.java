package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;

import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Earliest Deadline First Most Required Time First Fit Heuristics</h3>
 * <p>
 * One pass heuristic method. Streams are sorted by earliest deadline and then by decreasing number of total required time on resources.
 * <p>
 * For each stream each stream instance is placed to first available slot.
 */

public class DF_MRT_FF extends Heuristics {
    public DF_MRT_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.DF_MRT_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) {
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        int value;
        for (Stream stream : getProblemInstance().getStreams()) {
            value = stream.getProblemInstance().getHP() - stream.getMinimalRequiredTime() * stream.getNumHPInstances();
            pQ.add(new ObjectPriorities<>(stream, stream.getDeadline() / 100, value));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamsFirstFit(pQ);
    }

}