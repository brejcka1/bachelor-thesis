package cz.cvut.ciirc.network_and_traffic_model;

import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.InputIntegerOverFlowException;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple class representing the stream instance in network model.
 * For more information, see the Problem Statement.
 */
@Getter
public class StreamInstance {
    private final int id;
    private final Link link;
    private int numInHP;
    private List<ReoccurredStreamInstance> repStreamInstances;

    private Stream stream;
    private StreamInstance predecessor;

    public StreamInstance(int id, Link link, Stream stream, StreamInstance predecessor) throws InputIntegerOverFlowException {
        this.id = id;
        this.link = link;
        this.stream = stream;
        this.predecessor = predecessor;
        this.repStreamInstances = new ArrayList<>();

        if (this.stream.getProblemInstance().getHP() % stream.getPeriod() != 0) {
            throw new InputIntegerOverFlowException("HP not multiple of stream period: HP="
                    + this.stream.getProblemInstance().getHP() + ", T =" + stream.getPeriod());
        }

        this.numInHP = this.stream.getProblemInstance().getHP() / this.stream.getPeriod();
    }

    /**
     * Constructor for copying.
     *
     * @param other Stream instance to be copied
     */
    public StreamInstance(StreamInstance other) {
        this.id = other.id;
        this.link = other.link;
        this.numInHP = other.numInHP;
        this.stream = other.stream;
        this.predecessor = other.predecessor;
        this.repStreamInstances = other.repStreamInstances;
    }

    /**
     * Creates list o reoccurred stream instances for given stream instance.
     */
    public void createReoccurredStreamInstances() {
        int duration;
        for (int hpIndex = 0; hpIndex < numInHP; hpIndex++) {
            duration = (int) Math.ceil(stream.getDuration() * link.getWeight());
            ReoccurredStreamInstance fi = new ReoccurredStreamInstance(hpIndex, duration, this);
            repStreamInstances.add(fi);
        }
    }

    /**
     * Returns reoccurred stream instance in given period.
     *
     * @param hpIndex period in which the re. stream instance occurs
     * @return reoccurred stream instance in given period
     * @throws ArrayNotInitializedException In case the period index exceeded total number od periods available
     */
    public ReoccurredStreamInstance getReSIInHP(int hpIndex) throws ArrayNotInitializedException {
        if (hpIndex >= repStreamInstances.size())
            throw new ArrayNotInitializedException("Index exceeded array size in getReSIInHP()");
        return repStreamInstances.get(hpIndex);
    }

    @Override
    public String toString() {
        return "StreamInstance{" +
                "id=" + id +
                ", link=" + link.getFromToString() +
                ", numInHP=" + numInHP +
                ", stream=" + stream.getName() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof StreamInstance)) return false;
        StreamInstance item = (StreamInstance) o;
        return id == item.id;
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Method not implemented");
    }
}





















