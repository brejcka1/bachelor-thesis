package cz.cvut.ciirc.exceptions;

public class ResultsIOException extends CustomException {

    public ResultsIOException(String s) {
        super(s);
    }

    public ResultsIOException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
