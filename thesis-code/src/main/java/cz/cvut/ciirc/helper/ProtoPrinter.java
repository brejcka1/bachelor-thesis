package cz.cvut.ciirc.helper;

import cz.cvut.ciirc.data_format.InputFile;
import cz.cvut.ciirc.data_format.OutputFile;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class useful for quick debugging of proto files - converts them to string and prints to console.
 */
public class ProtoPrinter {
    public static void main(String[] args) {
        int fileID = 0;
        int dirID = 666;

        Path currentPath = Paths.get(System.getProperty("user.dir")).resolveSibling("instances").resolve("instance_" + dirID).resolve("instance_" + fileID + ".pb");
        Path res_1_path = Paths.get(System.getProperty("user.dir")).resolveSibling("results").resolve("result_" + dirID).resolve("result_" + fileID + "_CBJ_BM.pb");
        Path res_2_path = Paths.get(System.getProperty("user.dir")).resolveSibling("results").resolve("result_" + dirID).resolve("result_" + fileID + "_CBJ_BM_D.pb");
        try {
            FileInputStream fileInputStream = new FileInputStream(currentPath.toFile());
            InputFile.InputFileProto inputFileProto = InputFile.InputFileProto.parseFrom(fileInputStream);
            System.out.println(inputFileProto.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("============= RESULT 1 ======================");
        try {
            FileInputStream fileInputStream = new FileInputStream(res_1_path.toFile());
            OutputFile.OutputFileProto inputFileProto = OutputFile.OutputFileProto.parseFrom(fileInputStream);
            System.out.println(inputFileProto.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("============= RESULT 2 ======================");
        try {
            FileInputStream fileInputStream = new FileInputStream(res_2_path.toFile());
            OutputFile.OutputFileProto inputFileProto = OutputFile.OutputFileProto.parseFrom(fileInputStream);
            System.out.println(inputFileProto.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
