package cz.cvut.ciirc.scheduling.one_pass;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.exceptions.UtilizationException;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;
import cz.cvut.ciirc.scheduling.helper_classes.StreamInstanceInfo;

import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * <h3>Resource Equivalent Duration Stream Instances Earliest Deadline First Fit Heuristic</h3>
 * <p>
 * One pass heuristic method. Stream instances are sorted by decreasing product of duration and weighted resource requirements.
 * <p>
 * Each stream instance is placed to first available slot.
 */
public class RED_SI_EDF_FF extends Heuristics {
    public RED_SI_EDF_FF(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.RED_SI_EDF_FF);
    }

    @Override
    protected void updatePQ(PriorityQueue<ObjectPriorities> pQ, Object next) {
    }

    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws UtilizationException, ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        List<StreamInstanceInfo> siiList = initializeStreamInfoList();
        double[] utilization = getProblemInstance().getUtilization();
        int value;
        int utilCoef;

        for (StreamInstanceInfo curSii : siiList) {
            utilCoef = (int) Math.ceil(utilization[curSii.getCurSI().getLink().getId()] * 10);
            value = 10 * getProblemInstance().getHP() - (curSii.getCurSI().getReSIInHP(0).getDurationWithTimeLag() * utilCoef);
            pQ.add(new ObjectPriorities<>(curSii, value, curSii.getCurSI().getStream().getDeadline()));
        }
        return pQ;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        return scheduleStreamInstancesFirstFit(pQ);
    }
}