package cz.cvut.ciirc.gui.middle_panels;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.SchedulerConstants;
import cz.cvut.ciirc.gui.GUIConstants;
import cz.cvut.ciirc.gui.components.MyRadioButton;
import lombok.Getter;
import org.apache.commons.lang3.ArrayUtils;

import javax.swing.*;
import java.awt.*;
import java.util.Enumeration;

import static cz.cvut.ciirc.gui.GUIConstants.mediumBold;
import static cz.cvut.ciirc.gui.GUIConstants.small;

/**
 * Represents one of the first phase screens where the solver method is chosen.
 */
public class SolverPanel extends JPanel {
    @Getter
    private ButtonGroup teamGroup;

    public SolverPanel() {
        init();
    }

    private void init() {
        setLayout(new BorderLayout());
        setBorder(GUIConstants.border);

        // initialize solver chooser
        JPanel teamSelectorPanel = new JPanel(new GridLayout(0, 1, 5, 10));
        teamGroup = new ButtonGroup();
        addAlgorithmsToPanel(teamSelectorPanel, teamGroup);

        JScrollPane solverChooser = new JScrollPane(teamSelectorPanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        // initialize label
        JLabel label = new JLabel("<HTML>Please choose a solver method", SwingConstants.CENTER);
        label.setFont(mediumBold);
        label.setForeground(GUIConstants.warning);

        // add to framework
        add(solverChooser, BorderLayout.CENTER);
        add(label, BorderLayout.PAGE_START);
    }

    private void addAlgorithmsToPanel(JPanel teamSelectorPanel, ButtonGroup teamGroup) {
        AlgorithmType[] allMethods = ArrayUtils.addAll(SchedulerConstants.solverMethodsGUI);

        for (AlgorithmType alg : allMethods) {
            MyRadioButton radioButton = new MyRadioButton(alg);
            radioButton.setFont(small);
            teamGroup.add(radioButton);
            teamSelectorPanel.add(radioButton);
        }
    }

    public AlgorithmType getSelected() {
        Enumeration<AbstractButton> allRadioButtons = teamGroup.getElements();
        while (allRadioButtons.hasMoreElements()) {
            MyRadioButton curBtn = (MyRadioButton) allRadioButtons.nextElement();
            if (curBtn.isSelected()) {
                return curBtn.getAlgorithmType();
            }
        }
        return null;
    }
}
