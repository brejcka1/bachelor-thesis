package cz.cvut.ciirc.exceptions;

public class InputFileNotFoundException extends CustomException {

    public InputFileNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
