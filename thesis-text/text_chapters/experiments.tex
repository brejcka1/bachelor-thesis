\section{Experiments} \label{sec:experiments}

To evaluate the proposed algorithms we conducted experiments on artificially generated data. The generation methodology is described in the following section. Moreover, we compare the performance of selected methods (with a high enough percentage of scheduled instances).

\subsection{Experiments Setup}

We evaluated the proposed algorithms on randomly generated instances suitable for highly critical Ethernet communication. We generated schedulable instances of various sizes and topologies with the aim to have instances with sequentially growing average link utilization allowing us to test both easily and hardly schedulable instances.

In this chapter we will use additional terminology for the network nodes -- \textit{end systems} are network nodes that are connected to the rest of the network by one duplex link only (e.g., leaf of a tree graph), \textit{switches} are any other network nodes that are not end systems (i.e., node acting as an intermediate for other nodes). Time units used in experiments are microseconds.

The instance generation can be simplified into two main steps -- \textit{topology generation} of~all the network nodes and connections between them and \textit{communication generation} of~all streams that are sent between the end systems.

The \textit{topology generation} was inspired by Craciunas et al. \cite{crac2016} who designed several industrial-sized topologies for time-triggered scheduling in distributed systems. We run the experiments on three topology sizes, SMALL, MEDIUM, and LARGE, ranging from a couple of switches to several tens of switches (see Table \ref{tab:sizes}). The topologies are of three different types -- TREE, RING, and LINE. Example middle sized topologies of each type are depicted by~our GUI in Figures \ref{fig:tree}, \ref{fig:ring} and \ref{fig:line} respectively. In total, we have nine different topologies to~test~on.

\begin{figure}[H]
\centering
\begin{subfigure}{.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{fig/tree.png}
    \caption{Middle sized TREE topology}
    \label{fig:tree}
\end{subfigure}%
\hfill
\begin{subfigure}{.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{fig/ring.png}
    \caption{Middle sized RING topology}
    \label{fig:ring}
\end{subfigure}
\caption{Sample middle sized TREE and RING topology}
\end{figure}

Parameters of nodes and links in the network are chosen to suit the Ethernet communication problem and are similar to \cite{crac2016}. Link weight $e_k.w$ is set to 1 for links between two switches and 10 for links between the switch and end system, representing physical Ethernet link of speed 1 Gbit/sec and 100 Mbit/sec respectively. Similar as in \cite{hanzalek_profinet}, the link time lag $e_k.l$ is set to 1 $\mu s$ for all links, representing the propagation delay which is equal to~${wire~length}/{speed~of~light}$ and the node time lag is set to 10 $\mu s$ for all nodes.

\begin{figure}[th]
\centering
\includegraphics[width=\textwidth]{fig/line.png}
\caption{Middle sized LINE topology}
\label{fig:line}
\end{figure}


The \textit{communication generation} depends on the topology -- Streams are generated to be~sent between the end systems. In RING and TREE topology, a stream can be sent between any two end systems. In LINE topology the communication takes place only between one specified end system (control unit) and the other end systems.

The streams have a different period $s_j.T$ which is uniformly chosen from one of the three predefined period sets (for one generated instance, one period set is used) with values ranging from 1 ms to 16 ms and hyper period not larger than 16 ms. All period sets are shown in~Table~\ref{tab:periods}.


\begin{table}[th]
\centering
 \begin{tabular}{|c c c |} 
 \hline
 \textbf{periodSet} & \textbf{periods} ($\mu$s) & \textbf{HP} ($\mu$s) \\
 \hline\hline
 $P_1$ & $\{1000, 2500, 5000, 10000\}$ & $10000$ \\ 
 $P_2$ & $\{5000, 7500\}$ & $15000$ \\ 
 $P_3$ & $\{2000, 4000, 8000, 16000\}$ & $16000$ \\ 
 \hline
\end{tabular}
\caption{Period sets}
\label{tab:periods}
\end{table}

To model communication of different complexity, the total number of reoccurred stream instances $rsi$ was adjusted.
\begin{equation}
\label{rsi}
rsi = \displaystyle\sum_{s_j \in S} \frac{HP}{s_j.T} \cdot |R_j|
\end{equation}
We set upper and lower bound on $rsi$ for each topology type and size as shown in Table~\ref{tab:sizes} and iterate from lower to upper bound with a step size equal to the interval size divided by~20~resulting in 20 different communication complexities for each topology. Upper bound $ub$ on $rsi$ was set experimentally for each combination of topology size and topology type as~the maximum number of frames for which the generator yielded a schedulable instance in~a~reasonable time (in order of hundreds of seconds). Lower bound $lb$ was set as $lb = \frac{ub}{10}$ for each setting.

\begin{table}[th]
    \centering
    \begin{tabular}{|c c | c c | c c|}
    \hline
        \textbf{topMode} & \textbf{topSize} &  \textbf{numSwitches} & \textbf{numEndSystems} & \textbf{lb on rsi} & \textbf{ub on rsi}\\
        \hline \hline
        TREE & SMALL & 1 & 6 & 60 & 600\\
        TREE & MEDIUM & 7 & 36 & 1600 & 16000\\
        TREE & LARGE & 21 & 64 & 2000 & 20000\\
        RING & SMALL & 2 & 6 & 200 & 2000\\
        RING & MEDIUM & 6 & 36 & 1600 & 16000\\
        RING & LARGE & 14 & 70 & 2000 & 20000\\
        LINE & SMALL & 1 & 4 & 160 & 1600\\
        LINE & MEDIUM & 5 & 31 & 800 & 8000\\
        LINE & LARGE & 13 & 66 & 1800 & 18000\\
        \hline
    \end{tabular}
    \caption{Parameters of different topologies}
    \label{tab:sizes}
\end{table}

The stream duration is set randomly to correspond to an Ethernet packet of size \mbox{125--1500}~bytes transmitted through the 1Gbit/s network. Release time $s_j.r$ and deadline $s_j.\tilde{d}$ are randomly set so that the interval $[s_j.r, s_j.\tilde{d}]$ covers 15--40 \% of the period $s_j.T$ and the interval is large enough to cover the sum of transmission durations over all reoccurred stream instances on the path of the stream.

Summarized we have four different parameters defining one instance folder
\texttt{\string{topologySize, topologyMode, periodSet, rsi\string}} resulting in $3 \cdot 3 \cdot 3 \cdot 20 = 540$ folders in total. Each folder consists of 100 instances with the same quadruple of these parameters -- the instances differ in the generated communication which is partially random. The main generation cycle is shown in Algorithm \ref{generatorPseudo}.

\begin{algorithm}[th]
\caption{Benchmark generator}\label{generatorPseudo}
\begin{algorithmic}[1]
\Procedure{GenerateBenchmarks}{}
\ForAll{$topologySizes$}{
    \ForAll{$topologyModes$}{
        \ForAll{$periodSets$}{
            \State$lb, ub, step \gets get(topologyMode, topologySize)$
            \For{$rsi\gets lb, ub, step$} \label{lbub}
                \For{$i\gets 1, numInstances$}
                    \State $topology\gets$ \Call{GenTopology}{$topologyMode$, $topologySize$}
                    \State $streams\gets$ \Call{GenStreams}{$topology$, $periodSet$, $rsi$}
                    \If{!$valid(streams)$}
                        \State $discard(streams, topology)$
                    \EndIf
                \EndFor
            \EndFor
        } \EndFor
    } \EndFor
}\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

The pseudocode for communication generation is described in Algorithm \ref{streamsGen}. The streams are generated using a first fit heuristic approach described in \ref{ff}. The generator keeps an~online schedule of so far generated streams and updates it each time a new stream is~generated. The streams are generated until the desired $rsi$ bound is reached or the instance generation is not possible for given parameters (and generated random numbers).


\begin{algorithm}[th]
\caption{Communication generator}\label{streamsGen}
\begin{algorithmic}[1]
\Procedure{GenStreams}{$topology, periodSet, rsi$}
    \State $rsiID \gets 0$
    \State $streams \gets \{\}$
    \State $HP \gets lcm(periodSet)$
    \State $schedule \gets$ initialize empty schedule for all links
    \State $fromToPQ \gets$ initialize priority structure \label{fromToInit}
    \While{$rsiID < rsi$}
        \State $curP \gets$ select period based on $rsiID$ \label{curP}
        \If{$fromToPQ.peek().period > curP$}
        \State \Return null \label{terminate}
        \EndIf
        \State $curPeekPQ \gets fromToPQ.pop()$
        \State $path \gets$ find a path from $curPeekPQ.org$ to $curPeekPQ.trg$
        \State $success \gets$ \Call{TryToPlace}{$curP$, $path$, $schedule$, $streams$, $topology$}
        \If{$success$}
            \State $rsiID \gets rsiID + (HP/curP) * length(path)$
            \State $fromToPQ.insert(curPeekPQ, curP, rsiID)$
        \Else
            \State $nextP \gets$ select next period from the periodSet
            \State $fromToPQ.insert(curPeekPQ, nextP, rsiID)$
        \EndIf
    \EndWhile
    \State \Return streams
\EndProcedure
\end{algorithmic}
\end{algorithm}

A priority queue \texttt{fromToPQ} (Algorithm~\ref{streamsGen}, line~\ref{fromToInit}) is used for choosing origin and target nodes of the streams. 
Items in the queue consist of quadruple {origin, destination, current period and the last usage}. Before the communication generation starts, the \texttt{fromToPQ} is initialized with all possible combinations of end systems, the lowest period from the period set and negative random number (for the last usage). The top of the priority queue is an element with the lowest current period and in the case of a draw the element with the lowest last usage. The lowest current period represents the lowest period for which the stream of given origin and target can theoretically be placed to the schedule. The last usage is a timestamp (represented by a current number of reoccurred stream instances in the network) marking the last usage of the given origin -- target combination. Initializing the last usage to a random number ensures that the priority queue is different each time it is initialized. The \texttt{fromToPQ} structure guarantees that the communication generation stops in a finite time (Algorithm~\ref{streamsGen}, line~\ref{terminate}) and that the streams are as uniformly distributed as possible between different combinations of origin and target end systems.


Periods are distributed uniformly with respect to the number of reoccurred stream instances \texttt{rsi}. The period set is sorted from the lowest to the largest value. The period \texttt{curP} for the currently generated stream is selected based on the progress of already placed reoccurred stream instances in the schedule which is corresponding to the ratio $rsiID/rsi$.
Please note that since $rsi$ is calculated as in Equation (\ref{rsi}), the actual number of added reoccurred stream instances of the period $T \in P_i$ is not equal to $rsi / |P_i|$ but belongs to the interval $$
\left(\frac{rsi}{|P_i|} - \frac{HP}{T} \cdot d, \frac{rsi}{|P_i|} + \frac{HP}{T} \cdot d\right)
$$ where $d$ is the longest path between any two end systems in the network.

In case there is no combination of origin and target nodes that would allow us to place the stream with period \texttt{curP}, the instance is discarded. The stream with parameters origin, target, period is valid only if it is possible to place it to the current schedule using the first fit approach \ref{ff} so that it meets all constraints (\ref{lc})--(\ref{dc}) -- in pseudocode procedure \texttt{TryToPlace} (Algorithm \ref{tryToPlace}). In case the stream placement is possible, the online schedule is updated. Otherwise, the stream payload is being iteratively decreased. After reaching the lower bound for the stream transmission duration, the current stream is discarded and different combination of origin and target is chosen from the priority structure.

\begin{algorithm}[th]
\caption{Placing stream in the schedule} \label{tryToPlace}
\begin{algorithmic}[1]
\Procedure{TryToPlace}{$curP, path, schedule, streams, topology$}
    \State $duration \gets randomStreamDuration(1, 12)$
    \While{$duration > 0$}
        \State $schedule \gets placeFirstFit(curP, path, schedule, streams, topology, duration)$
        \If{$valid(schedule)$}
        \State \Return true
        \Else { $duration \gets{duration - 1}$}
        \EndIf
    \EndWhile
    \State \Return false
\EndProcedure
\end{algorithmic}
\end{algorithm}

\subsection{Results}

To test the proposed algorithms on generated instances, we implemented all of them in~Java 8, and for the ILP model we used Gurobi solver version 8.1. To ensure the same environment for both ILP and the other algorithms, the number of threads that Gurobi is~allowed to use was set to one. The experiments were run on a system with 4x Intel\textregistered\ Xeon\textregistered\ CPU E5-2690 v4 @ 2.60GHz with 14 cores (56 cores in total) and in total 251GB of RAM. We~set the time limit to 60 seconds per one solver. Note that the problem instance initialization (data loading, etc.) is not included in this time limit and is done in order of seconds. The~problem instances were run in parallel on 56 threads. The~parallelization was done in a way that allows the solvers to run without any common resources, only the call to create Gurobi environment is locked for synchronization safety reasons. In general, compared to single thread execution, the delay of one solver resulting from the parallelization is negligible.

\subsubsection{Structure of Generated Instances}

Due to the large number of input parameters, the difficulty of each instance is hard to estimate. However, we can still point out some trends based on the results of the first experiment. Each instance is influenced by several factors.

Firstly, we point out the topology type and size which influences the importance of bottleneck link. We call the link a bottleneck when it gathers significantly more communication traffic (i.e., the link has high utilization) than most of the links. Such bottleneck link then determines the throughput of the whole network. For LINE topology, the bottleneck is easy to determine. Since all communication includes the control unit, the bottleneck link is the one connecting the control unit to the rest of the network. This topology is extreme in a way that all streams in the network cross the bottleneck link. For TREE and RING topologies, the bottleneck link is not that clear and depends on the distribution of streams among end systems. In Figure~\ref{fig:util_scheduled}, we can see the average and maximal link utilization of all instances. We can see that the average and maximal utilization do not have the same distribution. This is caused by the fact that some of the links transfer more traffic than the other links. For this reason, we will need to analyze each topology type and size separately because the number of $rsi$ in the network does not necessarily correspond to the instance difficulty. In Appendix~\ref{app:util} we can see the utilization for each topology type and size separately.

\begin{figure}[th]
    \centering
    \includegraphics[width=.35\textheight]{fig/util/instances_utilization.eps}
    \caption{Link utilization of all topologies}
    \label{fig:util_scheduled}
\end{figure}


Secondly, the period set can be described by the number of periods, the average value of the period, the hyper period and if the periods are harmonic or not. All of these factors influence the instance difficulty. Table \ref{tab:period_sets} shows the dependence of the number of scheduled instances on the period set. We say that the instance is scheduled if there was at least one method that found a solution. The most successful was the harmonic set $P_3$ with 99.37~\% of~scheduled instances. On the other hand, set $P_1$ which is not harmonic and has four different periods, had only 71.27~\% of scheduled instances.


\begin{table}[th]
\centering
 \begin{tabular}{|c c c c c|} 
 \hline
 \textbf{periodSet} & \textbf{periods} ($\mu$s) & \textbf{HP} ($\mu$s) & \textbf{scheduled} & \textbf{scheduled ($\%$)} \\
 \hline\hline
 $P_1$ & $\{1000, 2500, 5000, 10000\}$ & $10000$ & $12829$ & $71.27$\\ 
 $P_2$ & $\{5000, 7500\}$ & $15000$ & $16012$ & $88.96$ \\ 
 $P_3$ & $\{2000, 4000, 8000, 16000\}$ & $16000$ & $17886$ & $99.37$ \\ 
 \hline
\end{tabular}
\caption{Number of scheduled instances based on period set}
\label{tab:period_sets}
\end{table}


\subsubsection{One Pass Heuristics}

The first experiment was performed on one pass heuristics and the baseline methods. In Table \ref{tab:onePassRes} we can see the results. Each heuristic is called by its criteria -- e.g., \textit{RED\_EDF} is~a~heuristic where the first criterion is the Resource Equivalent Duration and the second criterion is the Earliest Deadline First.

\begin{table}[th]
\centering
\begin{tabular}{| l  r  r  r |}
\hline
\textbf{method}    & \textbf{scheduled} & \textbf{avg time [s]}  & \textbf{best obj}  \\
\hline\hline
EDF\_MRT  & 40345 & 0.03  & 9836  \\
EDF\_RED  & 40343 & 0.06  & 9706  \\
DF\_RED   & 40267 & 0.06  & 3556  \\
DF\_MRT   & 40199 & 0.03  & 3742  \\
DF\_EST   & 20918 & 0.01  & 807   \\
DF\_LST   & 20871 & 0.01  & 199   \\
LST\_EDF  & 20804 & 0.01  & 190   \\
EDF\_EST  & 20796 & 0.01  & 324   \\
EDF\_LST  & 20788 & 0.01  & 301   \\
EDF\_MTS  & 20785 & 0.01  & 315   \\
DF\_MTS   & 20385 & 0.01  & 413   \\
ILP       & 18885 & 48.53 & 18769 \\
MTS\_EDF  & 12811 & 0.01  & 242   \\
MRT\_EDF  & 9102  & 0.00   & 7     \\
EST\_EDF  & 7631  & 0.01  & 133   \\
RED\_EDF  & 3657  & 0.03  & 1     \\
RANDOM    & 3352  & 0.00   & 197   \\
DF\_MSLK  & 136   & 0.00   & 0     \\
EDF\_MSLK & 117   & 0.00   & 1     \\
MSLK\_EDF & 96    & 0.00   & 0    \\
\hline
\end{tabular}
\caption{Performance of One Pass Heuristics and Baseline methods}
\label{tab:onePassRes}
\end{table}



Three performance measures were taken -- the number of scheduled instances, the average running time and the number of best objective values. As a reminder, we must mention that all of the instances were generated in a way that ensures they are schedulable (i.e., some solution exists for each one of them).

In total, we generated 54000 instances, which means that the most successful method \textit{EDF\_MRT} solved 74.7~\% of the instances. The other similarly successful methods (\mbox{74.4--74.7~\%}) were alternations of the aforementioned method. The most successful method that has a complement criterion as the first priority value was \textit{LST\_EDF} with 38.5~\% of~scheduled instances. The only criterion that seems useless for our problem is \textit{MSLK}. This may be~caused by the fact that it diminishes the release time and the deadline of the stream. Our baseline methods ILP and Random Heuristic scheduled fewer instances than most of the heuristic methods. The success rate of \textit{ILP} was 34.9~\%, and the success rate of \textit{RANDOM} was 6.2~\%.

The number of best objective values is calculated as a sum of the instances where the method obtained the best objective value among others. If more methods obtained the best objective value for some instance, all of these methods get a score point. As expected, the \textit{ILP} did very well in this performance measure -- for 34~\% of instances, it found a solution with the lowest objective value. As we mentioned earlier, the \textit{ILP} is a complete and optimal method. However, we can see that there is a 0.9~\% difference between the scheduled and best objective instances. This is caused by the Gurobi solver, which may return a sub-optimal solution in case the time limit is reached. The performance of the heuristics with respect to~the best objective metric was similar as for the number of scheduled instances -- none of the heuristics was exceptional in this performance measure.

The average running time for all heuristics was in the order of hundredth of a second. The~average running time for the \textit{ILP} was 48.53 seconds. However, it is important to point out that as opposed to heuristics, the \textit{ILP} did not return the first feasible solution but instead continued in searching for the optimal one. This is partially causing the larger running time of the \textit{ILP}. However, it is clear that the heuristics run much faster.


\subsubsection{Multiple Pass Heuristics} \label{sec:resMultiple}

We based our multiple pass heuristics on the best performing one pass heuristic \textit{DF\_MRT}. Table \ref{tab:multiPassRes} compares the original \textit{CBJ\_BM\_NOH} implementation with the enhanced implementation \textit{CBJ\_BM} and the implementations skipping some domain values \textit{CBJ\_BM\_D}, \textit{CBJ\_BM\_P} and \textit{CBJ\_BM\_ID}. 

\begin{table}[th]
\centering
\begin{tabular}{| l  r  r  r |}
\hline
\textbf{method}    & \textbf{scheduled} & \textbf{avg time [s]}  & \textbf{best obj}  \\
\hline\hline
CBJ\_BM\_D   & 44981 & 15.32 & 17026 \\
CBJ\_BM      & 44931 & 15.38 & 9340  \\
CBJ\_BM\_P   & 41780 & 14.35 & 507   \\
CBJ\_BM\_ID  & 40731 & 15.35 & 456   \\
ILP          & 18885 & 48.53 & 18738 \\
CBJ\_BM\_NOH & 4637  & 55.01 & 466   \\
RND          & 3352  & 0.0   & 12   \\
\hline
\end{tabular}
\caption{Performance of Multiple Pass Heuristics and Baseline methods}
\label{tab:multiPassRes}
\end{table}

The most scheduled instances were obtained by \textit{CBJ\_BM\_D} heuristics. It found a solution for 83.3~\% instances. The complete version \textit{CBJ\_BM} obtained comparable results with 83.2~\% of scheduled instances. The reason why these two methods behave similarly is that the step size for \textit{CBJ\_BM\_D} is one for stream instances passing between switches. Hence, these two methods act the same on stream instances between switches. The step size for stream instances passing between end systems and switches is between one and twelve -- the step size is affected by the stream transmission duration and the link speed. The other two heuristics do not perform significantly worse with 75.4--77.4~\% of scheduled instances. However, they do not show better results than the \textit{CBJ\_BM\_D} heuristic for any combination of instance parameters. The original implementation without the heuristical enhancement found a solution for 8.58~\% instances.

The best objective values were (apart from ILP) obtained by \textit{CBJ\_BM\_D} heuristics. If~we~remove all methods except for \textit{CBJ\_BM\_D} and \textit{CBJ\_BM} from the statistics, we obtain best objective values 26313 (48.7~\%) and 22615 (41.9~\%) respectively. This is significantly better than the difference in the number of scheduled instances. The rationale may be~that larger step size sometimes skips the first available time slot for the given stream instance. The larger the transmission duration of the stream instance is, the more available time slots it may skip. This allows shorter (in transmission duration) stream instances to fit into the empty gap. Hence, the end-to-end latency of the shorter stream instances may be lower. Nevertheless, this hypothesis would need a deeper exploration of the results to confirm it.

The time performance for all heuristic \textit{CBJ\_BM} based methods is similar (14--16 s). The~\textit{CBJ\_BM\_NOH} method timed out on most of the instances, and it is also reflected on the average solving time 55 s.

\subsection{Discussion}

To have a clear visual interpretation of the results, we have chosen to plot only results for the best performing one pass heuristic \textit{EDF\_MRT}, the best performing multiple pass heuristic \textit{CBJ\_BM\_D} and the \textit{ILP} method. However, it is possible to run the enclosed plotting script \texttt{python new\_stats.py 7000 7540 25 > results.txt} on any other combination of methods if needed. The figures showing the performance measures of the selected methods for each topology type and size can be found in Appendices \ref{app:scheduled} -- \ref{app:best}. 

Another interesting performance measure can be found in Appendix \ref{app:scheduled_util} where we show the dependence of success rate (percentage of scheduled instances) on the average and maximal link utilization of the instance. We can see that the maximal link utilization is a slightly better indicator of instance difficulty.

Generally speaking, ILP performs very well on small instances with low maximal link utilization (up to 40~\%). Since it also finds the optimal solution, it does not make much sense to compete with ILP on such instances. On the other hand, for larger or more utilized networks it is beneficiary to use the heuristic methods. We can see that for large instances, the \textit{CBJ\_BM\_D} success rate 85.2~\% was significantly better than the ILP success rate of 12.0~\%.

Further, we compare objective values of \textit{ILP} and \textit{CBJ\_BM\_D} on instances where \textit{ILP} found an optimal solution. The average objective value of the solution found by \textit{CBJ\_BM\_D} was 101.7~\% higher than of the solution found by \textit{ILP}. We could improve this measure by~introducing a version of \textit{CBJ\_BM\_D} that is optimal and it will be one of the directions for the future work.

If we compare the results of the best multiple pass (83.3~\% success rate) and the best one pass heuristics (74.7~\% success rate), we find out that the introduction of backtracking allows us to have 8.6~\% better difference in the percentage of scheduled instances. In Table \ref{tab:onePassRes} we see that the run time of one pass heuristics is negligible as opposed both to \textit{ILP} and \textit{CBJ\_BM} based methods. Hence, we could run all of the implementented one pass heuristics and then choose the one that yielded the best solution for the given instance. In such case, the success rate of the combined one pass heuristics would be 78.7~\%.
