package cz.cvut.ciirc.exceptions;

public class GurobiException extends CustomException {

    public GurobiException(String s) {
        super(s);
    }

    public GurobiException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
