package cz.cvut.ciirc.scheduling.helper_classes;

import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Link;
import lombok.Getter;

import java.util.LinkedList;

/**
 * Represents free and occupied time windows for given link.
 * <p>
 * Free time slots = union over all i [freeSlotsStarts[i], freeSlotsEnds[i])
 */

public class LinkTimeSlots {
    private LinkedList<Integer> freeSlotsStarts;
    private LinkedList<Integer> freeSlotsEnds;
    @Getter
    private Link link;

    public LinkTimeSlots(LinkedList<Integer> freeSlotsStarts, LinkedList<Integer> freeSlotsEnds, Link link) {
        this.freeSlotsStarts = freeSlotsStarts;
        this.freeSlotsEnds = freeSlotsEnds;
        this.link = link;
    }


    /**
     * Return first available offset in the first period for given stream instance.
     * It must fit the schedule in all periods (with zero jitter).
     *
     * @param duration      Duration of the transmission
     * @param period        Period of the transmission
     * @param numInstances  Number of repetitions in hyper period
     * @param earliestStart Earliest possible start of the stream
     * @param deadline      Deadline of the stream
     * @param release       Release time of the stream
     * @return The first available offset in the first period satisfying the constraints
     */
    public int getFirstAllFitSlotID(int duration, int period, int numInstances, int earliestStart, int deadline, int release) {
        int slotStart, numOffsets, upperBound;
        for (int i = 0; i < freeSlotsStarts.size(); i++) {
            slotStart = Math.max(freeSlotsStarts.get(i), release);
            slotStart = Math.max(slotStart, earliestStart);
            upperBound = Math.min(freeSlotsEnds.get(i), deadline);
            numOffsets = upperBound - slotStart - duration;

            for (int slotOffset = 0; slotOffset <= numOffsets; slotOffset++) {
                if (isValidInAllPeriods(duration, period, numInstances, slotStart + slotOffset)) {
                    return slotStart + slotOffset;
                }
            }
        }
        return -1;
    }

    /**
     * Checks if the proposed time is valid in all periods (considering zero jitter)
     *
     * @param duration      Duration of the transmission
     * @param period        Period of the transmission
     * @param numInHP       Number of instances in hyper period
     * @param proposedStart Proposed offset
     * @return True in cas the proposed time is correct
     */
    private boolean isValidInAllPeriods(int duration, int period, int numInHP, int proposedStart) {
        boolean allOK = true;
        int start = proposedStart + period;
        for (int hpID = 1; hpID < numInHP; hpID++) {
            allOK = false;
            for (int j = 0; j < freeSlotsStarts.size(); j++) {
                if (start >= freeSlotsStarts.get(j) && start + duration <= freeSlotsEnds.get(j)) {
                    allOK = true;
                    break;
                }
            }
            if (!allOK) break;
            start += period;
        }
        return allOK;
    }

    /**
     * Removes occupied slot from the free slots list.
     *
     * @param startTime Start time of the occupied slot
     * @param duration  Duration of the occupied slot
     * @throws ImplementationException in case of invalid slot assignment
     */
    public void removeFreeTimeSlot(int startTime, int duration) throws ImplementationException {
        int end = startTime + duration;
        boolean placed = false;
        int originalEnd;

        for (int i = 0; i < freeSlotsStarts.size(); i++) {
            if (startTime >= freeSlotsStarts.get(i) && end <= freeSlotsEnds.get(i)) {

                if (startTime == freeSlotsStarts.get(i)) {
                    freeSlotsStarts.set(i, startTime + duration);
                    placed = true;
                } else if (end == freeSlotsEnds.get(i)) {
                    freeSlotsEnds.set(i, end - duration);
                    placed = true;
                }

                if (placed) {
                    if (freeSlotsStarts.get(i).equals(freeSlotsEnds.get(i))) {
                        freeSlotsStarts.remove(i);
                        freeSlotsEnds.remove(i);
                    }
                } else {
                    placed = true;
                    originalEnd = freeSlotsEnds.get(i);
                    freeSlotsEnds.set(i, startTime);
                    freeSlotsStarts.add(i + 1, freeSlotsEnds.get(i) + duration);
                    freeSlotsEnds.add(i + 1, originalEnd);
                }
                break;
            }
        }
        if (!placed) {
            throw new ImplementationException("IMPLEMENTATION ERROR: Tried to remove schedule slot on link: " + link +
                    "\nbut the slot is already occupied!");
        }
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(link.toString() + ": [");
        if (freeSlotsStarts.size() != freeSlotsEnds.size()) {
            System.err.println("Size of freeSlots fields not same");
        }
        for (int i = 0; i < freeSlotsEnds.size(); i++) {
            out.append(freeSlotsStarts.get(i)).append(" - ").append(freeSlotsEnds.get(i)).append(", ");
        }
        return out.substring(0, out.length() - 2) + "]";
    }
}


































