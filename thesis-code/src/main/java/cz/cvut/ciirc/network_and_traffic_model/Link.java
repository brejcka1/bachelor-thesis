package cz.cvut.ciirc.network_and_traffic_model;

import lombok.Getter;
import lombok.ToString;

/**
 * Simple class representing the link in network model.
 * For more information, see the Problem Statement.
 */

@Getter
@ToString
public class Link {
    private final int id;
    private final String name;
    private final int from;
    private final int to;

    private final int weight;
    private final int timeLag;
    private final String fromToString;

    public Link(int id, String name, int from, int to, int weight, int timeLag, String fromToString) {
        this.id = id;
        this.name = name;
        this.from = from;
        this.to = to;
        this.weight = weight;
        this.timeLag = timeLag;
        this.fromToString = fromToString;
    }

    public boolean fromToEquals(int from, int to) {
        return from == this.from && to == this.to;
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Method not implemented");
    }
}