package cz.cvut.ciirc.generator;

import cz.cvut.ciirc.exceptions.CustomException;
import cz.cvut.ciirc.helper.ProtoHandler;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Class for parallel generation of a lot of different instances in many folders differing by parameters.
 */
public class ParallelGenerator {

    /**
     * Number of threads
     */
    private static final int NUM_THREADS = 55;

    /**
     * Main class to run.
     *
     * @param args Arguments are empty
     */
    public static void main(String... args) {
        main_server_parallel();
    }

    /**
     * Main for all the different instances, each set of parameters is in different folder.
     */
    private static void main_server_parallel() {
        long start = System.nanoTime();
        int baseDirID = 6000;
        List<Integer> params;
        int addTo, base, maxNumReSI;

        final ExecutorService threadPool = Executors.newFixedThreadPool(NUM_THREADS);

        // set the instances parameters
        final int numInstances = 100;
        final int numDiffNumFrames = 20;
        final int numPeriodSets = 3;
        final int numTopologyModes = 3;
        final int numTopSizes = 3;

        final int totalNumDirs = numTopologyModes * numTopSizes * numDiffNumFrames * numPeriodSets;

        // generate instances
        try {
            for (int topSizeID = 0; topSizeID < numTopSizes; topSizeID++) {
                for (int topologyMode = 0; topologyMode < numTopologyModes; topologyMode++) {
                    params = ParametersBG.getBaseAndAddTo(topologyMode, topSizeID, numDiffNumFrames);
                    addTo = params.get(1);
                    base = params.get(0);
                    maxNumReSI = base + addTo * numDiffNumFrames;
                    for (int periodMode = 0; periodMode < numPeriodSets; periodMode++) {
                        for (int numReSI = base; numReSI < maxNumReSI; numReSI += addTo) {
                            final int currentDirID = baseDirID++;
                            final int currentTopologyMode = topologyMode;
                            final int currentTopologySizeId = topSizeID;
                            final int currentPeriodMode = periodMode;
                            final int currentNumReSI = numReSI;
                            threadPool.execute(() -> {
                                Random random = new Random();
                                random.setSeed(1278983 + currentDirID);
                                try {
                                    InstanceGenerator generator = new InstanceGenerator(random);
                                    for (int fileID = 0; fileID < numInstances; fileID++) {
                                        InstanceGenerator.printCurrentInstanceParams(numInstances, currentDirID, totalNumDirs,
                                                currentTopologySizeId, currentTopologyMode, currentPeriodMode, currentNumReSI, fileID);
                                        generator.generateInstance(fileID, ProtoHandler.getInstanceFilePath(currentDirID, fileID),
                                                currentTopologyMode, currentTopologySizeId, currentNumReSI, currentPeriodMode);
                                    }
                                } catch (CustomException e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                    }
                }
            }
            threadPool.shutdown();
            threadPool.awaitTermination(4 * 60, TimeUnit.MINUTES);
            long finishTime = System.nanoTime();
            long duration = (long) Math.floor((finishTime - start) / (Math.pow(10, 9)));
            System.out.println("Duration of generating " + numInstances + " instances: " + duration + " s");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (!threadPool.isShutdown()) {
                threadPool.shutdownNow();
            }
        }
    }
}
