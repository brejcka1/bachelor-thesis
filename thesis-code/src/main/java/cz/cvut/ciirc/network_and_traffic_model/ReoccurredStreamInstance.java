package cz.cvut.ciirc.network_and_traffic_model;

import cz.cvut.ciirc.SchedulerConstants;
import lombok.Getter;
import lombok.Setter;

/**
 * Simple class representing the reoccurred stream instance in network model.
 * For more information, see the Problem Statement.
 */
@Getter
public class ReoccurredStreamInstance {
    private final int hpID;
    @Setter
    private int start;
    private int duration;
    private StreamInstance parent;

    ReoccurredStreamInstance(int hpID, int duration, StreamInstance parent) {
        this.hpID = hpID;
        this.start = SchedulerConstants.DEFAULT_START_TIME;
        this.duration = duration;
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "ReoccurredStreamInstance{" +
                "hpID=" + hpID +
                ", start=" + start +
                ", duration=" + duration +
                ", parent=" + parent +
                '}';
    }

    public int getDurationWithTimeLag() {
        return (int) Math.ceil(duration + parent.getLink().getTimeLag());
    }

    public int getEnd() {
        return getDurationWithTimeLag() + start;
    }
}
