package cz.cvut.ciirc.generator;

import cz.cvut.ciirc.exceptions.GeneratorException;

import java.util.ArrayList;
import java.util.List;


/**
 * Represents constants used in instance generator.
 */
class ParametersBG {
    static final int SMALL_INSTANCE = 0; // couple of switches
    static final int MEDIUM_INSTANCE = 1;
    static final int LARGE_INSTANCE = 2; // several tens of switches
    public static final int[] topologySizes = {ParametersBG.SMALL_INSTANCE, ParametersBG.MEDIUM_INSTANCE, ParametersBG.LARGE_INSTANCE};
    // LINE TOPOLOGY
    static final int TREE_TOPOLOGY = 0;
    static final int RING_TOPOLOGY = 1;
    static final int LINE_TOPOLOGY = 2;
    static final int BETTER_LINK_WEIGHT = 10;
    static final int WORSE_LINK_WEIGHT = 10;
    // 100 vs 1
    static final int LINK_TIME_LAG = 1;
    static final int NODE_TIME_LAG = 10;
    // min a max ethernet packet
    static final int LOWER_BOUND_STREAM_DURATION = 1; //= 125 bytes
    static final int UPPER_BOUND_STREAM_DURATION = 12; // 1500 bytes

    // (L * 8) / 1000 -> (0.34 - 12.34)
    static final double LOWER_BOUND_RELEASE_DEADLINE_GAP = 0.15;
    static final double UPPER_BOUND_RELEASE_DEADLINE_GAP = 0.4;

    // {tree_depth, tree_branching, ring_centre, ring_branching, on_line, out_line}
    private static final int[] smallParams = {2, 2, 2, 3, 2, 3};
    private static final int[] mediumParams = {3, 6, 6, 6, 6, 6};
    private static final int[] largeParams = {4, 4, 14, 5, 14, 5};

    private static final int[][] periods = {
            {1000, 2500, 5000, 10000},
            {1000, 5000, 10000},
            {5000, 7500},
            {2000, 4000, 8000, 16000}
    };

    /**
     * Returns network graphs parameters based on its size.
     *
     * @param sizeMode Size of the topology
     * @return List of parameters in form of {tree_depth, tree_branching, ring_centre, ring_branching, on_line, out_line}
     * @throws GeneratorException In case the parameters could not be found
     */
    static int[] getParams(int sizeMode) throws GeneratorException {
        int[] params;
        switch (sizeMode) {
            case SMALL_INSTANCE:
                params = smallParams;
                break;
            case MEDIUM_INSTANCE:
                params = mediumParams;
                break;
            case LARGE_INSTANCE:
                params = largeParams;
                break;
            default:
                throw new GeneratorException("Incorrect size mode selected " + sizeMode);
        }
        return params;
    }

    /**
     * Returns period set corresponding to the given period mode.
     *
     * @param periodsMode Period mode
     * @return Period set
     * @throws GeneratorException In case the period set could not be found
     */
    static List<Integer> getPeriods(int periodsMode) throws GeneratorException {
        ArrayList<Integer> periodSet = new ArrayList<>();
        if (periodsMode < periods.length) {
            for (Integer p : periods[periodsMode]) {
                periodSet.add(p);
            }
        } else {
            throw new GeneratorException("Incorrect period mode selected " + periodsMode);
        }
        return periodSet;
    }

    /**
     * Returns list of [lowerBound, step] on the total number of reoccurred stream instances in the network for given
     * topology type and size. The step size depends on the total number of different values to be generated.
     *
     * @param topologyMode topology mode
     * @param topologySize size of the topology
     * @param numDiff      total number of different values to be generated
     * @return List of [lowerBound, step]
     */
    static List<Integer> getBaseAndAddTo(int topologyMode, int topologySize, int numDiff) {
        int numReSIbase = 0;
        int addTo = 0;
        int numMax;
        if (topologyMode == TREE_TOPOLOGY) {
            switch (topologySize) {
                case 0:
                    numReSIbase = 30;
                    numMax = 600;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                case 1:
                    numReSIbase = 600;
                    numMax = 12000;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                case 2:
                    numReSIbase = 1200;
                    numMax = 24000;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                default:
                    numReSIbase = 0;
                    System.err.println("not correct");
            }
        }
        if (topologyMode == RING_TOPOLOGY) {
            switch (topologySize) {
                case 0:
                    numReSIbase = 30;
                    numMax = 600;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                case 1:
                    numReSIbase = 600;
                    numMax = 12000;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                case 2:
                    numReSIbase = 1200;
                    numMax = 24000;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                default:
                    numReSIbase = 0;
                    System.err.println("not correct");
            }
        }
        if (topologyMode == LINE_TOPOLOGY) {
            switch (topologySize) {
                case 0:
                    numReSIbase = 25;
                    numMax = 500;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                case 1:
                    numReSIbase = 60;
                    numMax = 1200;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                case 2:
                    numReSIbase = 120;
                    numMax = 2400;
                    addTo = (numMax - numReSIbase) / numDiff;
                    break;
                default:
                    numReSIbase = 0;
                    System.err.println("not correct");
            }
        }
        ArrayList<Integer> toRet = new ArrayList<>();
        toRet.add(numReSIbase);
        toRet.add(addTo);
        return toRet;
    }
}
