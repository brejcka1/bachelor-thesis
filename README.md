# ReadMe

This document contains the installation process description. For the detailed description of the protocol proto buffer API,
 please refer to `API_documentation.html`. For the detailed description of the code, please refer to the generated `JavaDoc`.

## Project installation
The project is using Java 8, build system Maven and Gurobi solver 8.1. Gurobi (and license file) must be installed on the machine.

+ Install gurobi.
+ Set paths `GUROBI_HOME` and `GRB_LICENSE_FILE`. In case of troubles, please also set `LD_LIBRARY_PATH` to `$GUROBI_HOME/lib/`
+ Install gurobi to your local project repository via: 
```mvn install:install-file -Dfile=$GUROBI_HOME/lib/gurobi.jar -DgroupId=gurobi -DartifactId=gurobi -Dversion=8.1 -Dpackaging=jar```
+ Import the project via `pom.xml` to your favorite coding environment. Maven must be enabled.

## Creating .jar library

+ Run `mvn clean package` to create the `.jar` libraries
+ Folder target should now contain the created jars:
	* `gui-jar-with-dependencies.jar` (Graphical user interface)
	* `generator-jar-with-dependencies.jar` (Parallel generator)
	* `scheduler-jar-with-dependencies.jar` (Parallel scheduler)

## Protocol buffer compilation

The enclosed protocol definitions were compiled by libprotoc 3.6.1 into Java programming language.
The dependency used in the program to handle proto files is [protobuf-java](https://mvnrepository.com/artifact/com.google.protobuf/protobuf-java), version 3.6.1.

## Author

Kateřina Brejchová, *brejcka1@fel.cvut.cz*

CTU digital library [link](https://dspace.cvut.cz/handle/10467/82385)