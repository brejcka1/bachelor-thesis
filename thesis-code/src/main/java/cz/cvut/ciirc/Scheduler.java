package cz.cvut.ciirc;

import cz.cvut.ciirc.data_format.InputFile;
import cz.cvut.ciirc.exceptions.CustomException;
import cz.cvut.ciirc.exceptions.InputFileNotFoundException;
import cz.cvut.ciirc.exceptions.ResultsIOException;
import cz.cvut.ciirc.exceptions.UtilizationException;
import cz.cvut.ciirc.helper.ProtoHandler;
import cz.cvut.ciirc.scheduling.Solution;
import cz.cvut.ciirc.scheduling.Solver;
import cz.cvut.ciirc.scheduling.Statistics;
import lombok.Getter;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Main class to run the scheduler. Solves instances in the specified directory of file IDs in range [first, last].
 * Method schedule is also suitable to be used as a stand alone method for scheduling one instance of specified parameters.
 */
@Getter
public class Scheduler {
    /**
     * Time limit after which the solver stops searching for the solution.
     */
    private int timeLimit = SchedulerConstants.DEFAULT_TIME_LIMIT;
    /**
     * Best solution found by any of the solver algorithms.
     */
    private Solution bestSolution;

    /**
     * True in case statistics should be saved.
     */
    private boolean saveStats;

    public Scheduler(boolean saveStats) {
        this.saveStats = saveStats;
    }

    public Scheduler() {
        this.saveStats = false;
    }

    /**
     * Runs the scheduler - iterates over all instances, reads input data and runs the solvers.
     * Saves schedules and additional stats if desired.
     *
     * @throws CustomException In case of scheduling or IO error.
     */
    public void run(int dirID, int first, int last) throws CustomException {
        ArrayList<Statistics> stats = new ArrayList<>();
        List<AlgorithmType> classes = new ArrayList<>();

        Path statsPath = ProtoHandler.getStatsPath(dirID);
        Path resPath;

        classes.addAll(Arrays.asList(SchedulerConstants.cbj));

        try {
            if (saveStats) Statistics.saveHeader(statsPath, classes, dirID);
            for (int fileID = first; fileID <= last; fileID++) {
                System.out.println("------------------\nScheduling instance: " + fileID + "/" + last);
                Solution solution = schedule(stats, classes, statsPath, fileID, dirID);
                resPath = ProtoHandler.getResultFilePath(dirID, fileID);
                bestSolution = solution;
                ProtoHandler.writeProto(resPath, solution.getOutputProto());
            }
        } finally {
            try {
                if (saveStats) Statistics.saveTotalStats(stats, classes, statsPath);
            } catch (ResultsIOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * Schedules one instance using specified algorithms - reads it from file of specified ID and directory, solves it and
     * saves the schedule. If desired, also saves additional statistics.
     *
     * @param stats      List of all instances statistics
     * @param algorithms Algorithms to be used for the scheduling process
     * @param statsPath  Path to which the statistics are saved
     * @param fileID     ID of the instance file
     * @param dirID      ID of the directory where the instance is placed
     * @return class containing the information about the solution
     * @throws CustomException In case of child method error, please read the exception for more details
     */
    public Solution schedule(ArrayList<Statistics> stats, List<AlgorithmType> algorithms, Path statsPath, int fileID, int dirID) throws CustomException {
        Path filePath;
        InputFile.InputFileProto inputProto;
        AlgorithmType algorithmType;
        Solver solver;
        Solution solution;
        Solution bestSolution = null;

        filePath = ProtoHandler.getInstanceFilePath(dirID, fileID);

        try {
            inputProto = ProtoHandler.readInput(filePath);
        } catch (InputFileNotFoundException e) {
            System.err.println("Skipping FILE:" + e.getMessage());
            return null;
        }

        ProblemInstance instance = new ProblemInstance(inputProto);
        try {
            instance.getUtilization();
        } catch (UtilizationException e) {
            System.err.println("Skipping FILE:" + e.getMessage());
            return null;
        }

        if (saveStats) {
            stats.add(new Statistics(statsPath, fileID, instance.getAvgUtilization(), instance.getMaxUtilization(), algorithms.size()));
            Statistics.saveCommonInstanceInfo(statsPath, fileID, instance.getAvgUtilization(), instance.getMaxUtilization());
        }

        for (int algID = 0; algID < algorithms.size(); algID++) {
            instance = new ProblemInstance(inputProto);
            algorithmType = algorithms.get(algID);
            solver = algorithmType.getSolver(instance);
            solver.solve(timeLimit);
            solution = solver.getSolution();
            solution.process();
            if (bestSolution == null || solution.isScheduleFound() && solution.getObjValue() < bestSolution.getObjValue()) {
                bestSolution = solution;
            }
            if (saveStats) stats.get(stats.size() - 1).addResultsAndSave(algorithmType.getMethodClass().getSimpleName(),
                    solution.isScheduleFound(), solution.getObjValue(), solution.getElapsedTime(), algID);
        }

        return bestSolution;
    }
}
