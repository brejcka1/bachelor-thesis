package cz.cvut.ciirc.exceptions;

public class UtilizationException extends CustomException {

    public UtilizationException(String s) {
        super(s);
    }
}
