package cz.cvut.ciirc.exceptions;

public class StreamRoutingException extends CustomException {

    public StreamRoutingException(String s) {
        super(s);
    }
}
