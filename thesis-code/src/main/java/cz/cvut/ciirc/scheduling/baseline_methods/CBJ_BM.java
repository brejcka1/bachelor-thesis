package cz.cvut.ciirc.scheduling.baseline_methods;

import cz.cvut.ciirc.AlgorithmType;
import cz.cvut.ciirc.ProblemInstance;
import cz.cvut.ciirc.exceptions.ArrayNotInitializedException;
import cz.cvut.ciirc.exceptions.ImplementationException;
import cz.cvut.ciirc.network_and_traffic_model.Stream;
import cz.cvut.ciirc.network_and_traffic_model.StreamInstance;
import cz.cvut.ciirc.scheduling.Heuristics;
import cz.cvut.ciirc.scheduling.helper_classes.ObjectPriorities;
import cz.cvut.ciirc.scheduling.helper_classes.StreamInstanceInfo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeoutException;

/**
 * Conflicted backjumping and backmarking described in the diploma thesis pseudocode:
 * Marek Vlk: Dynamic Scheduling
 * with enhancements described in the text: domain reduction, clever queue sorting
 */

@SuppressWarnings("Duplicates")
public class CBJ_BM extends Heuristics {
    public CBJ_BM(ProblemInstance problemInstance) {
        super(problemInstance);
        setAlgorithmType(AlgorithmType.CBJ_BM);
    }

    // criterium = (DF, MRT, ID_IN_SI_LIST)
    @Override
    public PriorityQueue<ObjectPriorities> getPQ() throws ArrayNotInitializedException {
        PriorityQueue<ObjectPriorities> pQ = new PriorityQueue<>();
        List<StreamInstanceInfo> siiList = initializeStreamInfoList();
        int value;
        Stream stream;

        for (StreamInstanceInfo curSii : siiList) {
            stream = curSii.getCurSI().getStream();
            value = stream.getProblemInstance().getHP() - stream.getMinimalRequiredTime() * stream.getNumHPInstances();
            pQ.add(new ObjectPriorities<>(curSii, stream.getDeadline() / 100, value, curSii.getIDinStreamList()));
        }
        return pQ;
    }

    protected int getStep(StreamInstanceInfo curVariable, int siID, int numSII) throws ArrayNotInitializedException {
        return 1;
    }

    @Override
    protected boolean schedule(PriorityQueue<ObjectPriorities> pQ) throws ArrayNotInitializedException, ImplementationException, TimeoutException {
        int numSII = pQ.size();
        StreamInstanceInfo[] variables = new StreamInstanceInfo[numSII];
        for (int i = 0; i < numSII; i++) {
            variables[i] = (StreamInstanceInfo) pQ.poll().getObject();
        }
        int siiID = 0;
        int curStart;
        boolean success = false;
        boolean fail;

        // create arrays
        int[] newVals = new int[numSII];
        int[] startTimes = new int[numSII];
        int[][] mark = new int[numSII][getProblemInstance().getHP()]; // A default value of 0 for arrays of integral types is guaranteed by the language spec
        int[][] backTo = new int[numSII][getProblemInstance().getHP()];
        ArrayList<HashSet<Integer>> conflictSet = new ArrayList<>(numSII);
        for (int id = 0; id < numSII; id++) {
            conflictSet.add(new HashSet<>());
        }
        // initialize start times to earliest values
        for (int id = 0; id < startTimes.length; id++) {
            startTimes[id] = variables[id].getEarliestStart();
            newVals[id] = variables[id].getEarliestStart();
        }

        while (siiID < numSII) {
            curStart = calculateOffset(newVals[siiID], variables[siiID], variables, siiID, conflictSet);
            success = false;
            // try to assign current variable of index siiID
            while (!success && curStart <= variables[siiID].getLatestStart()) {
                // break in case the time is out
                if (Thread.currentThread().isInterrupted()) {
                    return false;
                }
                // check if assignment was prohibited in the future (type A saving)
                if (mark[siiID][curStart] < backTo[siiID][curStart]) {
                    conflictSet.get(siiID).add(mark[siiID][curStart]);
                    curStart += getStep(variables[siiID], siiID, numSII);
                    continue;
                }
                startTimes[siiID] = curStart;
                fail = false;

                // run consistency checks and skip the ones that were already done (type B saving)
                for (int j = backTo[siiID][curStart]; j < siiID; j++) {
                    if (!isConsistent(variables[siiID], startTimes[siiID], variables[j], startTimes[j])) {
                        conflictSet.get(siiID).add(j);
                        mark[siiID][curStart] = j;
                        fail = true;
                        break;
                    }
                }

                // update mark and back to based on successful or failed assignment
                if (!fail) {
                    mark[siiID][curStart] = siiID - 1;
                    success = true;
                }
                backTo[siiID][curStart] = siiID;
                curStart += getStep(variables[siiID], siiID, numSII);
            }
            // if assignment of current variable not successful -> backtrack
            if (!success) {
                siiID = getNewSiiID(variables, siiID, newVals, backTo, conflictSet);
                if (siiID == -1) return false;
            } else {
                newVals[siiID] = curStart;
                variables[siiID].getCurSI().getReSIInHP(0).setStart(startTimes[siiID]);
                siiID++;
            }
        }

        // save calculated offsets
        if (success) {
            for (int i = 0; i < numSII; i++) {
                StreamInstance curSI = variables[i].getCurSI();
                for (int hpID = 0; hpID < curSI.getNumInHP(); hpID++) {
                    curSI.getReSIInHP(hpID).setStart(startTimes[i] + curSI.getStream().getPeriod() * hpID);
                }
            }
        }
        return success;
    }

}